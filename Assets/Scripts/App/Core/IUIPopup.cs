﻿using UnityEngine;

namespace ClassicGames.WildBilly
{
    public interface IUIPopup
    {
        void Init();
        void Show();
        void Show(object data);
        void Hide();
        void Update();
        void Dispose();
        void SetMainPriority();
    }

    public interface IUIGamePopup
    {
        void Init(Transform canvas);
        void Show();
        void Hide();
        void Update();
        void Dispose();
    }
}