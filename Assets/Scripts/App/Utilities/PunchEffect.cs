﻿using UnityEngine;

namespace ClassicGames.WildBilly
{
	public class PunchEffect : MonoBehaviour
	{
		private Vector3 _startPosition;
		private Vector3 _targetPosition;
		private Vector3 _reversePosition;
		private bool _punching;
		private float _time;
		private float _distance;
		private bool _reverse;
		private int _reverseStateIndex;
		private System.Action _callback;

		public void Punch(Vector3 dir, float time, System.Action callback = null, bool forceEnd = false)
		{
			_callback = callback;
			_time = time;
			_startPosition = transform.localPosition;
			_targetPosition = _startPosition + (dir * time);
			_reversePosition = _startPosition - (dir * (time) / 20);
			_distance = Vector3.Distance(_startPosition, _targetPosition);
			_punching = true;

            if (forceEnd)
            {
                ForceEndPunch();
            }
		}

		public void ForceEndPunch()
		{
			transform.localPosition = _startPosition;
			_punching = false;
			_reverse = false;
			_reverseStateIndex = 0;
			_callback?.Invoke();
			Destroy(this);
		}

		private void Update()
		{
			if (_punching)
			{
				if (_reverse)
				{
					if (_reverseStateIndex == 1)
					{
						transform.localPosition = Vector3.MoveTowards(transform.localPosition, _reversePosition,
							_distance / (_time / 10f) * Time.deltaTime);

						if (Mathf.Abs(Vector3.Distance(transform.localPosition, _reversePosition)) <= 0.01f)
						{
							_reverseStateIndex = 2;
						}

					}
					else if (_reverseStateIndex == 2)
					{
						transform.localPosition = Vector3.MoveTowards(transform.localPosition, _startPosition,
							_distance / (_time / 10f) * Time.deltaTime);

						if (Mathf.Abs(Vector3.Distance(transform.localPosition, _startPosition)) <= 0.01f)
						{
							ForceEndPunch();
						}
					}
				}
				else
				{
					transform.localPosition = Vector3.MoveTowards(transform.localPosition, _targetPosition,
						_distance / (_time / 20f) * Time.deltaTime);

					if (Mathf.Abs(Vector3.Distance(transform.localPosition, _targetPosition)) <= 0.01f)
					{
						transform.localPosition = _targetPosition;
						_reverse = true;
						_reverseStateIndex = 1;
					}
				}
			}
		}

	}

	public static class TransformEffectsExtensions
	{
		public static void DoPunch(this Transform transform, Vector3 dir, float time, System.Action callback = null, bool forceEnd = false)
		{
			transform.gameObject.AddComponent<PunchEffect>().Punch(dir, time, callback, forceEnd);
		}
		public static void ForceEndPunch(this Transform transform)
		{
			PunchEffect effect = transform.GetComponent<PunchEffect>();
			if (effect != null)
			{
				effect.ForceEndPunch();
                transform.localEulerAngles = Vector3.zero; // fix to reset the rotation
			}
		}
	}
}