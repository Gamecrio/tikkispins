﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ClassicGames.WildBilly
{
	public class OnBehaviourHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDragHandler,
		IBeginDragHandler, IEndDragHandler
	{

		public event Action<GameObject> MouseUpTriggered;

		public event Action<GameObject> MouseDownTriggered;

		public event Action<Collider2D> Trigger2DEntered;

		public event Action<Collider2D> Trigger2DExited;

		public event Action<Collider> TriggerEntered;

		public event Action<Collider> TriggerExited;

		public event Action<Collider> TriggerStaying;

		public event Action<GameObject> Destroying;

		public event Action<PointerEventData> PointerEntered;

		public event Action<PointerEventData> PointerExited;

		public event Action<GameObject> Updating;

		public event Action<PointerEventData, GameObject> DragUpdated;

		public event Action<PointerEventData, GameObject> DragBegan;

		public event Action<PointerEventData, GameObject> DragEnded;

		public event Action<GameObject> OnParticleCollisionEvent;

		public event Action<string> OnAnimationStringEvent;

		public event Action<int> OnAnimationIntEvent;

		public event Action<int> OnAnimatorIKEvent;


		public void OnPointerEnter(PointerEventData eventData)
		{
			if (EventSystem.current.IsPointerOverGameObject(
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
			0
#endif
				))
				return;
			PointerEntered?.Invoke(eventData);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			if (EventSystem.current.IsPointerOverGameObject(
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
			0
#endif
				))
				return;
			PointerExited?.Invoke(eventData);
		}

		private void OnMouseUp()
		{
			if (EventSystem.current.IsPointerOverGameObject(
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
			0
#endif
			))
				return;
			MouseUpTriggered?.Invoke(gameObject);
		}

		private void OnMouseDown()
		{
			if (EventSystem.current.IsPointerOverGameObject(
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
			0
#endif
				))
				return;

			MouseDownTriggered?.Invoke(gameObject);
		}

		public void OnBeginDrag(PointerEventData eventData)
		{
			DragBegan?.Invoke(eventData, gameObject);
		}

		public void OnDrag(PointerEventData eventData)
		{
			DragUpdated?.Invoke(eventData, gameObject);
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			DragEnded?.Invoke(eventData, gameObject);
		}
		private void Update()
		{
			Updating?.Invoke(gameObject);
		}
		private void OnAnimationEvent(string parameter)
		{
			OnAnimationStringEvent?.Invoke(parameter);
		}

		private void OnAnimationEvent(int parameter)
		{
			OnAnimationIntEvent?.Invoke(parameter);
		}

		private void OnAnimatorIK(int layerIndex)
		{
			OnAnimatorIKEvent?.Invoke(layerIndex);
		}
	}
}
