﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ClassicGames.WildBilly
{
	public static class UriHelper
	{
		public static Dictionary<string, string> DecodeQueryParameters(this Uri uri)
		{
			if (uri == null)
				throw new ArgumentNullException("uri");

			if (uri.Query.Length == 0)
				return new Dictionary<string, string>();

			return uri.Query.TrimStart('?')
							.Split(new[] { '&', ';' }, StringSplitOptions.RemoveEmptyEntries)
							.Select(parameter => parameter.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries))
							.GroupBy(parts => parts[0],
									 parts => parts.Length > 2 ? string.Join("=", parts, 1, parts.Length - 1) : (parts.Length > 1 ? parts[1] : ""))
							.ToDictionary(grouping => grouping.Key,
										  grouping => string.Join(",", grouping));
		}
	}

    public static class EditorHelpers
    {
#if UNITY_EDITOR
        [MenuItem("Helpers/Assign 'databundle' to selected objects")]
        private static void ChangeBundleName()
        {
            var ids = Selection.instanceIDs;
            string assetPath;

            if (ids != null && ids.Length > 0)
            {
                foreach (var item in ids)
                {
                    assetPath = AssetDatabase.GetAssetPath(item);
                    AssetImporter.GetAtPath(assetPath).SetAssetBundleNameAndVariant("databundle", "");
                }
            }
            else
            {
                Debug.Log("Nothing selected");
            }
        }
		
#endif
    }
	
}
