﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using System.Globalization;

namespace ClassicGames.WildBilly
{
    public static class InternalTools
    {
        public static void ShuffleList<T>(this IList<T> list)
        {
            System.Random rnd = new System.Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static Color HexToColor(string hex)
        {
            Color color = Color.white;

            if (!ColorUtility.TryParseHtmlString(hex, out color))
            {
#if UNITY_EDITOR
                Debug.Log("HexToColor parse error with string: " + hex);
#endif
            }

            return color;
        }

        public static T GetEnumFromString<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value);
        }

        public static int[] StringToIntRange(string value)
        {
            value = value.Trim();

            int[] range;

            if (value.Contains("-"))
            {
                string[] split = value.Split("-"[0]);

                int first = int.Parse(split[0]);
                int last = int.Parse(split[1]);

                range = new int[last - first + 1];

                int j = 0;
                for (int i = first; i <= last; i++)
                {
                    range[j] = i;
                    j++;
                }

                return range;
            }
            else if (value.Contains(","))
            {
                string[] split = value.Split(","[0]);
                range = new int[split.Length];

                for (int i = 0; i < range.Length; i++)
                {
                    range[i] = int.Parse(split[i]);
                }

                return range;
            }
            else
            {
                throw new FormatException("Value " + value + " is incorrect!");
            }
        }

        public static Sequence DoActionDelayed(TweenCallback action, float delay = 0f)
        {
            if (action == null)
                return null;

            Sequence sequence = DOTween.Sequence();
            sequence.PrependInterval(delay);
            sequence.AppendCallback(action);

            return sequence;
        }

        public static void KillSequence(Sequence sequence, bool complete = false)
        {
            if (sequence != null)
            {
                sequence.Kill(complete);
                sequence = null;
            }
        }

        public static IEnumerator ParseModel(string model, Action<SpinResultData> callback)
        {
			//Debug.LogError(model);
            SpinResultData spinResultData = new SpinResultData();
            SpinResultOptimizedModel optimizedModel = JsonUtility.FromJson<SpinResultOptimizedModel>(model);

            spinResultData.statusWin = optimizedModel.status ? 1 : 0;
            spinResultData.statusBet = optimizedModel.status ? 1 : 0;
            spinResultData.currentBalance = optimizedModel.curBal;
            spinResultData.currentWinBalance = optimizedModel.curWinBal;
            spinResultData.status = optimizedModel.status;
            spinResultData.isBonusGame = optimizedModel.isBonusGame;

            string[] split2;
            string[] split3;

            // parse results
            string[] split = optimizedModel.results.Split(',');
            spinResultData.resultMatrix = new int[split.Length];
			for (int i = 0; i < split.Length; i++)
			{
				//Debug.Log(split[i]);
				spinResultData.resultMatrix[i] = (int)Convert.ToDouble(split[i], CultureInfo.InvariantCulture);
			}
				

            yield return null;

            if (string.IsNullOrEmpty(optimizedModel.winLines))
            {
                spinResultData.winLines = new SpinResultWinInfoItem[0];
            }
            else
            {
                // parse win lines
                split = optimizedModel.winLines.Split('|');
                spinResultData.winLines = new SpinResultWinInfoItem[split.Length];
                for (int i = 0; i < split.Length; i++)
                {
                    split2 = split[i].Split(';');

                    int lineIndex = (int)Convert.ToDouble(split2[0], CultureInfo.InvariantCulture);
                    double winValue = Convert.ToDouble(split2[1], CultureInfo.InvariantCulture);
                    string elements = split2[2];
                    split2 = elements.Split('_');

                    SpinResultWinItem[] winnersIcons = new SpinResultWinItem[split2.Length];
                    for (int j = 0; j < split2.Length; j++)
                    {
                        split3 = split2[j].Split(',');
                        winnersIcons[j] = new SpinResultWinItem((int)Convert.ToDouble(split3[0], CultureInfo.InvariantCulture), (int)Convert.ToDouble(split3[1], CultureInfo.InvariantCulture), (int)Convert.ToDouble(split3[2], CultureInfo.InvariantCulture));
                    }

                    spinResultData.winLines[i] = new SpinResultWinInfoItem(lineIndex, winValue, winnersIcons);

                    yield return null;
                }
            }

            yield return null;

            if (string.IsNullOrEmpty(optimizedModel.shells))
            {
                spinResultData.shellInfo = new SpinResultWinItem[0];
            }
            else
            {
                split = optimizedModel.shells.Split('|');
                spinResultData.shellInfo = new SpinResultWinItem[split.Length];
                for (int i = 0; i < split.Length; i++)
                {
                    split2 = split[i].Split(',');
                    spinResultData.shellInfo[i] = new SpinResultWinItem(Convert.ToInt32(split2[0]), Convert.ToInt32(split2[1]), Convert.ToInt32(split2[2]));

                    yield return null;
                }
            }

            spinResultData.totalReward = optimizedModel.rew;

            callback?.Invoke(spinResultData);
        }

        private class SpinResultOptimizedModel
        {
            public bool status;
            public int isBonusGame;
            public double curBal;
            public double curWinBal;         
            public double rew;
            public string results;
            public string shells;
            public string winLines;
        }
    }

    public class DebugWrapper
    {
        public static void LogRed(string message)
        {
            Debug.Log("<color=red>" + message + "</color>");
        }
    }
}