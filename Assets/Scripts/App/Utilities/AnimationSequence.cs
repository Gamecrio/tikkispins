﻿using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
    public class AnimationSequence : MonoBehaviour
    {
        private WaitForSeconds _frameDelay;

        private Image _uiImage;

        private SpriteRenderer _spriteRenderer;

        public Sprite[] frames;

        public float framesPerSecond;

        public bool loop;

        public int currentFrame;

        public bool wasPaused;

        public bool isPlaying;

        public bool playAtStart;

        private bool _breakOfCoroutine;


        private void Awake()
        {
            _uiImage = GetComponent<Image>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void OnEnable()
        {
            if (playAtStart)
            {
                Play(framesPerSecond, loop, currentFrame);
            }
        }

        private void OnDisable()
        {
            if (playAtStart)
            {
                Stop();
                _breakOfCoroutine = false;
            }
        }

        public void LoadFrames(Sprite[] frames )//string resourcesPath)
        {
            if (frames == null || frames.Length == 0)
                Debug.LogError("Frames list is null or has no frames");
                

           // frames = Resources.LoadAll<Sprite>(resourcesPath);
           // frames = frames.OrderBy(go => go.name.Length).ThenBy(x => x.name).ToArray();
            this.frames = frames;
            currentFrame = 0;
        }

        public void LoadFrames(string resourcesPath)
        {
            frames = MainApp.Instance.LoadObjectsController.GetObjectsByPath<Sprite>(resourcesPath);
            frames = frames.OrderBy(go => go.name.Length).ThenBy(x => x.name).ToArray();
            currentFrame = 0;
        }

        public void Play(float framesPerSecond = 60, bool loop = true, int frame = 0)
        {
            if (frames == null || frames.Length == 0)
                return;

            if (isPlaying)
            {
                Stop();
            }

            this.framesPerSecond = framesPerSecond;
            this.loop = loop;

            _frameDelay = new WaitForSeconds(1f / this.framesPerSecond);

            //if (!wasPaused)
            //{
            //	currentFrame = frame;
            //}

            wasPaused = false;

            if (gameObject.activeSelf)
            {
                StartCoroutine(AnimationSequenceRoutine());
                isPlaying = true;
            }
        }

        public void Stop()
        {
            if (!isPlaying)
                return;

            wasPaused = false;
            _breakOfCoroutine = true;

            StopCoroutine(AnimationSequenceRoutine());

            isPlaying = false;
        }

        public void Pause()
        {
            if (!isPlaying)
                return;

            wasPaused = true;

            StopCoroutine(AnimationSequenceRoutine());

            isPlaying = false;
        }

        public void ResetAnimation()
        {
            if (frames == null || frames.Length == 0)
                return;

            currentFrame = 0;

            if (_uiImage != null)
            {
                _uiImage.sprite = frames[currentFrame];
            }

            if (_spriteRenderer != null)
            {
                _spriteRenderer.sprite = frames[currentFrame];
            }
        }

        private IEnumerator AnimationSequenceRoutine()
        {
            do
            {
                while (currentFrame < frames.Length)
                {
                    if (_breakOfCoroutine)
                    {
                        _breakOfCoroutine = false;
                        yield break;
                    }

                    if (_uiImage != null)
                    {
                        _uiImage.sprite = frames[currentFrame];
                    }

                    if (_spriteRenderer != null)
                    {
                        _spriteRenderer.sprite = frames[currentFrame];
                    }

                    currentFrame++;

                    yield return _frameDelay;
                }

                currentFrame = 0;
            } while (loop);
        }
    }
}