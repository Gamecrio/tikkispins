﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClassicGames.WildBilly
{
	public enum RTPPreset
	{
		RTP_96,
		RTP_100,
		RTP_90,
		RTP_98,
		RTP_105,
		RTP_85
	}

	public class Constants
	{
		public static readonly string PATH_TO_GAMES_CACHE = Application.persistentDataPath + "/Games/Cache/";
		public static readonly string GAME_NAME = "SlotsAquaman";

		public static bool AllowDebugRTP = true;
		public static bool DebugGamesWithoutNetwork = true;
		public const RTPPreset RTP_PRESET = RTPPreset.RTP_96;

		public const int BlockCount = 2;
		public const float TimeoutBetResponce = 30f;

        public const int MultiplierBonusGame = 2;

		public const string MessageFreeGames = "FREE GAME {0} OF {1}";
		public const string MessageBonusGame = "BONUS FEATURE WON";
        public const bool TEST_LOCAL = true;
    }
}