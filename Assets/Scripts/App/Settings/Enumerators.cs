﻿namespace ClassicGames.WildBilly.Common
{
	public class Enumerators
	{
		public enum RegistrationFieldType
		{
			NAME,
			SURNAME,
			EMAIL,
			COUNTRY,
			DIOCESE,
			USERNAME,
			PHONE,
			PASSWORD,
			CONFIRM_PASSWORD
		}

		public enum SoundType
		{
			Background,
			FreeGameBackground,
			FreeGameWinnerPopup,
			Click,
			Spin,
			SpinFree,
			StopSpin,
			ShowInfoPopup,
            CloseInfoPopup,
            Win,
			WinFrame,
			WinPopup,
			StartAutoSpin,
			StopAutoSpin,
			FreeSpinsStart,
			HotFreeSpins_Voice,
			BonusGame,
			BonusGamePopupBackground,
			//bonuses
			Bonus1,
			Bonus2,
			Bonus3,
			Bonus4,
			Bonus5,
			ChooseBonusItem,
		}

		public enum MiniGameType
		{
			BONUS_GAME,
			DOUBLE_MONEY
		}

		public enum ApiType
		{
			ClassicGames
		}

		public enum FrameAnimationType
		{
			FRAME_BY_FRAME,
			GLOW
		}

		public enum SourceType
		{
			Hub,
			Browser,
			OutsidePlatform
		}
	}
}