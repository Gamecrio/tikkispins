﻿using ClassicGames.WildBilly.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClassicGames.WildBilly
{
	public class LayerDataModel
	{
		public enum LayerType
		{
			Simple,
			Any,
			Bonus
		}

		public class LayersDataInfo
		{
			public List<LayerDataInfo> data;

			[UnityEngine.Scripting.Preserve]
			public LayersDataInfo()
			{
				data = new List<LayerDataInfo>();
			}

			[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
			public LayersDataInfo(List<LayerDataInfo> data)
			{
				this.data = data;
			}
		}

		public class LayerDataInfo
		{
			public int id;
			public LayerType type;
			public Enumerators.FrameAnimationType animationType;
			public List<RewardInfo> rewards;

			[UnityEngine.Scripting.Preserve]
			public LayerDataInfo()
			{
				rewards = new List<RewardInfo>();
			}

			[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
			public LayerDataInfo(
				int id,
				LayerType type,
				Enumerators.FrameAnimationType animationType,
				List<RewardInfo> rewards)
			{
				this.id = id;
				this.type = type;
				this.animationType = animationType;
				this.rewards = rewards;
			}
		}

		public class RewardInfo
		{
			public int count;
			public int money;

			[UnityEngine.Scripting.Preserve]
			public RewardInfo()
			{

			}

			[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
			public RewardInfo(
				int count,
				int money)
			{
				this.count = count;
				this.money = money;
			}
		}
	}
}
