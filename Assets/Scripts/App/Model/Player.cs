﻿namespace ClassicGames.WildBilly
{
	public class Player
	{
		public long identity;
		public long manager_id;
		public string[] rules;
		public string manager_login,
						email,
						login,
						first_name,
						parent_name,
						phone_number,
						gender,
						address;

		public int birthday,
					created_at,
					verification_at,
					credit_limit;

		public bool online;

		public double balance;
		public double winBalance;

		public long UserId;

		public bool WelcomeBonusReceived;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public Player()
		{

		}
	}
}
