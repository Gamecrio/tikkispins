﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ClassicGames.WildBilly.Common;

namespace ClassicGames.WildBilly
{
	public class MiniGameDataModel
	{
		public Enumerators.MiniGameType type;
		public bool active;

		[UnityEngine.Scripting.Preserve]
		public MiniGameDataModel()
		{
		}

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public MiniGameDataModel(
			Enumerators.MiniGameType type,
			bool active)
		{
			this.type = type;
			this.active = active;
		}
	}

	public class BonusGameDataModel : MiniGameDataModel
	{
		////public List<BonusGameRewardData> parameters;

		//[UnityEngine.Scripting.Preserve]
		//public BonusGameDataModel()
		//{

		//}

		//[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		////public BonusGameDataModel(
		////	Enumerators.MiniGameType type,
		////	bool active,
		////	List<BonusGameRewardData> parameters)
		////{
		////	this.type = type;
		////	this.active = active;
		////	this.parameters = parameters;
		////}
	}

	public class MiniGameDataConverter : JsonCreationConverter<MiniGameDataModel>
	{
		protected override MiniGameDataModel Create(Type objectType, JObject jObject)
		{
			Enumerators.MiniGameType type = Enumerators.MiniGameType.BONUS_GAME;

			if (jObject.TryGetValue("type", out JToken value))
			{
				if (!Enum.TryParse(value.ToObject<string>(), out type))
				{
					Debug.LogException(new Exception($"parse value: {value.ToObject<string>()} error"));
				}
			}

			//if (type == Enumerators.MiniGameType.BONUS_GAME)
			//{
			//	return new BonusGameDataModel();
			//}
			//else
			{
				return new MiniGameDataModel();
			}
		}

		private bool FieldExists(string fieldName, JObject jObject)
		{
			return jObject[fieldName] != null;
		}
	}

	public abstract class JsonCreationConverter<T> : JsonConverter
	{
		protected abstract T Create(Type objectType, JObject jObject);

		public override bool CanConvert(Type objectType)
		{
			return typeof(T).IsAssignableFrom(objectType);
		}

		public override bool CanWrite
		{
			get { return false; }
		}

		public override object ReadJson(JsonReader reader,
										Type objectType,
										 object existingValue,
										 JsonSerializer serializer)
		{
			JObject jObject = JObject.Load(reader);

			T target = Create(objectType, jObject);

			serializer.Populate(jObject.CreateReader(), target);

			return target;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			serializer.Serialize(writer, value);
		}
	}
}
