﻿using System;

namespace ClassicGames.WildBilly
{
	public class PlayerMoney
    {
		public event Action<double, double> OnMoneyOwnChanged;

        public double Money
		{
			get { return MainApp.Instance.PlayerController.Money; }
			set { MainApp.Instance.PlayerController.Money = value; }
		}

        public double TotalWinMoney
        {
            get { return MainApp.Instance.PlayerController.TotalWinMoney; }
            set { MainApp.Instance.PlayerController.TotalWinMoney = value; }

        }

        public PlayerMoney(GameController SlotsGameController)
        {
        }

        public void SetMoney(double money, double totalWin)
        {
            Money = money;
            if (Money < 0)
                Money = 0;

            TotalWinMoney = totalWin;
            if (TotalWinMoney < 0)
                TotalWinMoney = 0;

            if (OnMoneyOwnChanged != null)
                OnMoneyOwnChanged(Money, TotalWinMoney);
        }

        public void ChangeMoney(double balance, double winBalance)
        {
            SetMoney(balance, winBalance);
        }
    }
}
