﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClassicGames.WildBilly
{
	public class InformationPopupData
	{
		public string title,
					  description,
					  leftButtonText,
					  rightButtonText;

		public Action LeftButtonAction,
					  RightButtonAction;

		[UnityEngine.Scripting.Preserve]
		public InformationPopupData(string title, string description, string leftButtonText = "", Action LeftButtonAction = null, string rightButtonText = "", Action RightButtonAction = null)
		{
			this.title = title;
			this.description = description;

			this.leftButtonText = leftButtonText;
			this.rightButtonText = rightButtonText;

			this.LeftButtonAction = LeftButtonAction;
			this.RightButtonAction = RightButtonAction;
		}

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public InformationPopupData()
		{

		}
	}

	public class UserExclusiveBonusModel
	{
		public double amount;
		public double max;
		public double bonus;

		[UnityEngine.Scripting.Preserve]
		public UserExclusiveBonusModel()
		{
		}

		[Newtonsoft.Json.JsonConstructor]
		[UnityEngine.Scripting.Preserve]
		public UserExclusiveBonusModel(double amount, double max, double bonus)
		{
			this.amount = amount;
			this.max = max;
			this.bonus = bonus;
		}
	}
}
