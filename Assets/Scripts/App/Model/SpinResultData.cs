﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClassicGames.WildBilly
{
    public class SpinResultData
    {
        public bool status = false;
        public int statusBet = 0;
        public int statusWin = 0;
        public int isBonusGame = 0;
        public double totalReward = 0;
		public double currentBalance = 0;
		public double currentWinBalance = 0;
		public int[] resultMatrix = null;
        public SpinResultWinInfoItem[] winLines = null;
        public SpinResultWinItem[] shellInfo = null;

		[UnityEngine.Scripting.Preserve]
		public SpinResultData()
		{
		}

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public SpinResultData(
			bool status, 
			int statusBet,
			int statusWin,
			int isBonusGame,
			double totalReward,
			double currentBalance,
			double currentWinBalance,
			int[] resultMatrix,
			SpinResultWinInfoItem[] winLines,
			SpinResultWinItem[] shellInfo)
		{
			this.status = status;
			this.statusBet = statusBet;
			this.statusWin = statusWin;
			this.isBonusGame = isBonusGame;
			this.totalReward = totalReward;
			this.currentBalance = currentBalance;
			this.currentWinBalance = currentWinBalance;
			this.resultMatrix = resultMatrix;
			this.winLines = winLines;
			this.shellInfo = shellInfo;
		}
	}

    public class SpinResultWinInfoItem
    {
		public int lineIndex;
        public double winValue = 0;
        public SpinResultWinItem[] winnersIcons;

		[UnityEngine.Scripting.Preserve]
		public SpinResultWinInfoItem()
		{
		}

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public SpinResultWinInfoItem(int lineIndex,
			double winValue,
			SpinResultWinItem[] winnersIcons)
		{
			this.lineIndex = lineIndex;
			this.winValue = winValue;
			this.winnersIcons = winnersIcons;
		}
	}

    public class SpinResultWinItem
    {
		public int num = 0;
		public int col = 0;
		public int pos = 0;

		[UnityEngine.Scripting.Preserve]
		public SpinResultWinItem()
		{		
		}

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public SpinResultWinItem(
			int num,
			int col,
			int pos)
		{
			this.num = num;
			this.col = col;
			this.pos = pos;
		}
	}
}