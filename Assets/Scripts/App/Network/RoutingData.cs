﻿namespace ClassicGames.WildBilly
{
	public static class RoutingData
    {
        public const string LOGIN = "/login";
        public const string LOGOUT = "/logout";
        public const string GET_USER_INFO = "/user/find/";
        public const string WIN = "/win";
		public const string BET = "/bet";
		public const string GET_GAMES = "/games";	

		public const string CHECK_EMAIL = "/rest/users/IsEmailExists";
        public const string CHECK_USERNAME = "/rest/users/IsUsernameExists";
        public const string SIGN_IN = "/rest/users/signIn";
        public const string SIGN_UP = "/rest/users/signUp";
        public const string CHANGE_USER_PASSWORD = "/rest/users/ChangeUserPassword";
        public const string REMIND_PASSWORD = "/rest/users/RemindPassword";
        public const string RESENT_CONFIRM_EMAIL = "/rest/users/ResendConfirmEmail";
        public const string GET_PERSONAL_DATA = "/rest/users/GetPersonalData";


    }
}
