﻿namespace ClassicGames.WildBilly
{
    public class LoginRequestData
    {
        public string login;
        public string password;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public LoginRequestData()
		{

		}
    }

    public class LoginRequestResponseData
    {
        public string status;
        public LoginUserData result;

		//public bool success;
		//public string JWT;
		//public int user_id;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public LoginRequestResponseData()
		{

		}
    }

    public class LoginUserData
    {
        public bool status;
        public string JWT;
        public long user_id;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public LoginUserData()
		{

		}
    }

    public class ResultRequestResponseData
    {
        public bool status;
        public string message;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public ResultRequestResponseData()
		{
				
		}
    }

    public class BalanceRequestResponseData : ResultRequestResponseData
    {
        public double balance;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public BalanceRequestResponseData()
		{

		}
    }

    public class UserRequestResponseData : ResultRequestResponseData
    {
        public bool status;
        public UserModelData user;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public UserRequestResponseData()
		{

		}
    }

    public class FailRequestResponseData : ResultRequestResponseData
    {
        public ErrorItemRequestResponseData[] errors;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public FailRequestResponseData()
		{

		}
    }

    public class UserModelData
    {
        public long identity;
        public long manager_id;
        public string manager_login;
        public string email;
        public string login;
        public string[] rules;
        public string first_name;
        public string parent_name;
        public string phone_number;
        public string gender;
        public string address;
        public int birthday;
        public int created_at;
        public int verification_at;
        public bool online;
        public double balance;
        public int credit_limit;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public UserModelData()
		{

		}
    }

    public class MarkAction
    {
        public string uuid;
        public int created_at;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public MarkAction()
		{

		}
    }

    public class LogoutForm : MarkAction
    {
        public long user_id;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public LogoutForm()
		{

		}
    }

    public class LoginForm : MarkAction
    {
        public string username;
        public string password;
        public string login_ip;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public LoginForm()
		{

		}
    }

    public class ErrorItemRequestResponseData
    {
        public string property;
        public string message;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public ErrorItemRequestResponseData()
		{

		}
    }

	public class GetGamesRequestResponseData
	{
		public string status;
		public GameModelData[] result;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public GetGamesRequestResponseData()
		{

		}
	}

	public class GameModelData
	{
		public int id;
		public string unique_code;
		public string provider_name;
		public int provider_id;
		public string name;
		public int rtp;
		public int[] rtp_available;
		public string category;
		public string icon;
		public bool enabled;
		public string age_range;
		public int gender_preference;
		public string background_color;
		public string version;
		public GameFilesModelData[] files;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public GameModelData()
		{

		}
	}

	public class GameFilesModelData
	{
		public int code;
		public string type;
		public string url;

		[Newtonsoft.Json.JsonConstructor][UnityEngine.Scripting.Preserve]
		public GameFilesModelData()
		{

		}
	}
}