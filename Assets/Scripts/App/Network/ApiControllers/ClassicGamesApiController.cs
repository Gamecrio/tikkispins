using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ClassicGames.WildBilly
{
    public class ClassicGamesApiController : BaseApiController
    {
        private string _apiBackendMathRoute = "/classicmobi/backend/backend_2/api-backend.php";
        private string _apiBackendRoot = "http://173.231.200.240:81";

        private long _GameId { get { return 1; } }
        private string _GameUniqueId { get { return "wild-texas"; } }
        private string _UUId { get { return "0"; } }
        private int _RTP { get; set; }

        private Dictionary<string, string> _defaultHeader = new Dictionary<string, string>()
        {
            { "Content-Type", "application/json" }
        };

        private bool _isUserDataLoaded;
        private bool _isBundleLoaded;
        private bool _isBetUpdated;
        private List<List<int>> _initialMatrix;

        public ClassicGamesApiController() : base() { }

        public override void Dispose()
        {
            base.Dispose();

#if !DEV_LOCAL
            ClassicGames.Hub.Common.GameHolderTool.GameActionEvent -= GameActionReceivedEventHandler;
#endif
        }

        public override void Init()
        {
#if !DEV_LOCAL
			// GET ACCOUNT DATA FROM HUB
			object[] accountData = ClassicGames.Hub.Common.GameHolderTool.GetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.AccountData);		
			_token = (string)accountData[0];
			_userId = (string)accountData[1];
			_RTP = (int)accountData[2];
            _apiBackendRoot = (string)ClassicGames.Hub.Common.GameHolderTool.GetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.ServerData)[0];
            ClassicGames.Hub.Common.GameHolderTool.GameActionEvent += GameActionReceivedEventHandler;
#else
            // for local test
            _userId = "1002052780033";
            _token = "7z2KeWPDnEhHpz3incXayLrSrtZk0FZK";
            _RTP = 87;
#endif

            GetUserInfo();
            _loadObjectsController.BundlesDataLoadedEvent += BundlesDataLoadedEventHandler;
        }

        public override void GetAvailableBetMultipliers(Action<string, byte[]> successCallback, Action<string> failCallback)
        {
            if (_isLocalPlayer)
                return;

            var requestData = new Dictionary<string, object>()
            {
                { "uuid", _UUId },
                { "user_id", _userId },
                //{ "created_at", 0 },
                { "token", _token },

                { "requestType", "getAvailableBetMultipliers" },
                { "api", "ClassicGames" }
            };

            SendPostRequest(string.Empty,
                requestData,
                _defaultHeader,
                successCallback,
                failCallback,
                customDomain: _apiBackendRoot + _apiBackendMathRoute);
        }

        public override void WinToBalance(double amount)
        {
            if (_isLocalPlayer)
                return;

            var requestData = new Dictionary<string, object>()
            {
                { "uuid", _UUId },
                { "user_id", _userId },
                { "created_at", 0 },
                { "amount", amount },
                { "token", _token },

                { "requestType", "winToBalance" },
                { "api", "ClassicGames" }
            };

            SendPostRequest(string.Empty,
                requestData,
                _defaultHeader,
                HandleDoubleGameSuccessfulResponse,
                HandleDoubleGameFailResponse,
                customDomain: _apiBackendRoot + _apiBackendMathRoute);
        }

        private void BundlesDataLoadedEventHandler()
        {
            _isBundleLoaded = true;
            if (_isUserDataLoaded)
            {
                OnGameDataLoadedHandler();
                MainApp.Instance.GameController.SlotsMachineController.SetSlotMachineMatrix(_initialMatrix);
                GetAvailableBets();
            }
        }

#if !DEV_LOCAL
        private void GameActionReceivedEventHandler(ClassicGames.Hub.Common.GameHolderTool.EventType type)
        {
            switch (type)
            {
                case ClassicGames.Hub.Common.GameHolderTool.EventType.PurchaseSuccess:
                    MainApp.Instance.GameController.GameIntefacePage.NeedTotalWinUpdate = true;
                    UpdateUserInfo();
                    break;
                case ClassicGames.Hub.Common.GameHolderTool.EventType.GameWinPopupClosed:
                    MainApp.Instance.GameController.SlotsMachineController.OnFiveOfAKindPopupClosed();
                    break;
				case ClassicGames.Hub.Common.GameHolderTool.EventType.Reconnected:
					MainApp.Instance.GameController.SlotsMachineController.OnReconectedHandler();
					break;
                case ClassicGames.Hub.Common.GameHolderTool.EventType.UpdateBet:
                    MainApp.Instance.GameController.SlotsMachineController.OnUpdateBetHandler();
                    break;
                case Hub.Common.GameHolderTool.EventType.ForceDispose:
                    MainApp.Instance.DataController?.Dispose();
                    MainApp.Instance.LoadObjectsController?.Dispose();
                    MainApp.Instance.GameController?.Dispose();
                    MainApp.Instance.PlayerController?.Dispose();
                    MainApp.Instance.NetworkController?.Dispose();
                    break;
                default:
                    break;
            }
        }
#endif

        public override void GetUserInfo()
        {
            if (_isLocalPlayer)
                return;

            var requestData = new Dictionary<string, object>()
            {
                { "user_id", _userId },
                { "token", _token },
                { "requestType", "userInfo" },
                { "api", "ClassicGames" }
            };

            SendPostRequest(string.Empty,
                requestData,
                _defaultHeader,
                OnCompleteRequest: GetUserInfoSuccessfulResponse,
                OnErrorRequest: GetUserInfoFailResponse,
                customDomain: _apiBackendRoot + _apiBackendMathRoute);
        }

        public void GetInitialMatrix()
        {
            var requestData = new Dictionary<string, object>()
            {
                { "requestType", "getInitialMatrix" },
                { "rtp", _RTP }
            };

            var startTime = DateTime.Now;

            SendPostRequest(string.Empty,
                requestData,
                _defaultHeader,
                (json, data) =>
                {
                    _initialMatrix = Newtonsoft.Json.JsonConvert.DeserializeObject<List<List<int>>>(json);

                    _isUserDataLoaded = true;
                    if (_isBundleLoaded)
                    {
                        OnGameDataLoadedHandler();
                        MainApp.Instance.GameController.SlotsMachineController.SetSlotMachineMatrix(_initialMatrix);
                        GetAvailableBets();
                    }

                }, (error) => { Debug.Log("get initial matrix error " + error); }, customDomain: _apiBackendRoot + _apiBackendMathRoute);
        }

        public void GetAvailableBets()
        {
            var requestData = new Dictionary<string, object>()
            {
                { "requestType", "getAvailableBets" },
                { "rtp", _RTP }
            };

            var startTime = DateTime.Now;

            SendPostRequest(string.Empty,
                requestData,
                _defaultHeader,
                (json, data) =>
                {
                    MainApp.Instance.GameController.SlotsMachineController.OnBetChange += OnBetChangedEventHandler;
                    MainApp.Instance.GameController.SlotsMachineController.SetBets(Newtonsoft.Json.JsonConvert.DeserializeObject<int[]>(json));

                }, (error) => { Debug.Log("get Available Bets error " + error); }, customDomain: _apiBackendRoot + _apiBackendMathRoute);
        }

        public override void Bet(double amount, Action<bool> callback)
        {
            if (_isLocalPlayer)
                return;

            Debug.Log("Bet: multiplier = " + +MainApp.Instance.GameController.SlotsMachineController.Multiplier);

#if !DEV_LOCAL
            if (_isBetUpdated)
            {
                ClassicGames.Hub.Common.GameHolderTool.SetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.UpdateJackpot, new object[] { false });
                _isBetUpdated = false;
            }
#endif

            var requestData = new Dictionary<string, object>()
            {
                { "uuid", _UUId },
                { "user_id", _userId },
                { "game_id", _GameId },
                { "game_unique_code", _GameUniqueId},
                { "bet", MainApp.Instance.GameController.SlotsMachineController.currentBetIndex },
                { "token", _token },
                { "api", "ClassicGames" },
                { "requestType", "bet" },
                { "linesCount", MainApp.Instance.GameController.SlotsMachineController.currentLines },
                { "multiplier", MainApp.Instance.GameController.SlotsMachineController.Multiplier },
                { "bonusGameEnabled", true },
                { "resultLine", MainApp.Instance.GameController.SlotsMachineController.ResultLine },
                { "freeSpin", MainApp.Instance.GameController.SlotsMachineController.freeSpin },
                { "rtp", _RTP },
                { "jackpotStatus", MainApp.Instance.GameController.SlotsMachineController.JackpotStatus }
            };

            SendPostRequest(string.Empty,
                          requestData,
                          _defaultHeader,
                          (text, data) =>
                          {
                              if (MainApp.Instance.GameController.IsDoubleGame)
                              {
                                  UpdateBetSuccessfulResponse(text, data);
                                  callback?.Invoke(true);
                              }
                              else
                              {
                                  OnBetSuccessfulResponce(text, data, amount, callback);
                              }

                          },
                          UpdateBetFailResponse,
                          customDomain: _apiBackendRoot + _apiBackendMathRoute);
        }

        private void OnBetChangedEventHandler(int bet)
        {
            _isBetUpdated = true;
        }

        public override void HandleDoubleGame(double amount, bool correct)
        {
            //if (_isLocalPlayer)
            //	return;

            //var requestData = new Dictionary<string, object>()
            //{
            //	{ "uuid", _UUId },
            //	{ "user_id", _userId },
            //	{ "game_id", _GameId },
            //	{ "game_unique_code", _GameUniqueId},
            //	{ "bet", amount },
            //	{ "token", _token },

            //	{ "correct", correct },
            //	{ "requestType", "handleDoubleGame" },
            //	{ "api", "ClassicGames" }
            //};

            //SendPostRequest(string.Empty,
            //	requestData,
            //	_defaultHeader,
            //	HandleDoubleGameSuccessfulResponse,
            //	HandleDoubleGameFailResponse,
            //	customDomain: _apiBackendRoot + _apiBackendMathRoute);
        }

        private void UpdateUserInfo()
        {
            if (_isLocalPlayer)
                return;

            var requestData = new Dictionary<string, object>()
            {
                { "user_id", _userId },
                { "token", _token },
                { "requestType", "userInfo" },
                { "api", "ClassicGames" }
            };

            SendPostRequest(string.Empty,
                requestData,
                _defaultHeader,
                OnCompleteRequest: UpdateUserInfoSuccessfulResponse,
                OnErrorRequest: UpdateUserInfoFailResponse,
                customDomain: _apiBackendRoot + _apiBackendMathRoute);
        }

        protected override void HandleDoubleGameFailResponse(string error)
        {
            MainApp.Instance.NetworkController.OnPurchaseFailedEvent?.Invoke();
        }

        protected override void HandleDoubleGameSuccessfulResponse(string json, byte[] data)
        {
            DoubleGameResponseModel doubleGameResponseModel = null;

            try
            {
                doubleGameResponseModel = Newtonsoft.Json.JsonConvert.DeserializeObject<DoubleGameResponseModel>(json);
            }
            catch (Exception ex)
            {
                Debug.LogError("HandleDoubleGameSuccessfulResponse: Invalid json! " + ex.Message + "\n" + ex.StackTrace + "\n" + json);

                doubleGameResponseModel = new DoubleGameResponseModel();
                doubleGameResponseModel.status = false;
            }

            if (!doubleGameResponseModel.status)
                return;

            MainApp.Instance.GameController.PlayerMoney.ChangeMoney(doubleGameResponseModel.balance, doubleGameResponseModel.win_balance);

            MainApp.Instance.NetworkController.OnPurchaseSuccessfulEvent?.Invoke();
        }

        protected override void GetUserInfoSuccessfulResponse(string json, byte[] data)
        {
            PlayerResponseModel userRRD = Newtonsoft.Json.JsonConvert.DeserializeObject<PlayerResponseModel>(json);
            if (userRRD == null || userRRD.user == null)
            {
                Debug.LogError(json);
                return;
            }

            Player player = new Player();
            player.balance = userRRD.user.balance;
            player.winBalance = userRRD.user.win_balance;

            _playerController.SetUserData(player);

            GetInitialMatrix();
        }

        private void UpdateUserInfoSuccessfulResponse(string json, byte[] data)
        {
            PlayerResponseModel userRRD = Newtonsoft.Json.JsonConvert.DeserializeObject<PlayerResponseModel>(json);
            if (userRRD.user == null)
            {
                Debug.LogError(json);
                return;
            }

            MainApp.Instance.GameController.PlayerMoney.ChangeMoney(userRRD.user.balance, userRRD.user.win_balance);
        }

        private void UpdateUserInfoFailResponse(string error)
        {
        }

        private void OnBetSuccessfulResponce(string json, byte[] bytes, double bet, Action<bool> callback)
        {
            SlotBackendResponseModel slotBackendResponseModel = Newtonsoft.Json.JsonConvert.DeserializeObject<SlotBackendResponseModel>(json);

            BetResponseModel betResponseModel = null;

            try
            {
                betResponseModel = Newtonsoft.Json.JsonConvert.DeserializeObject<BetResponseModel>(slotBackendResponseModel.betJson);
            }
            catch (Exception ex)
            {
                Debug.LogError("OnBetSuccessfulResponce: Invalid json! " + ex.Message + "\n" + ex.StackTrace + "\n" + json);

                betResponseModel = new BetResponseModel();
                betResponseModel.status = false;
            }

            if (!betResponseModel.status)
            {
                callback?.Invoke(false);
                return;
            }

#if !DEV_LOCAL
			if (betResponseModel.jackpot_won_resp != null)
			{
				if (betResponseModel.jackpot_won_resp.value > 0)
				{
					// THROW JACKPOT EVENT TO HUB
					ClassicGames.Hub.Common.GameHolderTool.ThrowGameActionEvent(ClassicGames.Hub.Common.GameHolderTool.EventType.Jackpot,
						betResponseModel.jackpot_won_resp.level,
						betResponseModel.jackpot_won_resp.value);
                    MainApp.Instance.GameController.GameIntefacePage.NeedTotalWinUpdate = true;
                    MainApp.Instance.GameController.GameIntefacePage.StopAutospins();
                    UpdateUserInfo();
				}
			}
#endif

            if (MainApp.Instance.GameController.SlotsMachineController.freeSpin)
            {
                callback?.Invoke(true);
            }
            else
            {
                callback?.Invoke(betResponseModel.balance >= bet);

                MainApp.Instance.GameController.PlayerMoney.ChangeMoney(betResponseModel.balance, betResponseModel.win_balance);
            }

#if !DEV_LOCAL
            // SET JACKPOT TIME TO HUB
            ClassicGames.Hub.Common.GameHolderTool.SetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.UpdateJackpot, new object[]
			{
				betResponseModel.jackpot_activation_time,
				betResponseModel.current_time,
                MainApp.Instance.GameController.SlotsMachineController.TotalBet
			});
#endif

            if (!string.IsNullOrEmpty(slotBackendResponseModel.matrixJson))
            {
                OnGotSlotMatrixResponse(slotBackendResponseModel.matrixJson, null);
            }
        }

        private void OnGotSlotMatrixResponse(string json, byte[] bytes)
        {
            MainApp.Instance.StartCoroutine(InternalTools.ParseModel(json, (spinResultData) =>
            {
                if (spinResultData == null || spinResultData.status == false)
                {
                    spinResultData = MainApp.Instance.GameController.SlotsServer.GetFailedSpinData();

                    Debug.LogError("Failed to receive data! " + "\n" + json);
                }

#if UNITY_EDITOR
                if (Input.GetKey(KeyCode.KeypadMinus))
                {
                    Debug.Log("Bonus game test");

                    spinResultData = new SpinResultData();
                    spinResultData.isBonusGame = 1;
                    spinResultData.statusWin = 0;
                    spinResultData.statusBet = 0;
                    spinResultData.shellInfo = new SpinResultWinItem[3];
                    spinResultData.shellInfo[0] = new SpinResultWinItem();
                    spinResultData.shellInfo[1] = new SpinResultWinItem();
                    spinResultData.shellInfo[2] = new SpinResultWinItem();
                    spinResultData.shellInfo[0].col = 0;
                    spinResultData.shellInfo[0].num = 12;
                    spinResultData.shellInfo[0].pos = 35;
                    spinResultData.shellInfo[1].col = 1;
                    spinResultData.shellInfo[1].num = 12;
                    spinResultData.shellInfo[1].pos = 8;
                    spinResultData.shellInfo[2].col = 3;
                    spinResultData.shellInfo[2].num = 12;
                    spinResultData.shellInfo[2].pos = 13;
                    spinResultData.resultMatrix = new int[] { 35, 8, 7, 14, 6 };
                    spinResultData.totalReward = 0;
                    spinResultData.winLines = new SpinResultWinInfoItem[0];
                    spinResultData.currentBalance = _playerController.Money;
                    spinResultData.status = true;
                }
#endif

                MainApp.Instance.GameController.SlotsServer.HandleBetResponse(spinResultData);
                MainApp.Instance.GameController.SlotsMachineController.HandleBetResponse(spinResultData);
            }));
        }

        private class GameConfig
        {
            public string backendUrl;
            public string game_uuid;

            [UnityEngine.Scripting.Preserve]
            public GameConfig()
            {
            }

            [Newtonsoft.Json.JsonConstructor]
            [UnityEngine.Scripting.Preserve]
            public GameConfig(string backendUrl, string game_uuid)
            {
                this.backendUrl = backendUrl;
                this.game_uuid = game_uuid;
            }
        }

        private class PlayerResponseModel
        {
            public User user;

            [Newtonsoft.Json.JsonConstructor]
            [UnityEngine.Scripting.Preserve]
            public PlayerResponseModel(User user)
            {
                this.user = user;
            }

            public class User
            {
                public double balance;
                public double win_balance;
                public string message;

                [UnityEngine.Scripting.Preserve]
                public User()
                {
                }

                [UnityEngine.Scripting.Preserve]
                public User(double balance, double win_balance)
                {
                    this.balance = balance;
                    this.win_balance = win_balance;
                }

                [Newtonsoft.Json.JsonConstructor]
                [UnityEngine.Scripting.Preserve]
                public User(double balance, double win_balance, string message)
                {
                    this.balance = balance;
                    this.win_balance = win_balance;
                    this.message = message;
                }
            }
        }

        private class SlotBackendResponseModel
        {
            public string matrixJson;
            public string betJson;

            [UnityEngine.Scripting.Preserve]
            public SlotBackendResponseModel()
            {
            }

            [Newtonsoft.Json.JsonConstructor]
            [UnityEngine.Scripting.Preserve]
            public SlotBackendResponseModel(string matrixJson, string betJson)
            {
                this.matrixJson = matrixJson;
                this.betJson = betJson;
            }
        }

        private class BetResponseModel
        {
            public string id;
            public double balance;
            public double win_balance;
            public double rtp;
            public bool status;
            public long jackpot_activation_time;
            public long current_time;
            public JackpotResponse jackpot_won_resp;



            [UnityEngine.Scripting.Preserve]
            public BetResponseModel()
            {
            }

            [Newtonsoft.Json.JsonConstructor]
            [UnityEngine.Scripting.Preserve]
            public BetResponseModel(string id, double balance, double win_balance, double rtp, bool status, long jackpot_activation_time, long current_time, JackpotResponse jackpot_won_resp)
            {
                this.id = id;
                this.balance = balance;
                this.win_balance = win_balance;
                this.rtp = rtp;
                this.status = status;
                this.jackpot_activation_time = jackpot_activation_time;
                this.current_time = current_time;
                this.jackpot_won_resp = jackpot_won_resp;
            }


            public class JackpotResponse
            {
                public int level;
                public double value;

                [UnityEngine.Scripting.Preserve]
                public JackpotResponse()
                {
                }

                [Newtonsoft.Json.JsonConstructor]
                [UnityEngine.Scripting.Preserve]
                public JackpotResponse(int level, double value)
                {
                    this.level = level;
                    this.value = value;
                }
            }
        }

        private class DoubleGameResponseModel
        {
            public string id;
            public double balance;
            public double win_balance;
            public bool status;
            public double earned;

            [UnityEngine.Scripting.Preserve]
            public DoubleGameResponseModel()
            {
            }

            [Newtonsoft.Json.JsonConstructor]
            [UnityEngine.Scripting.Preserve]
            public DoubleGameResponseModel(string id, double balance, double win_balance, bool status, double earned)
            {
                this.id = id;
                this.balance = balance;
                this.win_balance = win_balance;
                this.status = status;
                this.earned = earned;
            }
        }

        private class PlayerResponseBonusModel
        {
            public double amount;
            public double max;
            public double bonus;

            [UnityEngine.Scripting.Preserve]
            public PlayerResponseBonusModel()
            {
            }

            [Newtonsoft.Json.JsonConstructor]
            [UnityEngine.Scripting.Preserve]
            public PlayerResponseBonusModel(double amount, double max, double bonus)
            {
                this.amount = amount;
                this.max = max;
                this.bonus = bonus;
            }
        }
    }
}