﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace ClassicGames.WildBilly
{
	public abstract class BaseApiController
	{
		public event Action OnGameDataLoadedEvent;
		protected virtual string DOMAIN { get { return ""; } }
		protected virtual string GET_USER_INFO { get { return ""; } }
		protected virtual string BET { get { return ""; } }
		protected virtual string WIN { get { return ""; } }
		protected virtual long GAME_ID { get { return 0; } }
		protected virtual string GAME_UNIQUE_CODE { get { return ""; } }
		protected virtual string UUID { get { return ""; } }

		protected string _url;

		protected string _token;

		protected string _userId;

		protected bool _isLocalPlayer;

		protected PlayerController _playerController;
		protected LoadObjectsController _loadObjectsController;

		public BaseApiController()
		{
			_playerController = MainApp.Instance.PlayerController;
			_loadObjectsController = MainApp.Instance.LoadObjectsController;
		}

		public virtual void Init()
		{
			string url = Application.absoluteURL;

			GetUserIdFromUrl(url);
		}

		public virtual void GetUserIdFromUrl(string url)
		{

		}

		public virtual void GetUserInfo()
		{

		}

		public virtual void Bet(double amount, Action<bool> callback)
		{

		}

		public virtual void HandleDoubleGame(double bet, bool correct)
		{

		}

        public virtual void GetAvailableBetMultipliers(Action<string, byte[]> successCallback, Action<string> failCallback)
        {

        }

        public virtual void WinToBalance(double amount)
        {

        }

        public void SendPostRequest(string name, object param = null, Dictionary<string, string> headers = null,
								Action<string, byte[]> OnCompleteRequest = null, Action<string> OnErrorRequest = null, string customDomain = "")
		{
			string url = (string.IsNullOrEmpty(customDomain) ? DOMAIN : customDomain) + name;
			CallRequest(url, UnityWebRequest.kHttpVerbPOST, param, headers, OnCompleteRequest, OnErrorRequest);
		}

		public void SendGetRequest(string name, object param = null, Dictionary<string, string> headers = null,
								Action<string, byte[]> OnCompleteRequest = null, Action<string> OnErrorRequest = null)
		{
			string url = DOMAIN + name;
			CallRequest(url, UnityWebRequest.kHttpVerbGET, param, headers, OnCompleteRequest, OnErrorRequest);
		}

		protected void CallRequest(string url, string method, object param = null, Dictionary<string, string> headers = null,
								Action<string, byte[]> OnCompleteRequest = null, Action<string> OnErrorRequest = null, int timeout = 30)
		{
			DownloadHandler downloader = new DownloadHandlerBuffer();
			UnityWebRequest request = new UnityWebRequest
			{
				url = url,
				downloadHandler = downloader,
				timeout = timeout
			};

			switch (method)
			{
				case UnityWebRequest.kHttpVerbPUT:
				case UnityWebRequest.kHttpVerbPOST:
				case UnityWebRequest.kHttpVerbGET:
					if (param != null)
					{
						var jsonStr = EncryptRequestParameters(param);
						byte[] sendBytes = Encoding.ASCII.GetBytes(jsonStr);
						request.uploadHandler = new UploadHandlerRaw(sendBytes);
					}
					break;
				default:
					break;
			}

			request.method = method;

			if (headers != null)
			{
				foreach (var header in headers)
				{
					request.SetRequestHeader(header.Key, header.Value);
				}
			}

			MainApp.Instance.StartCoroutine(SendWebRequestWaiter(request, OnCompleteRequest, OnErrorRequest));
		}

		private IEnumerator SendWebRequestWaiter(UnityWebRequest req, Action<string, byte[]> OnCompleteRequest = null, Action<string> OnErrorRequest = null)
		{
			yield return req.SendWebRequest();
			if (req.isNetworkError || req.isHttpError)
			{
				OnErrorRequest?.Invoke(req.error);
			}
			else
			{
				OnCompleteRequest?.Invoke(req.downloadHandler.text, req.downloadHandler.data);
			}

			req.Dispose();
		}

		#region Responses
		protected virtual void GetUserInfoSuccessfulResponse(string text, byte[] data)
		{
		}

		protected virtual void GetUserInfoFailResponse(string error)
		{
			SetLocalPlayer();
		}

		protected virtual void UpdateBetSuccessfulResponse(string text, byte[] data)
		{
		}

		protected virtual void UpdateBetFailResponse(string error)
		{
		}

		protected virtual void HandleDoubleGameSuccessfulResponse(string text, byte[] data)
		{
		}

		protected virtual void HandleDoubleGameFailResponse(string error)
		{
		}

		protected void SetLocalPlayer()
		{
			_isLocalPlayer = true;
			Player player = new Player();
			player.balance = 0;
			_playerController.SetUserData(player);
		}

		protected void OnGameDataLoadedHandler()
		{
			OnGameDataLoadedEvent?.Invoke();
		}

        public virtual void Dispose()
        {

        }

		#endregion

		public string EncryptRequestParameters(object parameters)
		{
			return CryptRequestData(Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(parameters))));
		}

		private string CryptRequestData(string data)
		{
			int firstCount = 10, lastCount = 4;
			string last_4_string = data.Substring(data.Length - lastCount);
			string first_10_string = data.Substring(0, firstCount);

			data = last_4_string + data.Substring(firstCount, data.Length - (firstCount + lastCount)) + first_10_string;

			return data;
		}
	}
}