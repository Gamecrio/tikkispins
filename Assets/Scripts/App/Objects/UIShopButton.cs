﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
    public class UIShopButton
    {
        public GameObject SelfObject { get; private set; }
        public float Amount { get; private set; }

        private Button _btn;

        public UIShopButton(Transform transform, float amount)
        {
            Amount = amount;
            SelfObject = transform.gameObject;
            _btn = SelfObject.GetComponent<Button>();
            _btn.onClick.AddListener(OnClickHandler);
        }

        private void OnClickHandler()
        {
            Debug.Log("Adding " + Amount);
        }

        public void Dispose()
        {
            if (SelfObject != null)
                GameObject.Destroy(SelfObject);
        }
    }
}