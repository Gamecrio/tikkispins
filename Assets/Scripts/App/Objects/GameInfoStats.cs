﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClassicGames.WildBilly
{
    #region Model

    [System.Serializable]
    public class GameInfoStatsModel
    {
        public GameInfoStatsItem[] itemsRewards;
        public int[] multipliers;
    }

    [System.Serializable]
    public class GameInfoStatsItem
    {
        public GameInfoStatsRewardItem[] rewards;
        public int id, type;
    }

    [System.Serializable]
    public class GameInfoStatsRewardItem
    {
        public int count, money;
    }

    #endregion

    public class GameInfoStats
    {
        private GameInfoStatsModel _currentInfo = null;
        private bool _updateStats = false;

        private Dictionary<int, UIGameInfoStatsItem> _items = new Dictionary<int, UIGameInfoStatsItem>();

        public GameInfoStats()
        {

        }

        #region Requests & Responses

        public void GetDataFromServer()
        {
            if (_currentInfo != null)
            {
                return;
            }

            //Debug.Log("GetDataFromServer...");
            MainApp.Instance.NetworkController.CurrentApiController.GetAvailableBetMultipliers(OnGetAvailableBetMultipliersSuccess, OnGetAvailableBetMultipliersFail);
        }

        private void OnGetAvailableBetMultipliersSuccess(string json, byte[] data)
        {
            //Debug.Log("Success!");
            //Debug.Log(json);

            _currentInfo = JsonConvert.DeserializeObject<GameInfoStatsModel>(json);

            if (_updateStats)
            {
                _updateStats = false;
                UpdateStats();
            }
        }

        private void OnGetAvailableBetMultipliersFail(string error)
        {
            Debug.Log("OnGetAvailableBetMultipliersFail, ERROR: " + error);
            _updateStats = true;
        }

        #endregion

        #region UI

        public void UpdateStats()
        {
            if (_currentInfo == null)
            {
                _updateStats = true;
                GetDataFromServer();
            }
            else
            {
                int betIndex = MainApp.Instance.GameController.SlotsMachineController.currentBetIndex;
                //Debug.Log("Updating stats, betIndex = " + betIndex);

                if (_currentInfo.multipliers == null)
                {
                    Debug.LogError("_currentInfo.multipliers is null");
                    return;
                }

                if (betIndex < 0 || betIndex >= _currentInfo.multipliers.Length)
                {
                    Debug.LogError("Invalid betIndex = " + betIndex + ", length = " + _currentInfo.multipliers.Length);
                    return;
                }

                foreach (var rewardsList in _currentInfo.itemsRewards)
                {
                    if (_items.ContainsKey(rewardsList.id))
                    {
                        _items[rewardsList.id].UpdateInfo(rewardsList.rewards, _currentInfo.multipliers[betIndex]);
                    }
                    else
                    {
                        Debug.LogError("The items list doesn't have the key: " + rewardsList.id);
                    }
                }
            }
        }

        public void AddItem(int id, UIGameInfoStatsItem item)
        {
            if (_items.ContainsKey(id) == false)
            {
                _items.Add(id, item);
            }
            else
            {
                Debug.LogError("AddItem: the list already has an item with id: " + id);
            }
        }

        #endregion
    }

    public class UIGameInfoStatsItem
    {
        private Dictionary<int, TMPro.TextMeshProUGUI> _labels = new Dictionary<int, TMPro.TextMeshProUGUI>();
        private Transform _container;

        public UIGameInfoStatsItem(Transform multipliersContainer)
        {
            _container = multipliersContainer;

            int labelId;
            for (int i = 0; i < multipliersContainer.childCount; i++)
            {
                if (int.TryParse(multipliersContainer.GetChild(i).name, out labelId))
                {
                    if (_labels.ContainsKey(labelId) == false)
                    {
                        _labels.Add(labelId, multipliersContainer.GetChild(i).GetComponent<TMPro.TextMeshProUGUI>());
                    }
                    else
                    {
                        Debug.LogError("The Labels list already has the key: " + labelId + ", parent: " + multipliersContainer);
                    }
                }
                else
                {
                    Debug.LogError("Failed to parse to int: " + multipliersContainer.GetChild(i).name + ", parent: " + multipliersContainer);
                }
            }
        }

        public void UpdateInfo(GameInfoStatsRewardItem[] rewards, int multiplier)
        {
            if (rewards == null)
            {
                Debug.LogError("UpdateInfo: rewards is null");
                return;
            }

            foreach (var reward in rewards)
            {
                if (_labels.ContainsKey(reward.count))
                {
                    _labels[reward.count].text = (reward.money * multiplier).ToString();
                }
                else
                {
                    Debug.LogError("The Labels list doesn't have the key: " + reward.count + ", parent: " + _container);
                }
            }
        }
    }
}