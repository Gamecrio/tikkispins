﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClassicGames.WildBilly
{
    public class ShellBonusGameItem
    {
        private const float SpeedForAnimation = 6;

        public bool IsActive { get; private set; }

        public int column;
        public int number;
        public GameObject selfObject;
        private AnimationSequence _animationSequence;
        private SlotsItem _slotsItem;

        public ShellBonusGameItem(Transform parent, int column, int number, GameObject prefab, Sprite[] animationFrames, SlotsItem slotsItem)
        {
            this.column = column;
            this.number = number;

            selfObject = GameObject.Instantiate(prefab, parent, false);

            Vector3 localScale = selfObject.transform.localScale;
            selfObject.transform.SetParent(parent, false);

            _animationSequence = selfObject.GetComponent<AnimationSequence>();
            _animationSequence.LoadFrames(animationFrames);
            _animationSequence.Play(SpeedForAnimation, true);
            _slotsItem = slotsItem;
            _slotsItem.spriteRenderer.enabled = false;
            IsActive = false;
        }

        public void Dispose()
        {
            if (selfObject != null)
            {
                MonoBehaviour.Destroy(selfObject);
            }
            if (_slotsItem != null)
            {
                _slotsItem.spriteRenderer.enabled = true;
            }
        }
    }
}
