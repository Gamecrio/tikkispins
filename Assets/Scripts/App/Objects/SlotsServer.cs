﻿using System.Linq;

namespace ClassicGames.WildBilly
{
    public class SlotsServer
    {
        public int WinLinesCount { get; private set; }
        public bool IsBonusGame { get; set; }
        public bool IsBonusGameOpen { get; set; }

        private PlayerMoney _playerMoney;

        private SlotsMachineController SlotsMachineController;

        private GameController _gameController;

        public int currentLineCount = 25;

        public double earnForSpin = 0;

        private float _currentCoefForShells;


        public SlotsServer(GameController SlotsGameController)
        {
            _gameController = SlotsGameController;
        }

        public void Init()
        {
            SlotsMachineController = _gameController.SlotsMachineController;
            _playerMoney = _gameController.PlayerMoney;
        }

        public SpinResultData GetFailedSpinData()
        {
            var spinResultData = new SpinResultData();
            spinResultData.statusWin = 0;
            spinResultData.statusBet = 0;
            spinResultData.shellInfo = new SpinResultWinItem[0];
            spinResultData.resultMatrix = new int[] { 0, 0, 0, 0, 0 };
            spinResultData.totalReward = 0;
            spinResultData.winLines = new SpinResultWinInfoItem[0];
            spinResultData.currentBalance = MainApp.Instance.PlayerController.Money;
            spinResultData.currentWinBalance = MainApp.Instance.PlayerController.TotalWinMoney;
            spinResultData.status = true;

            return spinResultData;
        }

        public void HandleBetResponse(SpinResultData data)
        {
            SlotsMachineController.DoActionWhenSpinEnd(() => _playerMoney.ChangeMoney(data.currentBalance, data.currentWinBalance));

            WinLinesCount = (data.winLines != null ? data.winLines.Length : 0);
        }

        public double GetEarnForSpin(SpinResultData data)
        {
            SlotsMachineController.ResetLineSequence();
            SlotsMachineController.AddBonusesAsWinFrames();

            for (int i = 0; i < data.winLines.Length; i++)
            {
                SlotsMachineController.ShowCertainLines(data.winLines[i].lineIndex, data.winLines[i]);
            }
            SlotsMachineController.FinishedSpinHandler();

            earnForSpin = data.totalReward;

            return earnForSpin;
        }

        public void Dispose()
        {
        }
    }
}
