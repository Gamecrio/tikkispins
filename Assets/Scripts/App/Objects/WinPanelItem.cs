﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace ClassicGames.WildBilly
{
    public class WinPanelItem
    {
        public int lineNumber;
        public double winValue;
        public List<int> Frames => _frames;
        public Color LineColor => _lineColor;

        private GameObject _lineObject;
        private readonly List<int> _frames;
        private Color _lineColor;

        private DotItem _dot;

        public WinPanelItem(GameObject lineObject, List<int> frameIndexes, Color lineColor, DotItem dot, int lineNumber, double winValue)
        {
            this.lineNumber = lineNumber;
            this.winValue = winValue;
            _lineObject = lineObject;
            _frames = frameIndexes;
            _lineColor = lineColor;
            _dot = dot;
        }

        public void ChangeState(bool state)
        {
            if (_lineObject != null && _dot != null)
            {
                _lineObject.SetActive(state);
                _dot.ChangeGlowState(state);
            }
        }

        public void Dispose()
        {
            ChangeState(false);
        }
    }

    public class DotItem
    {
        private SpriteRenderer _glow;

        public DotItem(GameObject obj, string number)
        {
            obj.transform.Find("Text").GetComponent<TextMeshPro>().text = number;
            _glow = obj.transform.Find("Glow").GetComponent<SpriteRenderer>();
            ChangeGlowState(false);
        }

        public void ChangeGlowState(bool state)
        {
            _glow.enabled = state;
        }
    }
}
