﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;
using ClassicGames.WildBilly.Common;
using System.Collections;

namespace ClassicGames.WildBilly
{
    public class SlotsItem
    {
        public const float cellHeight = 1.65f;

		public event Action<int> OnMoveEndedEvent;

        public Sprite InitialSprite { get; private set; }

		public GameObject selfObject;
        public Transform selfTransform;
        public SpriteRenderer spriteRenderer;
        public Sprite sprite;
        public int number;
        public int posNum;
        
        private Vector3 _initialScale;
        private int _currentScaleCycle;

		public SlotsItem(GameObject prefab, Transform container, Sprite sprite, int number, int posNum)
        {
			selfObject = GameObject.Instantiate(prefab, container);
			selfObject.name = "Item_Slot_" + posNum + "_(" + number + ")";
			selfTransform = selfObject.transform;
            selfTransform.localPosition = Vector3.zero;
            Vector3 pos = selfTransform.localPosition;
            pos.y += ((posNum - 1) * cellHeight);
            selfTransform.localPosition = pos;
            this.number = number;
            this.posNum = posNum;
            this.sprite = InitialSprite = sprite;
            spriteRenderer = selfTransform.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = sprite;
            spriteRenderer.sortingOrder = 5;

            _initialScale = selfTransform.localScale;  //selfTransform.localScale = MainApp.Instance.GameController.SlotsScaleController.GetScale(number);
        }

        //public void PlayScaleAnimation()
        //{
        //    _currentScaleCycle = 2;
        //    selfTransform.DOKill();
        //    PlayScaleUp();
        //}

        //private void PlayScaleUp()
        //{
        //    selfTransform.DOScale(_initialScale.x * 1.3f, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
        //    {
        //        PlayScaleDown();
        //    });
        //}

        //private void PlayScaleDown()
        //{
        //    _currentScaleCycle--;
        //    selfTransform.DOScale(_initialScale.x, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
        //    {
        //        if (_currentScaleCycle > 0)
        //            PlayScaleUp();
        //    });
        //}

        public void Dispose()
        {
            if (selfObject != null)
            {
                DOTween.Kill(selfObject);
                GameObject.Destroy(selfObject);
            }
        }
    }

	public enum ColumnState
	{
		None,
		Active,
		EndMove,
		Stopped
	}

	public class BlockOfSlots
	{
		public GameObject selfObject;
		public Transform SelfTransform => selfObject.transform;
		public event Action<int> OnMoveEndedEvent;

        public float Size { get; private set; }

        public int number;
		public int id;
		public float yBoundary = -48.6f;
		public int spinsDone = 0;
		public float oneCellMoveementDuration = 0.035f * 1;

		private bool _isReset;

		private bool _moveImmediately;

		private SoundController _soundController;

		//public bool punchAtEnd;

		private Vector3 _start;
		private float _speed = 1f;
		private float _immediateMoveSpeedCoeff = 0.85f;
		private float _immediateMoveSpeed;
		
		private int _columnId;

        private bool _startingSpin = false;
        private bool _isFinishing = false;
        
        private Vector3 _targetFinishPosition;

        private Sequence _startMoveSequence, _bonusPunchSequence, _fastStopSequence;

        private bool _isBonusGameStopAnimation = false;
        private bool _stoppedAfterSpin, _punchFinished = true, _fastStop = false;
        
		public BlockOfSlots(Transform container, int id, int amountOfTiles, int columnId)
		{
            _columnId = columnId;
			selfObject = new GameObject($"Block_{id}");
			selfObject.transform.SetParent(container, false);
			this.id = id;
			_immediateMoveSpeed = SlotsItem.cellHeight / oneCellMoveementDuration * _immediateMoveSpeedCoeff;
			_soundController = MainApp.Instance.GameController.SoundController;


            Size = amountOfTiles * SlotsItem.cellHeight;
            selfObject.transform.localPosition = Vector3.up * (Size * id);
		}

		public void MoveUp(int columnIndex)
		{
            if (!_punchFinished)
                ForceEndParentEffects();

            _stoppedAfterSpin = false;
            _moveImmediately = true;
            _start = selfObject.transform.localPosition;
            _isFinishing = false;
            _startingSpin = true;
            SelfTransform.DOKill();
            InternalTools.KillSequence(_bonusPunchSequence);
            InternalTools.KillSequence(_startMoveSequence);
            InternalTools.KillSequence(_fastStopSequence);

            SelfTransform.DOKill();
            SelfTransform.DOLocalMoveY(_start.y + 1f, 0.4f).OnComplete(() =>
            {
                _startingSpin = false;
                _start = selfObject.transform.localPosition;
            });
        }

		public void UpdateMovementState(bool state)
		{
			_moveImmediately = state;
		}

        public void StopFastWithDelay(float deltaMovement)
        {
            StopMove(deltaMovement, true);
        }

        private float GetLowerThreshold()
        {
            return -(Size + (SlotsItem.cellHeight * 3));
        }

        public void StopMove(float deltaMovement, bool fastStop = false)
		{
            if (_stoppedAfterSpin)
                return;

            _stoppedAfterSpin = true;
            _fastStop = fastStop;

            //Debug.Log("StopMove, parent = " + SelfTransform.parent.parent.name);

            InternalTools.KillSequence(_startMoveSequence);

            if (_fastStop)
            {
                //SelfTransform.DOKill();

                _targetFinishPosition = SelfTransform.localPosition;

                _targetFinishPosition -= Vector3.up * deltaMovement;
                if (_targetFinishPosition.y <= GetLowerThreshold())
                {
                    _targetFinishPosition += Vector3.up * Size * Constants.BlockCount;
                }
                else if (_targetFinishPosition.y >= Size)
                {
                    _targetFinishPosition -= Vector3.up * Size * Constants.BlockCount;
                }

                _isFinishing = true;

                if (fastStop)
                {
                    SelfTransform.localPosition = _targetFinishPosition;
                    FinishStopAnimation();
                }
            }
            else
            {
                _moveImmediately = false;

                SelfTransform.localPosition -= Vector3.up * deltaMovement;
                if (SelfTransform.localPosition.y <= GetLowerThreshold())
                {
                    SelfTransform.localPosition += Vector3.up * Size * Constants.BlockCount;
                }
                else if (SelfTransform.localPosition.y >= Size)
                {
                    SelfTransform.localPosition -= Vector3.up * Size * Constants.BlockCount;
                }

                _targetFinishPosition = SelfTransform.localPosition;

                MainApp.Instance.GameController.SlotsMachineController.PlayStopSpinSoundByColumnId(_columnId);
                SelfTransform.parent.DoPunch(Vector3.down * 1.5f, 1f, callback: () =>
                {
                    OnMoveEndedEvent?.Invoke(_columnId);

                }, forceEnd: false);
            }
        }

		public void Move()
		{
            if (_startingSpin)
                return;

            if (_moveImmediately || _isFinishing)
            {
                SelfTransform.localPosition = new Vector3(SelfTransform.localPosition.x, SelfTransform.localPosition.y - _immediateMoveSpeed * Time.deltaTime * _speed, SelfTransform.localPosition.z);

                if (_isFinishing)
                {
                    float difference = Mathf.Abs(SelfTransform.localPosition.y - _targetFinishPosition.y);
                    float connectionDistance = _immediateMoveSpeed * Time.deltaTime * _speed * 1.5f; // should be relative to speed

                    if (difference <= connectionDistance)
                    {
                        //Debug.Log("CONNECTED!");
                        FinishStopAnimation();
                    }
                }
            }

            if (SelfTransform.localPosition.y <= GetLowerThreshold()) // 3 extra cells to scroll)
            {
                SelfTransform.localPosition += Vector3.up * Size * Constants.BlockCount;
            }
        }

        private void FinishStopAnimation()
        {
            //Debug.Log("Bonus: FinishStopAnimation, parent: " + SelfTransform.parent.parent.name);
            _isFinishing = false;
            _stoppedAfterSpin = true;

            //SelfTransform.DOKill();
            //SelfTransform.DOLocalMove(_targetFinishPosition + Vector3.down, 0.25f).SetEase(Ease.OutSine).OnComplete(() =>
            //{
            //    SelfTransform.DOLocalMove(_targetFinishPosition, 0.25f).SetEase(Ease.InSine).OnComplete(() =>
            //    {
            //        OnMoveEndedEvent?.Invoke(_columnId);
            //    });
            //});


            _punchFinished = false;
            SelfTransform.localPosition = _targetFinishPosition;
            SelfTransform.parent.DoPunch(Vector3.down * 0.25f, 3f, callback: () =>
            {
                _punchFinished = true;
                OnMoveEndedEvent?.Invoke(_columnId);

            }, forceEnd: false);

            //OnMoveEndedEvent?.Invoke(_columnId);

            //InternalTools.KillSequence(_bonusPunchSequence);
            //_bonusPunchSequence = InternalTools.DoActionDelayed(() => { _punchFinished = true; }, 3f);

            // to avoid winframes positioning bug
            if (_fastStop)
            {
                //InternalTools.KillSequence(_fastStopSequence);
                //_fastStopSequence = InternalTools.DoActionDelayed(() => { OnMoveEndedEvent?.Invoke(_columnId); }, 0.5f);
                MainApp.Instance.GameController.SlotsMachineController.PlayStopSpinSoundByColumnId(_columnId);
            }
        }

		public void ForceEndParentEffects()
		{
            SelfTransform.parent.ForceEndPunch();
        }

		public void SetPosition(float cellsCount)
		{
            SelfTransform.localPosition -= Vector3.up * SlotsItem.cellHeight * cellsCount;
        }

		public void Dispose()
		{
			if (selfObject != null)
			{
				DOTween.Kill(selfObject);
				GameObject.Destroy(selfObject);
			}
		}
	}

	public class ColumnItem
	{
		//public bool EndedSpining { get; set; }
		//public bool IsActive { get; set; }
		//public bool IsReadyForSpin { get; set; }
		public int Index => _index;

		public ColumnState currentState;
        public bool playedStopSound = false;

		public event Action<ColumnItem> OnColumnClickEvent;

		private SlotsMachineController _slotsMachineController;
		private GameController _gameController;

		private GameObject _selfObject;
		private SpriteRenderer _shadowBackgroundRenderer;
		//private OnBehaviourHandler _behaviourHandler;


		private int _index;
		private int _countOfBlocks;

		private bool _tappedOnColumn;

		public ColumnItem(GameObject self, int index, GameController gameController, SlotsMachineController slotsMachineController)
		{
			_gameController = gameController;
			_slotsMachineController = slotsMachineController;
			_index = index;
			_selfObject = self;
			//_behaviourHandler = _selfObject.transform.Find("Mask").GetComponent<OnBehaviourHandler>();
			_shadowBackgroundRenderer = _selfObject.transform.Find("shadow").GetComponent<SpriteRenderer>();
			//_behaviourHandler.MouseUpTriggered += MouseUpTriggeredEventHandler;
			//_behaviourHandler.MouseDownTriggered += MouseDownTriggeredEventHandler;

			Reset();
            SetShadowState(false);
		}

        public void SetShadowState(bool active)
        {
            _shadowBackgroundRenderer.gameObject.SetActive(active);
        }

		public void UpdateCountOfBlocks()
		{
			_countOfBlocks++;

			if(_countOfBlocks >= _slotsMachineController.BlockOfSlots[Index].Count)
			{
				OnColumnMoveEndedEventHandler();
			}
		}

		public void OnAllColumnSpinDoneEventHandler()
		{
			Reset();
		}

		private void OnColumnMoveEndedEventHandler()
		{
			//if (currentState != ColumnState.EndMove)
			//{
			//	Debug.LogError("Error In Moving");
			//}
            
			currentState = ColumnState.Stopped;
			if (_slotsMachineController.SingleColumnSpin)
			{
				_gameController.SoundController.StopSound(Enumerators.SoundType.Spin);
			}

            _slotsMachineController.SignalColumnEnded(this);
        }

		private void Reset()
		{
			currentState = ColumnState.None;
			_countOfBlocks = 0;
        }

		private void MouseUpTriggeredEventHandler(GameObject obj)
		{
            return;

			if (_gameController.IsDoubleGame || !_tappedOnColumn)
				return;

			_tappedOnColumn = false;

			if (currentState == ColumnState.None)
			{
				//IsReadyForSpin = true;
				OnColumnClickEvent?.Invoke(this);
			}
			else if(currentState == ColumnState.EndMove)
			{
				_gameController.SlotsMachineController.ForceEndColumnSpin(this);
			}
		}

		private void MouseDownTriggeredEventHandler(GameObject obj)
		{
			if (_gameController.IsDoubleGame)
				return;

			_tappedOnColumn = true;
		}
	}
}
