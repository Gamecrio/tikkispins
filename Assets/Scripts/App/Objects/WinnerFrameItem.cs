﻿using ClassicGames.WildBilly.Common;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace ClassicGames.WildBilly
{
    public class WinnerFrameItem
    {
        private const int IconSortingOrderFront = 20;
        private const int IconSortingOrderBack = 14;
        private const int BackSortingOrderFront = 19;
        private const int BackSortingOrderBack = 13;
        private const int SpeedForAnimation = 60;

        private const string AnimationPath = "Images/Animations/Gameplay/WinnerFrame/Layer";

        public bool IsActive { get; private set; }
        public bool isBonusElement = false;

        public float AnimationTime => _animationType == Enumerators.FrameAnimationType.FRAME_BY_FRAME ?
            (_animationSequence != null ? _animationSequence.currentFrame : 0) :
            (_selfAnimator != null ? _selfAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime : 0);

        public int index;
        public int column;
        public int posNum;
        public int number;
        public int layerIndex;

        public GameObject selfObject;

        private GameObject _mask;
        private SpriteRenderer _border,
                                 _icon,
                                 _startIcon,
                                 _back,
                                 _backBonusGame,
                                 _glow;

        private AnimationSequence _animationSequence;
        private Animator _selfAnimator;

        private Sequence _textAnimationSequence;
        private TMPro.TextMeshPro _textAmount;

        private readonly Enumerators.FrameAnimationType _animationType;

        public WinnerFrameItem(Transform parent, int index, int column, int number, int posNum, Transform frameContainer, Vector3 position, Sprite icon, Color borderColor, int layerIndex, GameObject prefab, Sprite[] animationFrames, bool isBonusGame, int winAmount)
        {
            this.index = index;
            this.column = column;
            this.number = number;
            this.posNum = posNum;
            this.layerIndex = layerIndex;

            selfObject = GameObject.Instantiate(prefab, parent, true);
            Vector3 localScale = selfObject.transform.localScale;
            selfObject.transform.position = position;
            selfObject.transform.localScale = localScale;

            _startIcon = selfObject.transform.Find("MaskIcon/Icon").GetComponent<SpriteRenderer>();
            _startIcon.sprite = icon;
            _border = selfObject.transform.Find("Frame").GetComponent<SpriteRenderer>();
            _border.color = borderColor;
            _mask = selfObject.transform.Find("Mask").gameObject;
            _mask.GetComponent<SpriteMask>().frontSortingOrder = IconSortingOrderFront + 2;
            _icon = selfObject.transform.Find("MaskIcon/IconAnim").GetComponent<SpriteRenderer>();
            _icon.sprite = icon;
            _textAmount = selfObject.transform.Find("Text_Amount").GetComponent<TextMeshPro>();
            _textAmount.text = winAmount.ToString();
            _glow = selfObject.transform.Find("MaskIcon/GlowMask/Glow").GetComponent<SpriteRenderer>();
            _back = selfObject.transform.Find("Back").GetComponent<SpriteRenderer>();
            _backBonusGame = selfObject.transform.Find("BackBonusGame").GetComponent<SpriteRenderer>();
            _animationSequence = _icon.GetComponent<AnimationSequence>();

            if (_animationSequence != null)
            {
                _animationSequence.LoadFrames(animationFrames);
            }

            _selfAnimator = selfObject.GetComponent<Animator>();
            _selfAnimator.SetFloat("LayerIndex", layerIndex);

            _backBonusGame.enabled = isBonusGame;
            _back.enabled = !isBonusGame;

            _animationType = Enumerators.FrameAnimationType.FRAME_BY_FRAME;
            switch (_animationType)
            {
                case Enumerators.FrameAnimationType.FRAME_BY_FRAME:
                    _selfAnimator.enabled = false;
                    _startIcon.enabled = false;
                    _icon.enabled = true;
                    if (_animationSequence != null)
                    {
                        _animationSequence.ResetAnimation();
                    }

                    break;
            }

            IsActive = false;

            int value = number + 1;
            Vector3 localPosition = _icon.transform.localPosition;
            Vector3 scale = _icon.transform.localScale;

            _icon.transform.localPosition = localPosition;
            _icon.transform.localScale = scale;
            _textAmount.transform.localPosition = Vector3.zero;

            ChangeBorderState(false);
        }

        private void StartTextAnimation()
        {
            float speed = 0.8f, waiting = 0.8f;
            InternalTools.KillSequence(_textAnimationSequence);
            _textAnimationSequence = null;
            _textAmount.transform.localScale = Vector3.zero;

            // 5 seconds in total
            _textAnimationSequence = DOTween.Sequence();
            _textAnimationSequence.Append(_textAmount.transform.DOScale(0.9f, speed).SetEase(Ease.OutBounce));
            _textAnimationSequence.AppendInterval(waiting);
            _textAnimationSequence.Append(_textAmount.transform.DOScale(1.3f, speed).SetEase(Ease.OutBounce));
            _textAnimationSequence.AppendInterval(waiting);
            _textAnimationSequence.Append(_textAmount.transform.DOScale(0, 0.6f).SetEase(Ease.Linear));
            _textAnimationSequence.Play();
        }

        public void SetWinTextAmount(double amount)
        {
            _textAmount.text = (int)amount + "";
        }

        int count = 0;

        public void ChangeBorderState(bool state, Color color = default(Color), float time = 0, bool disableAnim = true)
        {
            // for bonus elements we show only text animation
            if (state && isBonusElement)
                return;

            _border.enabled = state;
            _border.color = color;

            if (disableAnim)
            {
                _mask.SetActive(state);
                _icon.enabled = state;
            }

            if (state)
            {
                count++;
                _icon.sortingOrder = IconSortingOrderFront;
                _back.sortingOrder = BackSortingOrderFront;
                _glow.sortingOrder = IconSortingOrderFront + 1;
                //_numberGlow.frontSortingOrder = IconSortingOrderFront + 1;
                //_numberGlow.backSortingOrder = IconSortingOrderFront;
                _startIcon.sortingOrder = IconSortingOrderFront;

                if (_animationType == Enumerators.FrameAnimationType.GLOW)
                {
                    if (!_selfAnimator.GetBool("IsStart"))
                    {
                        _selfAnimator.SetBool("IsStart", true);
                        IsActive = true;
                    }
                    _selfAnimator.Play("WinnerAnim", 0, time);
                }
                else
                {
                    if (!IsActive)
                    {
                        IsActive = true;
                    }
                    if (_animationSequence != null)// && !_animationSequence.isPlaying)
                    {
                        if (disableAnim)
                        {
                            _animationSequence.Stop();
                            _animationSequence.ResetAnimation();
                        }
                        //Debug.Log((int)time);
                        _animationSequence.Play(SpeedForAnimation, true, (int)time);
                    }
                    //MainApp.Instance.GameController.SoundController.PlaySound(Enumerators.SoundType.WinFrame, "/SlotItem_Layer" + layerIndex, null, false);
                }

                //if (column == 0)
                //{
                //    if (count % 2 != 0)
                //    {
                //        StartTextAnimation();
                //    }

                //}

            }
            else if (disableAnim)
            {
                _icon.sortingOrder = IconSortingOrderBack;
                _back.sortingOrder = BackSortingOrderBack;
                _glow.sortingOrder = IconSortingOrderBack + 3;
                //_numberGlow.frontSortingOrder = IconSortingOrderBack + 3;
                //_numberGlow.backSortingOrder = IconSortingOrderBack + 2;
                _startIcon.sortingOrder = IconSortingOrderBack;
                _animationSequence.Stop();
                _animationSequence.ResetAnimation();
            }
        }

        public void ChangeState(bool state)
        {
            selfObject.SetActive(state);
            if (!state)
            {
                _selfAnimator?.SetBool("IsStart", false);
                IsActive = false;
            }
            else
            {
                ChangeBorderState(true, _border.color, 0, true);
                _animationSequence.Stop();
                _animationSequence.ResetAnimation();
            }
        }

        public void UpdateAnimation(bool state = true)
        {
            //Debug.Log("UpdateAnimation: " + state + ", index: " + index + ", name = " + selfObject.name);

            InternalTools.KillSequence(_textAnimationSequence, true);
            _textAnimationSequence = null;

            if (state)
                StartTextAnimation();
        }

        public void Dispose()
        {
            InternalTools.KillSequence(_textAnimationSequence);
            _textAnimationSequence = null;

            if (selfObject != null)
            {
                MonoBehaviour.Destroy(selfObject);
            }
        }
    }
}
