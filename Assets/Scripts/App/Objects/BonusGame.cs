﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using ClassicGames.WildBilly.Common;
using Newtonsoft.Json;
using UnityEngine.U2D;

namespace ClassicGames.WildBilly
{
	public class BonusGame
	{
		
		private GameObject _selfPage;
		private GameObject _gameInterfacePage;
		private GameObject _slotMachine;
		private GameController _SlotsGameController;
		private LoadObjectsController _loadObjectsController;



		private List<_rewardData> _rewardData;
		private List<BonusGameItem> _bonusGameItems;

		//private ResultScreenItem _resultScreen;

		private CanvasGroup _itemsCanvasGroup;
		private Animator _selfAnimator;

		private Image _chooseCoinImage;
		//private Image _spinleftImage;
		//private Image _multiplierImage;

		private TextMeshProUGUI _spinsLeftText, _multiplierText;
		private int _spinsLeft, _multipliers;
		private List<_rewardData> _choosenItemsList;
		public BonusGame(Transform parent, GameController SlotsGameController)
		{
			_choosenItemsList = new List<_rewardData>();
			_loadObjectsController = MainApp.Instance.LoadObjectsController;
			_SlotsGameController = SlotsGameController;

			_selfPage = parent.Find("BonusGame").gameObject;
			_gameInterfacePage = parent.Find("GameInterfacePage").gameObject;
			_slotMachine = parent.parent.Find("SlotMachine_Screen_NEW").gameObject;

			_itemsCanvasGroup = _selfPage.transform.Find("Panel_Items").GetComponent<CanvasGroup>();
			_selfAnimator = _selfPage.GetComponent<Animator>();
			_chooseCoinImage = _selfPage.transform.Find("Image_ChoseACoin").GetComponent<Image>();
			_multiplierText = _selfPage.transform.Find("Panel_Multiplier/Text_Value").GetComponent<TextMeshProUGUI>();
			_spinsLeftText = _selfPage.transform.Find("Panel_Spinleft/Text_Value").GetComponent<TextMeshProUGUI>();

			var handler = _selfPage.GetComponent<OnBehaviourHandler>();
			handler.OnAnimationStringEvent += OnAnimationStringEventHandler;

			Hide();
		}

		private void OnAnimationStringEventHandler(string name)
		{
			Debug.Log("Anim End");
			switch (name)
			{
				case "ShowEnd":
					_selfAnimator.enabled = false;
					ShowBonusItem();
					break;
				default:
					break;
			}
		}

		private void TurnOnItemButtons()
		{
			if (_bonusGameItems != null)
			{
				foreach (var item in _bonusGameItems)
				{
					item.ChangeButtonState(true);
				}
			}
		}


		private void LoadMiniGameData()
		{

			_rewardData = new List<_rewardData>();
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.FREE_SPINS, count = 10 });
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.FREE_SPINS, count = 10 });
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.FREE_SPINS, count = 10 });
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.FREE_SPINS, count = 15 });
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.FREE_SPINS, count = 15 });
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.FREE_SPINS, count = 15 });
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.FREE_SPINS, count = 20 });
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.FREE_SPINS, count = 20 });
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.FREE_SPINS, count = 20 });
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.MULTIPLIER, count = 5 });
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.MULTIPLIER, count = 5 });
			_rewardData.Add(new _rewardData() { type = BonusGameRewardType.MULTIPLIER, count = 5 });

		}

		

		public void Show()
		{
			_multipliers = 1;
			_spinsLeft = 5;
			UpdateBonusesText(_multipliers, _spinsLeft);
			_selfAnimator.enabled = true;
			_itemsCanvasGroup.alpha = 1;
			_chooseCoinImage.color = Color.white;
			_selfPage.SetActive(true);
			LoadMiniGameData();
			FillItems();
			UpdateInfo();
			//ShowBonusItem();

			MainApp.Instance.GameController.SoundController.StopSound(Enumerators.SoundType.Background);
			MainApp.Instance.GameController.SoundController.PlaySound(Enumerators.SoundType.BonusGamePopupBackground, parameters: new SoundController.SoundParameters() { Loop = true });
		}

		private void RunBonusGameAutoSpin()
		{
			_SlotsGameController.SlotsMachineController.RunBonusGame();
			MainApp.Instance.GameController.SoundController.PlaySound(Enumerators.SoundType.FreeGameBackground, parameters: new SoundController.SoundParameters() { Loop = true, Volume = 0.5f });
			Hide();
		}

		private void ShowBonusItem()
		{

			foreach (var item in _bonusGameItems)
			{
				float spawnDeley = UnityEngine.Random.Range(0, 0.5f);
				InternalTools.DoActionDelayed(item.ShowAnimate, spawnDeley);
			}
			TurnOnItemButtons();
		}

		public void Hide()
		{
			_gameInterfacePage.SetActive(true);
			_slotMachine.SetActive(true);
			_selfPage.SetActive(false);
			_choosenItemsList.Clear();
			_multipliers = 0;
			_spinsLeft = 0;

			MainApp.Instance.GameController.SoundController.StopSound(Enumerators.SoundType.BonusGamePopupBackground);
		}

		public void UpdateInfo()
		{
			_rewardData.ShuffleList();
			Debug.Log(_bonusGameItems.Count);
			for (int i = 0; i < _bonusGameItems.Count; i++)
			{
				Debug.Log(i);
				_bonusGameItems[i].UpdateRewardData(_rewardData[i]);
			}
		}

		private void FillItems()
		{
			if (_bonusGameItems != null)
			{
				foreach (var item in _bonusGameItems)
				{
					item.OnItemClickEvent -= BonusShellItemCkickEventHandler;
				}
				_bonusGameItems.Clear();
			}
			_bonusGameItems = new List<BonusGameItem>();
			BonusGameItem gameItem = null;
			var parent = _selfPage.transform.Find("Panel_Items");

			int index = 0;
			for (int i = 0; i < parent.childCount; i++)
			{
				index = i;
				gameItem = new BonusGameItem(parent.GetChild(i).gameObject, index);
				gameItem.OnItemClickEvent += BonusShellItemCkickEventHandler;
				gameItem.ChangeButtonState(false);
				_bonusGameItems.Add(gameItem);
			}
		}


		private void BonusShellItemCkickEventHandler(_rewardData data)
		{
			_choosenItemsList.Add(data);
			var freqMap = _choosenItemsList.GroupBy(x => new { x.type, x.count })
																		 .Where(g => g.Count() > 1)
																		 .ToDictionary(x => x.Key, x => x.Count());
			var RewardDataDictionary = new Dictionary<_rewardData, int>();
			foreach (var pair in freqMap)
			{
				RewardDataDictionary.Add(new _rewardData(pair.Key.type, pair.Key.count), pair.Value);
			}

			//Debug.LogWarning(String.Join(",", freqMap));

			foreach (KeyValuePair<_rewardData, int> valuePair in RewardDataDictionary)
			{
				if (valuePair.Value == 3)
				{
					if (valuePair.Key.type == BonusGameRewardType.FREE_SPINS)
					{
						_spinsLeft += valuePair.Key.count;
						_multipliers = 1;
					}
					if (valuePair.Key.type == BonusGameRewardType.MULTIPLIER)
					{
						_multipliers = valuePair.Key.count;
					}
					foreach (var item in _bonusGameItems)
					{
						item.ChangeButtonState(false);
						if (item.Data.count == valuePair.Key.count && item.Data.type == valuePair.Key.type)
						{
							InternalTools.DoActionDelayed(item.AnimateWin, 2f);
						}
						else
						{
							InternalTools.DoActionDelayed(item.AnimateLose, 2f);
						}
					}
					UpdateBonusesText(_multipliers, _spinsLeft);
					InternalTools.DoActionDelayed(() =>
					{
						_SlotsGameController.SlotsMachineController.SetBonusInfo(_spinsLeft, _multipliers);

						foreach (var item in _bonusGameItems)
						{
							item.SelectionCompleted();
						}
						MainApp.Instance.GameController.BonusWinPopup.Show(new int[] { _spinsLeft, _multipliers });

						var sequence = DOTween.Sequence();
						sequence.AppendInterval(3.5f);
						//sequence.Append(_itemsCanvasGroup.DOFade(0, 1.5f));
						sequence.AppendCallback(() =>
						{
							RunBonusGameAutoSpin();
						});
						sequence.Play();

					}, 5f);
				}
			}
		}

		private void UpdateBonusesText(int multipliers, int spins)
		{
			_multiplierText.text = multipliers.ToString();
			_spinsLeftText.text = spins.ToString();
		}

		private class BonusGameItem
		{
			private LoadObjectsController _loadObjectsController;
			public Action<_rewardData> OnItemClickEvent;

			private GameObject _selfObject;

			private RectTransform _selfRect;

			private Button _selfButton;

			private Image _freeSpinsImage,
										_multiplierImage,
										_enabledImage,
										_potImage,
										_disabledImage;


			private _rewardData _data;

			private AnimationSequence _animationSequence;

			private CanvasGroup _infoCanvasGroup;

			private bool _isActive;

			private Vector3 _startPosition;
			private SpriteAtlas _spriteAtlas;
			private int _activeImagesCount;

			public _rewardData Data { get => _data; }

			public BonusGameItem(GameObject self, int index)
			{
				_loadObjectsController = MainApp.Instance.LoadObjectsController;
				_spriteAtlas = _loadObjectsController.GetObjectByPath<SpriteAtlas>("images/bonusgamenumbers/numbers");
				_selfObject = self;
				_selfButton = _selfObject.GetComponent<Button>();
				_selfButton.onClick.AddListener(OnSelfButtonClickHandler);
				_selfRect = _selfObject.GetComponent<RectTransform>();
				_freeSpinsImage = _selfObject.transform.Find("Panel_Info/Image_FreeSpins").GetComponent<Image>();
				_multiplierImage = _selfObject.transform.Find("Panel_Info/Image_SuperRound").GetComponent<Image>();
				_enabledImage = _selfObject.transform.Find("Image_Coin_Enabled").GetComponent<Image>();
				_disabledImage = _selfObject.transform.Find("Image_Coin").GetComponent<Image>();
				_potImage = _selfObject.transform.Find("Image_pot").GetComponent<Image>();


				_animationSequence = _selfObject.transform.Find("Image_Coin").GetComponent<AnimationSequence>();
				_infoCanvasGroup = _selfObject.transform.Find("Panel_Info").GetComponent<CanvasGroup>();
				_startPosition = _selfRect.anchoredPosition;
			}
			public void ShowAnimate()
			{
				var rotateSequence = DOTween.Sequence();
				var scaleSequence = DOTween.Sequence();
				rotateSequence.Append(_selfObject.transform.DORotate(new Vector3(0, 0, 25f), 1f));
				rotateSequence.Append(_selfObject.transform.DORotate(new Vector3(0, 0, 0), 0.6f));
				scaleSequence.Append(_selfObject.transform.DOScale(1.2f, 1f));
				scaleSequence.Append(_selfObject.transform.DOScale(1f, 0.6f));
				scaleSequence.Play();
				rotateSequence.Play();
			}
			public void AnimateWin()
			{
				var sequence = DOTween.Sequence();
				//sequence.AppendInterval(0.01f);
				sequence.Append(_selfObject.transform.DOScale(0.8f, 0.5f));
				sequence.Append(_selfObject.transform.DOScale(1.2f, 0.5f));
				sequence.Append(_selfObject.transform.DOScale(0.8f, 0.5f));
				sequence.Append(_selfObject.transform.DOScale(1.2f,		0.5f));
				sequence.Append(_selfObject.transform.DOScale(1.0f, 0.5f));
				sequence.Play();
			}
			public void AnimateLose()
			{
				_selfObject.transform.DOScale(0, 1.5f);
			}

			public void UpdateRewardData(_rewardData data)
			{
				_selfRect.anchoredPosition = _startPosition;
				//_selfObject.transform.localScale = Vector3.one;

				_data = data;
				Debug.Log("Data Is Update");

				if (!_enabledImage.enabled)
				{
					_isActive = true;
					_potImage.sprite = _loadObjectsController.GetObjectByPath<Sprite>("images/items/" + data.count);
					_infoCanvasGroup.alpha = 0;
					Debug.Log("Iamgeoff");

					if (data.type == BonusGameRewardType.FREE_SPINS)
					{
						Debug.Log("isFreeSping");
						_freeSpinsImage.enabled = true;
						_multiplierImage.enabled = false;

						string[] values = new string[data.count.ToString().Length];
						_activeImagesCount = values.Length;
						
						for (int i = 0; i < values.Length; i++)
						{
							values[i] = data.count.ToString().ElementAt(i).ToString();
							Debug.Log("End " + values[i]);
							_selfObject.transform.Find("Panel_Info/Panel_TextValue/Image_" + i).GetComponent<Image>().sprite =
								_spriteAtlas.GetSprite("digital_" + values[i]);
						}
					}
					else if (data.type == BonusGameRewardType.MULTIPLIER)
					{
						_freeSpinsImage.enabled = false;
						_multiplierImage.enabled = true;

						string[] values = new string[data.count.ToString().Length];
						_activeImagesCount = values.Length + 1;
						for (int i = 0; i < values.Length; i++)
						{
							values[i] = data.count.ToString().ElementAt(i).ToString();
							_selfObject.transform.Find("Panel_Info/Panel_TextValue/Image_" + i).GetComponent<Image>().sprite =
								_spriteAtlas.GetSprite("digital_" + values[i]);
						}
						_selfObject.transform.Find("Panel_Info/Panel_TextValue/Image_" + values.Length).GetComponent<Image>().sprite =
								_spriteAtlas.GetSprite("digital_x");

					}
					//ChangeButtonState(_isActive);
				}
				Debug.Log("End");
			}

			public void UpdateButtonState()
			{
				ChangeButtonState(!_enabledImage.enabled);
			}

			private void TurnOffFace()
			{
				_enabledImage.enabled = true;
				_disabledImage.enabled = false;
				_potImage.enabled = true;
				for (int i = 0; i < _activeImagesCount; i++)
				{
					_selfObject.transform.Find("Panel_Info/Panel_TextValue/Image_" + i).gameObject.SetActive(true);
				}
				_infoCanvasGroup.gameObject.SetActive(true);
				_infoCanvasGroup.alpha = 1;
				_selfObject.transform.SetAsLastSibling();
				_isActive = false;
				ChangeButtonState(_isActive);
				_selfObject.transform.DOScale(2, 0.5f).OnComplete(() => _selfObject.transform.DOScale(1f, 0.5f));

			}

			private void OnSelfButtonClickHandler()
			{
				ChangeButtonState(false);
				MainApp.Instance.GameController.SoundController.PlaySound(Enumerators.SoundType.ChooseBonusItem);
				var sequence = DOTween.Sequence();
				sequence.Append(_selfObject.transform.DORotate(new Vector3(0, 0, 25f), 0.5f));
				sequence.Append(_selfObject.transform.DORotate(new Vector3(0, 0, -25f), 0.5f));
				sequence.Append(_selfObject.transform.DORotate(new Vector3(0, 0, 0f), 0.5f));
				sequence.Play().OnComplete(() => TurnOffFace());
				OnItemClickEvent?.Invoke(_data);
			}

			

			public void SelectionCompleted()
			{
				//_infoCanvasGroup.DOFade(1, 1f);
				ChangeButtonState(false);
				InternalTools.DoActionDelayed(() => Dispose(), 2f);
			}

			public void ChangeButtonState(bool state)
			{
				_selfButton.interactable = state;
			}
			public void Dispose()
			{
				_infoCanvasGroup.alpha = 0;
				for (int i = 0; i < 3; i++)
				{
					_selfObject.transform.Find("Panel_Info/Panel_TextValue/Image_" + i).gameObject.SetActive(false);
					_selfObject.transform.Find("Panel_Info/Panel_TextValue/Image_" + i).GetComponent<Image>().sprite = null;
				}
				_enabledImage.enabled = false;
				_disabledImage.enabled = true;
				_potImage.enabled = false;
				_selfButton.interactable = false;
				_data = null;
				_infoCanvasGroup.gameObject.SetActive(false);
				_selfObject.transform.localScale = Vector3.zero;
			}
		}

	}

	public enum BonusGameRewardType
	{
		FREE_SPINS,
		MULTIPLIER,
		SUPER_ROUND
	}

	public class _rewardData
	{
		public BonusGameRewardType type;
		public int count;

		[UnityEngine.Scripting.Preserve]
		public _rewardData()
		{
		}


		[Newtonsoft.Json.JsonConstructor]
		[UnityEngine.Scripting.Preserve]
		public _rewardData(
		BonusGameRewardType type,
		int count)
		{
			this.type = type;
			this.count = count;
		}
	}
}