using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
	public class BetPopup : IUIGamePopup
    {
        public bool ActiveInHierarchy => _selfPage != null ? _selfPage.activeInHierarchy : false;

        private GameObject _selfPage;
		private GameController _gameController;

        //--VARIABLES_START

		private Button _buttonBack, _betIncrease, _betDecrease;
        private TMPro.TextMeshProUGUI _betAmount, _betInCashAmount, _betInCoinsAmount;

        //--VARIABLES_END

		public void Init(Transform canvas)
		{
			_gameController = MainApp.Instance.GameController;
			_selfPage = canvas.Find("BetPopup").gameObject;

            MainApp.Instance.GameController.SlotsMachineController.OnBetChange += OnBetChangedHandler;

            //--VARIABLES_INIT_START

            _buttonBack = _selfPage.transform.Find("Button_Back").GetComponent<UnityEngine.UI.Button>();
            _buttonBack.onClick.AddListener(OnBackButtonClickHandler);

            _betIncrease = _selfPage.transform.Find("Rows/BetLevel/Button_Plus").GetComponent<UnityEngine.UI.Button>();
            _betIncrease.onClick.AddListener(MainApp.Instance.GameController.SlotsMachineController.OnAddBetButtonOnClickHandler);
            _betDecrease = _selfPage.transform.Find("Rows/BetLevel/Button_Minus").GetComponent<UnityEngine.UI.Button>();
            _betDecrease.onClick.AddListener(MainApp.Instance.GameController.SlotsMachineController.OnDecreaseBetButtonOnClickHandler);

            _betAmount = _selfPage.transform.Find("Rows/BetLevel/Amount/Text").GetComponent<TMPro.TextMeshProUGUI>();
            _betInCashAmount = _selfPage.transform.Find("BetInCash/Text_Amount").GetComponent<TMPro.TextMeshProUGUI>();
            _betInCoinsAmount = _selfPage.transform.Find("BetInCoins/Text_Amount").GetComponent<TMPro.TextMeshProUGUI>();

            //click sounds
            _buttonBack.onClick.AddListener(_gameController.SoundController.PlayClickSound);
            _betIncrease.onClick.AddListener(_gameController.SoundController.PlayClickSound);
            _betDecrease.onClick.AddListener(_gameController.SoundController.PlayClickSound);

            //--VARIABLES_INIT_END

            Hide();
		}

        private void OnBetChangedHandler(int bet)
        {
            _betAmount.text = (MainApp.Instance.GameController.SlotsMachineController.currentBetLevel) + "";
            _betInCoinsAmount.text = bet.ToString();
            _betInCashAmount.text = "" + (bet / 100f);
        }

        //--BUTTON_HANDLERS_START

		private void OnBackButtonClickHandler()
        {
            Hide();
        }
        

        //--BUTTON_HANDLERS_END

        public void Update()
		{

		}

		public void Show(object data)
		{
			Show();
		}

		public void Show()
		{
			Screen.fullScreen = true;
			_selfPage.SetActive(true);
			_selfPage.transform.SetAsLastSibling();
		}

		public void Hide()
		{
			_selfPage.SetActive(false);
		}

		public void Dispose()
		{

		}

		private void OnFullScreenModeClickHandler()//BaseEventData data
		{
			Hide();
			_gameController.UpdateFullScreenMode();
		}
	}
}
