using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
    public class WinnerPopup : IUIGamePopup
    {
        public bool ActiveInHierarchy => _selfPage != null ? _selfPage.activeInHierarchy : false;

        private GameObject _selfPage;
        private GameController _gameController;

        //--VARIABLES_START

        private TMPro.TextMeshProUGUI _amountText;
        private Transform _container;

        //--VARIABLES_END

        public void Init(Transform canvas)
        {
            _gameController = MainApp.Instance.GameController;
            _selfPage = canvas.Find("WinnerPopup").gameObject;

            //--VARIABLES_INIT_START
            _amountText = _selfPage.transform.Find("Container/Text_Amount").GetComponent<TMPro.TextMeshProUGUI>();
            _container = _selfPage.transform.Find("Container");

            //--VARIABLES_INIT_END

            Hide();
        }

        //--BUTTON_HANDLERS_START


        //--BUTTON_HANDLERS_END

        private void AnimateAndHide()
        {
            Debug.Log("FreeSpinsMoney ==== " + _gameController.SlotsMachineController.AccumulatedMoney);

            _gameController.GameIntefacePage.SetInteractability(false);
            _gameController.SoundController.PlaySound(Common.Enumerators.SoundType.WinPopup);

            InternalTools.DoActionDelayed(() =>
            {
                _gameController.GameIntefacePage.SetWinInfo(_gameController.SlotsMachineController.AccumulatedMoney);

            }, 1.5f);

            var wasAutoSpin = _gameController.SlotsMachineController.autoSpin;
            _amountText.text = ((int)_gameController.SlotsMachineController.AccumulatedMoney) + "";
            _container.DOKill();
            _container.localScale = Vector3.zero;
            _container.DOScale(1, 0.5f).OnComplete(() =>
            {
                _container.DOScale(0, 0.5f).SetDelay(3f).OnComplete(() =>
                {
                    _gameController.SlotsMachineController.FreeSpinsMoney = 0;
                    _gameController.SlotsMachineController.autoSpin = _gameController.SlotsMachineController.wasAutoSpinBeforeFreeSpins;
                    _gameController.GameIntefacePage.SetAllButtonsInteractablity(true);

                    if (!_gameController.SlotsMachineController.autoSpin)
                        _gameController.GameIntefacePage.StopAutospins();
                    else
                        _gameController.SlotsMachineController.Run();

                    Hide();
                });

            });
        }

        public void Update()
        {

        }

        public void Show(object data)
        {
            Show();
        }

        public void Show()
        {
            if (_selfPage.activeSelf)
                return;

            Screen.fullScreen = true;
            _selfPage.SetActive(true);
            _selfPage.transform.SetAsLastSibling();
            AnimateAndHide();
        }

        public void Hide()
        {
            _selfPage.SetActive(false);

            if (_gameController.GameIntefacePage != null)
                _gameController.GameIntefacePage.SetInteractability(true);
        }

        public void Dispose()
        {

        }

        private void OnFullScreenModeClickHandler()//BaseEventData data
        {
            Hide();
            _gameController.UpdateFullScreenMode();
        }
    }
}
