﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
	public class WinToBalancePurchaseItem
	{
		public event Action<WinToBalancePurchaseItem> OnPurchaseButtonClickedEvent;

		public double amount;
		public bool isAll;

		private Button _purchaseButton;

		private GameObject _activeObject;

		public WinToBalancePurchaseItem(Button button, double amount = 0, bool isAll = false)
		{
			this.amount = amount;
			this.isAll = isAll;
			_purchaseButton = button;
			_purchaseButton.onClick.AddListener(PurchaseButtonOnClickHandler);
		}

		private void PurchaseButtonOnClickHandler()
		{
			OnPurchaseButtonClickedEvent?.Invoke(this);
		}

		public void UpdateButtonState(bool state)
		{
			_purchaseButton.interactable = state;
		}
	}
}
