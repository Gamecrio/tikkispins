﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
    public class GameInfoPopup : IUIGamePopup
    {
        private GameObject _selfPage;
        private Button _backButton, _nextBtn, _prevBtn;

        private GameController _gameController;

        private int _currentIndex;
        private int _minValue = 0;
        private int _maxValue = 2;

        private float _endPosition;
        private float _deltaValue = 0.5f;

        private List<Toggle> _pageToggles;
        private List<RectTransform> _infoViews;

        private RectTransform _canvasRect;
        private Transform canvas;

        private GameInfoStats _gameInfoStats;

        public void Init(Transform canvas)
        {
            _gameController = MainApp.Instance.GameController;

            _selfPage = canvas.Find("GameInfoPopup").gameObject;
            _selfPage.transform.SetParent(canvas, false);

            _backButton = _selfPage.transform.Find("Button_Back").GetComponent<Button>();
            _nextBtn = _selfPage.transform.Find("Button_Next").GetComponent<Button>();
            _prevBtn = _selfPage.transform.Find("Button_Prev").GetComponent<Button>();

            _nextBtn.onClick.AddListener(OnNextClickHandler);
            _prevBtn.onClick.AddListener(OnPrevClickHandler);

            _pageToggles = new List<Toggle>();
            _infoViews = new List<RectTransform>();
            _canvasRect = canvas.GetComponent<RectTransform>();

            var panel = _selfPage.transform.Find("ScrollView_Info/Viewport/Content");

            for (int i = 0; i < panel.childCount; i++)
            {
                _infoViews.Add(panel.GetChild(i).GetComponent<RectTransform>());
            }

            var togglePanel = _selfPage.transform.Find("CurrentScreen_Panel");
            foreach (var item in togglePanel.GetComponentsInChildren<Toggle>())
            {
                _pageToggles.Add(item);
            }

            _gameController.OnFullScreenModeUpdatedEvent += UpdateSlotsSizeAfterChangeFullScreen;

            _backButton.onClick.AddListener(BackButtonOnClickHandler);

            MainApp.Instance.StartCoroutine(UpdateSizeOfView(3));

            //sounds
            _prevBtn.onClick.AddListener(_gameController.SoundController.PlayClickSound);
            _nextBtn.onClick.AddListener(_gameController.SoundController.PlayClickSound);
            _backButton.onClick.AddListener(() => { _gameController.SoundController.PlaySound(Common.Enumerators.SoundType.CloseInfoPopup); });

            InitGameInfoStats();

            Hide();
        }

        private void OnNextClickHandler()
        {
            SetInfo(_currentIndex + 1);
        }

        private void OnPrevClickHandler()
        {
            SetInfo(_currentIndex - 1);
        }

        private void SetInfo(int index)
        {
            if (index >= _infoViews.Count)
                _currentIndex = 0;
            else if (index < 0)
                _currentIndex = _infoViews.Count - 1;
            else
                _currentIndex = index;

            foreach (var item in _infoViews)
            {
                item.gameObject.SetActive(false);
            }

            _infoViews[_currentIndex].gameObject.SetActive(true);
        }

        private void UpdateSlotsSizeAfterChangeFullScreen()
        {
            MainApp.Instance.StartCoroutine(UpdateSizeOfView(3));
        }

        private IEnumerator UpdateSizeOfView(int frameCount)
        {
            while (frameCount > 0)
            {
                frameCount--;
                yield return null;
            }
        }

        public void Update()
        {

        }

        public void Show(object data)
        {
            Show();
        }

        public void Show()
        {
            _selfPage.SetActive(true);
            _endPosition = 0;

            _gameInfoStats.UpdateStats();

            MainApp.Instance.GameController.SlotsMachineController.OnGameInfoPopupShown?.Invoke(true);
        }

        public void Hide()
        {
            _selfPage.SetActive(false);

            MainApp.Instance.GameController.SlotsMachineController.OnGameInfoPopupShown?.Invoke(false);
        }

        private void InitGameInfoStats()
        {
            _gameInfoStats = new GameInfoStats();

            AddGameInfoStatsForContainer(_selfPage.transform.Find("ScrollView_Info/Viewport/Content/Image_Info1/InfoTexts"));
            AddGameInfoStatsForContainer(_selfPage.transform.Find("ScrollView_Info/Viewport/Content/Image_Info2/InfoTexts"));
            AddGameInfoStatsForContainer(_selfPage.transform.Find("ScrollView_Info/Viewport/Content/Image_Info3/InfoTexts"));
            AddGameInfoStatsForContainer(_selfPage.transform.Find("ScrollView_Info/Viewport/Content/Image_Info4/InfoTexts"));

            _gameInfoStats.GetDataFromServer();
        }

        private void AddGameInfoStatsForContainer(Transform container)
        {
            int id;
            string name;
            Transform child;
            for (int i = 0; i < container.childCount; i++)
            {
                child = container.GetChild(i);
                name = child.name.Replace("Layer", string.Empty);
                if (int.TryParse(name, out id))
                {
                    // id - 1, because we start from Layer1, Layer2..., Layer1 -> id = 0
                    _gameInfoStats.AddItem(id - 1, new UIGameInfoStatsItem(child));
                }
                else
                {
                    Debug.LogError("Failed to parse to int: " + name);
                }
            }
        }

        public void Dispose()
        {

        }

        private void BackButtonOnClickHandler()
        {
            Hide();
        }
    }
}
