using DG.Tweening;

using System;
using System.Collections;
using System.Collections.Generic;

using TMPro;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
	public class BonusWinPopup : IUIGamePopup
	{
		public bool ActiveInHierarchy => _selfPage != null ? _selfPage.activeInHierarchy : false;

		private GameObject _selfPage;
		private GameController _gameController;

		private TextMeshProUGUI _freespinsText, _multiplierText;
		private Transform _container;

		public void Init(Transform canvas)
		{
			_gameController = MainApp.Instance.GameController;
			_selfPage = canvas.Find("BonusWinPopup").gameObject;

			_freespinsText = _selfPage.transform.Find("Container/Text_Freespins").GetComponent<TextMeshProUGUI>();
			_multiplierText = _selfPage.transform.Find("Container/Text_Multiplier").GetComponent<TextMeshProUGUI>();
			_container = _selfPage.transform.Find("Container");

			Hide();
		}

		private void AnimateAndHide()
		{
			//Debug.Log("FreeSpinsMoney ==== " + _gameController.SlotsMachineController.AccumulatedMoney);

			//_gameController.GameIntefacePage.SetInteractability(false);
			_gameController.SoundController.PlaySound(Common.Enumerators.SoundType.WinPopup);

			
			//var wasAutoSpin = _gameController.SlotsMachineController.autoSpin;

			_container.DOKill();
			_container.localScale = Vector3.zero;
			_container.DOScale(1, 0.5f).OnComplete(() =>
			{
				_container.DOScale(0, 0.5f).SetDelay(4f).OnComplete(() =>
							{
								Hide();
							});

			});
		}

		public void Update()
		{

		}

		public void Show(object data)
		{
			if (data != null)
			{
				int[] array = data as int[];
				_freespinsText.text = array[0].ToString();
				_multiplierText.text = array[1].ToString()+"X";
			}
			Show();
		}

		public void Show()
		{
			if (_selfPage.activeSelf)
				return;

			//Screen.fullScreen = true;
			_selfPage.SetActive(true);
			_selfPage.transform.SetAsLastSibling();
			AnimateAndHide();
		}

		public void Hide()
		{
			_selfPage.SetActive(false);

			if (_gameController.GameIntefacePage != null)
				_gameController.GameIntefacePage.SetInteractability(true);
		}

		public void Dispose()
		{

		}

		private void OnFullScreenModeClickHandler()//BaseEventData data
		{
			Hide();
			_gameController.UpdateFullScreenMode();
		}
	}
}
