using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
	public class DisconnectionPopup : IPopup
    {
        public bool ActiveInHierarchy => _selfPage != null ? _selfPage.activeInHierarchy : false;

        private GameObject _selfPage;
		private GameController _gameController;

        //--VARIABLES_START

		private UnityEngine.UI.Button _button1;
		private TMPro.TextMeshProUGUI _text0;

        

        //--VARIABLES_END

		public void Init(Transform canvas)
		{
			_gameController = MainApp.Instance.GameController;
			_selfPage = canvas.Find("DisconnectionPopup").gameObject;

            //--VARIABLES_INIT_START

			_button1 = _selfPage.transform.Find("Button_Ok").GetComponent<UnityEngine.UI.Button>();
			_button1.onClick.AddListener(OnButton1ClickHandler);
			_text0 = _selfPage.transform.Find("Text_Title").GetComponent<TMPro.TextMeshProUGUI>();
            

            //--VARIABLES_INIT_END

            Hide();
		}

        //--BUTTON_HANDLERS_START

		private void OnButton1ClickHandler() { Debug.Log("Button1 click"); }
        

        //--BUTTON_HANDLERS_END

        public void Update()
		{

		}

		public void Show(object data)
		{
			Show();
		}

		public void Show()
		{
			Screen.fullScreen = true;
			_selfPage.SetActive(true);
			_selfPage.transform.SetAsLastSibling();
		}

		public void Hide()
		{
			_selfPage.SetActive(false);
		}

		public void Dispose()
		{

		}

		private void OnFullScreenModeClickHandler()//BaseEventData data
		{
			Hide();
			_gameController.UpdateFullScreenMode();
		}
	}
}
