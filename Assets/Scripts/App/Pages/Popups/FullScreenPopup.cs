﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
	public class FullScreenPopup : IUIGamePopup
	{
		private GameObject _selfPage;
		private TextMeshProUGUI _descriptionText;
		private Button _fullScreenModeTrigger;

		private GameController _gameController;

		public bool ActiveInHierarchy => _selfPage != null ? _selfPage.activeInHierarchy : false;

		public void Init(Transform canvas)
		{
			_gameController = MainApp.Instance.GameController;

			_selfPage = canvas.Find("FullScreenPopup").gameObject; 
			_selfPage.transform.SetParent(canvas, false);

			_descriptionText = _selfPage.transform.Find("Text_Info").GetComponent<TextMeshProUGUI>();
			_fullScreenModeTrigger = _selfPage.transform.Find("Trigger_FullScreenMode").GetComponent<Button>();
			_fullScreenModeTrigger.onClick.AddListener(OnFullScreenModeClickHandler);

			Hide();
		}

		public void Update()
		{

		}

		public void Show(object data)
		{
			Show();
		}

		public void Show()
		{
			Screen.fullScreen = true;
			_selfPage.SetActive(true);
			_selfPage.transform.SetAsLastSibling();
		}

		public void Hide()
		{
			_selfPage.SetActive(false);
		}

		public void Dispose()
		{

		}

		private void OnFullScreenModeClickHandler()//BaseEventData data
		{
			Hide();
			_gameController.UpdateFullScreenMode();
		}
	}
}
