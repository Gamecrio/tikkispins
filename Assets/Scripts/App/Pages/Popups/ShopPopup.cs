using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
	public class ShopPopup : IUIGamePopup
	{
		public bool ActiveInHierarchy => _selfPage != null ? _selfPage.activeInHierarchy : false;

		private GameObject _selfPage;
		private GameController _gameController;

		//--VARIABLES_START

		private UnityEngine.UI.Button _buttonClose, _buttonBuyAll;
		private List<WinToBalancePurchaseItem> _winToBalancePurchaseItems;


		//--VARIABLES_END

		public void Init(Transform canvas)
		{
			_gameController = MainApp.Instance.GameController;
			_selfPage = canvas.Find("ShopPopup").gameObject;

			//--VARIABLES_INIT_START

			_buttonClose = _selfPage.transform.Find("Container/Button_Close").GetComponent<UnityEngine.UI.Button>();
			_buttonClose.onClick.AddListener(OnCloseClickHandler);
			_buttonBuyAll = _selfPage.transform.Find("Container/Button_BuyAll").GetComponent<UnityEngine.UI.Button>();
			_buttonBuyAll.onClick.AddListener(OnBuyAllClickHandler);

			FillPurchaseItems();

			MainApp.Instance.NetworkController.OnPurchaseSuccessfulEvent += OnPurchaseSuccessfulEventHandler;
			MainApp.Instance.NetworkController.OnPurchaseFailedEvent += OnPurchaseFailedEventHandler;

			//--VARIABLES_INIT_END

			Hide();
		}

		//--BUTTON_HANDLERS_START

		private void OnBuyAllClickHandler()
		{
			Hide();
			MainApp.Instance.GameController.PurchaseTimePopup.Show();
		}

		private void OnCloseClickHandler()
		{
			Hide();
		}

		private void FillPurchaseItems()
		{
			_winToBalancePurchaseItems = new List<WinToBalancePurchaseItem>();
			_winToBalancePurchaseItems.Add(new WinToBalancePurchaseItem(_selfPage.transform.Find("Container/Grid/Button_1").GetComponent<Button>(), 1));
			_winToBalancePurchaseItems.Add(new WinToBalancePurchaseItem(_selfPage.transform.Find("Container/Grid/Button_5").GetComponent<Button>(), 5));
			_winToBalancePurchaseItems.Add(new WinToBalancePurchaseItem(_selfPage.transform.Find("Container/Grid/Button_10").GetComponent<Button>(), 10));
			_winToBalancePurchaseItems.Add(new WinToBalancePurchaseItem(_selfPage.transform.Find("Container/Grid/Button_20").GetComponent<Button>(), 20));
			_winToBalancePurchaseItems.Add(new WinToBalancePurchaseItem(_selfPage.transform.Find("Container/Grid/Button_50").GetComponent<Button>(), 50));
			_winToBalancePurchaseItems.Add(new WinToBalancePurchaseItem(_selfPage.transform.Find("Container/Grid/Button_100").GetComponent<Button>(), 100));
			foreach (var item in _winToBalancePurchaseItems)
			{
				item.OnPurchaseButtonClickedEvent += OnPurchaseButtonClickedEventHandler;
			}
		}

		private void OnPurchaseButtonClickedEventHandler(WinToBalancePurchaseItem purchas)
		{
			if (purchas.isAll)
			{
				MainApp.Instance.NetworkController.CurrentApiController.WinToBalance(_gameController.PlayerMoney.TotalWinMoney); // _playerManager.LocalPlayer.winBalance
			}
			else
			{
				MainApp.Instance.NetworkController.CurrentApiController.WinToBalance(purchas.amount);
			}
			foreach (var purchase in _winToBalancePurchaseItems)
			{
				purchase.UpdateButtonState(false);
			}

			_buttonBuyAll.interactable = false;
		}

		private void UpdateWinToBalancePurchasesState()
		{
			double winBalance = _gameController.PlayerMoney.TotalWinMoney; //.LocalPlayer.winBalance;
			foreach (var purchase in _winToBalancePurchaseItems)
			{
				if (purchase.isAll)
				{
					purchase.UpdateButtonState(winBalance > 0);
				}
				else
				{
					purchase.UpdateButtonState(winBalance >= purchase.amount);
				}
			}

			_buttonBuyAll.interactable = winBalance > 0;
		}

		private void OnPurchaseSuccessfulEventHandler()
		{
			Debug.Log("Purchased successfully!");
			Hide();
		}

		private void OnPurchaseFailedEventHandler()
		{
			Debug.LogError("Purchase failed!");
			Hide();
			//_uiManager.DrawPopup<InfoPopup>(new InformationPopupData("Transaction failed"));
		}

		private void ClosePopupOnClickHandler()
		{
			_gameController.SoundController.PlaySound(Common.Enumerators.SoundType.Click);
			Hide();
		}

		private void BuyAllMoneyOnClickHandler()
		{
			_gameController.SoundController.PlaySound(Common.Enumerators.SoundType.Click);

		}

		private void BuyMoneyOnClickHandler(double value)
		{
			_gameController.SoundController.PlaySound(Common.Enumerators.SoundType.Click);

		}

		//--BUTTON_HANDLERS_END

		public void Update()
		{

		}

		public void Show(object data)
		{
			Show();
		}

		public void Show()
		{
			Screen.fullScreen = true;
			_selfPage.SetActive(true);
			_selfPage.transform.SetAsLastSibling();

			UpdateWinToBalancePurchasesState();
		}

		public void Hide()
		{
			_selfPage.SetActive(false);
		}

		public void Dispose()
		{

		}

		private void OnFullScreenModeClickHandler()//BaseEventData data
		{
			Hide();
			_gameController.UpdateFullScreenMode();
		}
	}
}
