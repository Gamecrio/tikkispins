using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
	public class PurchaseTimePopup : IPopup
	{
		public bool ActiveInHierarchy => _selfPage != null ? _selfPage.activeInHierarchy : false;

		private GameObject _selfPage;
		private GameController _gameController;

		//--VARIABLES_START

		private UnityEngine.UI.Button _buttonNo;
		private UnityEngine.UI.Button _buttonYes;
		private TMPro.TextMeshProUGUI _text1;
		private TMPro.TextMeshProUGUI _text0;

		private WinToBalancePurchaseItem _buyAllItem;

		//--VARIABLES_END

		public void Init(Transform canvas)
		{
			_gameController = MainApp.Instance.GameController;
			_selfPage = canvas.Find("PurchaseTimePopup").gameObject;

			//--VARIABLES_INIT_START

			_buttonNo = _selfPage.transform.Find("Button_No").GetComponent<UnityEngine.UI.Button>();
			_buttonNo.onClick.AddListener(OnButtonNoClickHandler);
			_buttonYes = _selfPage.transform.Find("Button_Yes").GetComponent<UnityEngine.UI.Button>();
			_buttonYes.onClick.AddListener(OnButtonYesClickHandler);
			_text1 = _selfPage.transform.Find("Text_Subtitle").GetComponent<TMPro.TextMeshProUGUI>();
			_text0 = _selfPage.transform.Find("Text_Title").GetComponent<TMPro.TextMeshProUGUI>();

			_buyAllItem = new WinToBalancePurchaseItem(_buttonYes, isAll: true);

			MainApp.Instance.NetworkController.OnPurchaseSuccessfulEvent += OnPurchaseSuccessfulEventHandler;
			MainApp.Instance.NetworkController.OnPurchaseFailedEvent += OnPurchaseFailedEventHandler;

			//--VARIABLES_INIT_END

			Hide();
		}

		//--BUTTON_HANDLERS_START

		private void OnButtonNoClickHandler()
		{
			Hide();
			MainApp.Instance.GameController.ShopPopup.Show();
		}

		private void OnButtonYesClickHandler()
		{
			Hide();
			// add time
		}

		private void OnPurchaseButtonClickedEventHandler(WinToBalancePurchaseItem purchas)
		{
			if (purchas.isAll)
			{
				MainApp.Instance.NetworkController.CurrentApiController.WinToBalance(_gameController.PlayerMoney.TotalWinMoney); // _playerManager.LocalPlayer.winBalance
			}

			_buyAllItem.UpdateButtonState(false);
		}

		private void OnPurchaseSuccessfulEventHandler()
		{
			Hide();
		}

		private void OnPurchaseFailedEventHandler()
		{
			Hide();
			//_uiManager.DrawPopup<InfoPopup>(new InformationPopupData("Transaction failed"));
		}

		private void UpdateWinToBalancePurchasesState()
		{
			double winBalance = _gameController.PlayerMoney.TotalWinMoney; //.LocalPlayer.winBalance;
			_buyAllItem.UpdateButtonState(winBalance > 0);
		}

		//--BUTTON_HANDLERS_END

		public void Update()
		{

		}

		public void Show(object data)
		{
			Show();
		}

		public void Show()
		{
			Screen.fullScreen = true;
			_selfPage.SetActive(true);
			_selfPage.transform.SetAsLastSibling();

			UpdateWinToBalancePurchasesState();
		}

		public void Hide()
		{
			_selfPage.SetActive(false);
		}

		public void Dispose()
		{

		}

		private void OnFullScreenModeClickHandler()//BaseEventData data
		{
			Hide();
			_gameController.UpdateFullScreenMode();
		}
	}
}
