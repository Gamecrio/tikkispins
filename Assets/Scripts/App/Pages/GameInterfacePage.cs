﻿using System;
using ClassicGames.WildBilly.Common;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ClassicGames.WildBilly
{
    public class GameInterfacePage
    {
        /// <summary>
        /// Set to true when purchasing entries
        /// </summary>
        public bool NeedTotalWinUpdate { get; set; } = false;

        private const float TimeForWinCounter = 0.5f;
        private const float DelayBeforeClick = 0.8f;

        private GameObject _selfPage;

        private GameController _gameController;
        private SoundController _soundController;
        private LoadObjectsController _loadObjectsController;
        private PlayerMoney _playerMoney;

        private TextMeshProUGUI _textMoney;
        public TextMeshProUGUI _textBet;
        public TextMeshProUGUI _wonAmountText;
        public TextMeshProUGUI _textLinesCount;
        public TextMeshProUGUI _freeSpinsCount;
        public TextMeshProUGUI _totalWin;
        public TextMeshProUGUI _noBalanceText;

        public Button _increaseBetButton,
                                        _decreaseBetButton,
                                        buttonAddLine,
                                        buttonDecreaseLine,
                                        _autoSpinButton,
                                        _returnToLobbyButton,
                                        _startSpinButton;
        private Button _stopSpinButton,
                                        _rulesButton,
                                        _settingsButton,
                                        _closeSettingsButton,
                                        _shopButton,
                                        _soundButton,
                                        _soundButtonMuted,
                                        _infoButton,
                                        _buyButton;

        private GameObject _settingsMenu, _panelRight;

        private CounterAnimation _winCounter, _totalWinCounter;

        private float _prevWinValue;
        private float _prevTotalWin = 0;

        private double _currentDelayForClick;
        private bool _isClickDelay;

        private WonLineItem _wonLineItem;

        private DebugSplitInoPanel _debugSplitInoPanel;

        private CanvasGroup _buttonsGrops;
        private CanvasGroup _pageGroup;
        private Canvas _pageCanvas;

        private Sprite _initialAutospinSprite, _autospinInProgressSprite;

        public double RandomBonus { get; set; }

        private bool _isInited = false;
        private bool _wasBonusSpin = false;

        public GameInterfacePage()
        {
        }

        public void Init(GameController SlotsGameController, Transform canvas)
        {
            _gameController = SlotsGameController;
            _playerMoney = _gameController.PlayerMoney;
            _soundController = _gameController.SoundController;
            _loadObjectsController = MainApp.Instance.LoadObjectsController;

            _winCounter = new CounterAnimation();
            _totalWinCounter = new CounterAnimation();

            _selfPage = canvas.Find("GameInterfacePage").gameObject;
            _selfPage.transform.SetParent(canvas, false);

            _freeSpinsCount = _selfPage.transform.Find("Panel_Header/FreeSpins/Text_Count").GetComponent<TextMeshProUGUI>();
            SetFreeSpinsState(-1);

            _noBalanceText = _selfPage.transform.Find("Panel_BottomInfo/Text_NoBalance").GetComponent<TextMeshProUGUI>();
            _noBalanceText.gameObject.SetActive(false);

            _totalWin = _selfPage.transform.Find("Panel_Bottom/LinesBlock/Image_TotalWin/Text_TotalWinValue").GetComponent<TextMeshProUGUI>();
            _textMoney = _selfPage.transform.Find("Panel_Bottom/LinesBlock/Image_Entries/Text_EntriesValue").GetComponent<TextMeshProUGUI>();
            _textBet = _selfPage.transform.Find("Panel_Bottom/LinesBlock/Image_TotalPlayed/Text_TotalPlayedValue").GetComponent<TextMeshProUGUI>();
            _textLinesCount = _selfPage.transform.Find("Panel_Bottom/LinesBlock/Image_LinesCount/Text_LinesCountValue").GetComponent<TextMeshProUGUI>();
            _wonAmountText = _selfPage.transform.Find("Panel_Bottom/LinesBlock/Image_Win/Text_WinVaue").GetComponent<TextMeshProUGUI>();

            // fix this shit in prefab!
            _textMoney.enableAutoSizing = true;
            _textMoney.fontSizeMax = 44f;

            var rt = _textMoney.GetComponent<RectTransform>();
            rt.offsetMax = new Vector2(-20, 0);
            rt.offsetMin = new Vector2(20, 0);

            _pageCanvas = _selfPage.AddComponent<Canvas>();
            _pageCanvas.overrideSorting = false;
            _pageCanvas.sortingOrder = 50;
            _selfPage.AddComponent<GraphicRaycaster>();
            _pageGroup = _selfPage.AddComponent<CanvasGroup>();
            //----------------------------------

            _panelRight = _selfPage.transform.Find("Panel_Right").gameObject;
            _increaseBetButton = _selfPage.transform.Find("Panel_Bottom/LinesBlock/Image_TotalPlayed/Button_IncreaseTotalPlayed").GetComponent<Button>();
            _decreaseBetButton = _selfPage.transform.Find("Panel_Bottom/LinesBlock/Image_TotalPlayed/Button_DecreaseTotalPlayed").GetComponent<Button>();
            _autoSpinButton = _selfPage.transform.Find("Panel_Right/Button_AutoSpin").GetComponent<Button>();
            _buyButton = _selfPage.transform.Find("Panel_Right/Button_Buy").GetComponent<Button>();
            _startSpinButton = _selfPage.transform.Find("Panel_Right/Button_StartSpin").GetComponent<Button>();
            _stopSpinButton = _selfPage.transform.Find("Panel_Right/Button_StopSpin").GetComponent<Button>();
            _returnToLobbyButton = _selfPage.transform.Find("Button_BackToLobby").GetComponent<Button>();
            _returnToLobbyButton.onClick.AddListener(OnCloseGameHandler);

            _initialAutospinSprite = _autoSpinButton.GetComponent<Image>().sprite;
            _autospinInProgressSprite = _autoSpinButton.spriteState.highlightedSprite;

            if (_autospinInProgressSprite == null)
                _autospinInProgressSprite = _initialAutospinSprite;

            _wonLineItem = new WonLineItem(_selfPage.transform.Find("Panel_WinInfo").gameObject);
            _wonLineItem.ChangeState(false);

            _settingsMenu = _selfPage.transform.Find("SettingsMenu").gameObject;
            _settingsButton = _selfPage.transform.Find("Button_Settings").GetComponent<Button>();
            _closeSettingsButton = _selfPage.transform.Find("Button_CloseSettings").GetComponent<Button>();
            _soundButton = _settingsMenu.transform.Find("Button_Sound").GetComponent<Button>();
            _soundButtonMuted = _settingsMenu.transform.Find("Button_SoundMuted").GetComponent<Button>();
            _shopButton = _settingsMenu.transform.Find("Button_Shop").GetComponent<Button>();
            _infoButton = _settingsMenu.transform.Find("Button_Info").GetComponent<Button>();

            SetSettingsMenuState(false);

            _increaseBetButton.onClick.AddListener(MainApp.Instance.GameController.SlotsMachineController.OnAddBetButtonOnClickHandler);
            _decreaseBetButton.onClick.AddListener(MainApp.Instance.GameController.SlotsMachineController.OnDecreaseBetButtonOnClickHandler);

            RandomBonus = UnityEngine.Random.Range(100, 500);

            _gameController.SlotsMachineController.OnFreeSpinsUpdated += SetFreeSpinsState;
            _gameController.SlotsMachineController.OnBetChange += SlotsMachineControllerOnBetChange;
            _gameController.SlotsMachineController.OnLineChange += SlotsMachineControllerOnLineChange;
            _gameController.SlotsMachineController.OnSpinDone += SlotsMachineControllerOnSpinDone;
            _gameController.SlotsMachineController.OnSpinRunEvent += OnSpinRunEventHandler;
            _playerMoney.OnMoneyOwnChanged += LocalPlayerOnMoneyOwnChanged;
            _autoSpinButton.onClick.AddListener(OnAutoSpinClickHandler);
            _startSpinButton.onClick.AddListener(OnSpinClickHandler);
            _stopSpinButton.onClick.AddListener(OnStopSpinOnClickHandler);
            _settingsButton.onClick.AddListener(OnSettingsButtonOnClickHandler);
            _closeSettingsButton.onClick.AddListener(OnCloseSettingsButtonHandler);
            _shopButton.onClick.AddListener(OnShopButtonClickHandler);
            _soundButton.onClick.AddListener(OnSoundButtonClickHandler);
            _soundButtonMuted.onClick.AddListener(OnSoundButtonClickHandler);
            _infoButton.onClick.AddListener(OnInfoButtonClickHandler);
            _buyButton.onClick.AddListener(OnBuyButtonClickHandler);

            //click sounds
            _increaseBetButton.onClick.AddListener(_soundController.PlayClickSound);
            _decreaseBetButton.onClick.AddListener(_soundController.PlayClickSound);
            _settingsButton.onClick.AddListener(_soundController.PlayClickSound);
            _closeSettingsButton.onClick.AddListener(_soundController.PlayClickSound);
            _shopButton.onClick.AddListener(_soundController.PlayClickSound);
            _soundButton.onClick.AddListener(_soundController.PlayClickSound);
            _soundButtonMuted.onClick.AddListener(_soundController.PlayClickSound);
            _infoButton.onClick.AddListener(() => { _soundController.PlaySound(Enumerators.SoundType.ShowInfoPopup); });
            _buyButton.onClick.AddListener(_soundController.PlayClickSound);
            _startSpinButton.onClick.AddListener(_soundController.PlayClickSound);
            _stopSpinButton.onClick.AddListener(_soundController.PlayClickSound);
            _returnToLobbyButton.onClick.AddListener(_soundController.PlayClickSound);

            MainApp.Instance.NetworkController.OnPurchaseSuccessfulEvent += OnPurchaseSuccededHandler;
            _gameController.SlotsMachineController.OnGameInfoPopupShown += OnGameInfoPopupShownHandler;

            FixSoundButton();

            _isInited = true;
            Hide();
        }

        private void FixSoundButton()
        {
            _soundButtonMuted.transform.SetAsFirstSibling();
            var le = _soundButtonMuted.GetComponent<LayoutElement>();
            if (le != null)
                MonoBehaviour.Destroy(le);
        }

        public void SetAfterFreeSpinsState()
        {
            _startSpinButton.gameObject.SetActive(false);
            _stopSpinButton.gameObject.SetActive(true);
            SetAllButtonsInteractablity(false);
            _stopSpinButton.interactable = true;
        }

        public void SetInteractability(bool interactable)
        {
            if (!_isInited) return;

            _pageGroup.interactable = interactable;
            _pageCanvas.overrideSorting = !interactable;
            _pageCanvas.sortingOrder = 50;
        }

        public void SetGeneralButtonsInteractablity(bool interactable)
        {
            _buyButton.interactable = _returnToLobbyButton.interactable = _shopButton.interactable = _infoButton.interactable = _increaseBetButton.interactable = _decreaseBetButton.interactable = interactable;
            SetAutospinInteractability();
        }

        public void SetAllButtonsInteractablity(bool interactable)
        {
            SetGeneralButtonsInteractablity(interactable);
            _startSpinButton.interactable = _stopSpinButton.interactable = interactable;
            SetAutospinInteractability();
        }

        public void SetAutospinInteractability()
        {
            _autoSpinButton.interactable = _gameController.SlotsMachineController.FreeSpinsLeft <= 0; // autospin active only out of the free game
        }
        public void SetAutospinInteractability(bool state)
        {
            _autoSpinButton.interactable = state;
        }

        private void SetInteractabilityForChangingBet(bool interactable)
        {
            if (!_isInited) return;

            _increaseBetButton.interactable = interactable;
            _decreaseBetButton.interactable = interactable;
        }

        private void OnPurchaseSuccededHandler()
        {
            UpdateBalanceText(_gameController.PlayerMoney.Money);
            UpdateTotalWinText(_gameController.PlayerMoney.TotalWinMoney);
        }

        private void OnGameInfoPopupShownHandler(bool active)
        {
            _settingsButton.gameObject.SetActive(!active);
            _returnToLobbyButton.gameObject.SetActive(!active);
            _panelRight.SetActive(!active);

            SetInteractabilityForChangingBet(!active);
        }

        private void SetFreeSpinsState(int count)
        {
            _freeSpinsCount.text = count.ToString();
            _freeSpinsCount.transform.parent.gameObject.SetActive(count >= 0);
        }

        private void OnCloseGameHandler()
        {
#if !DEV_LOCAL
            // OPEN HUB LOBBY
            ClassicGames.Hub.Common.GameHolderTool.ThrowGameActionEvent(ClassicGames.Hub.Common.GameHolderTool.EventType.LoadLobby);
#endif
        }

        private void OnBuyButtonClickHandler()
        {
            Debug.Log("Buy button click");

            if (_gameController.SlotsServer.IsBonusGame)
                return;

            _gameController.SlotsMachineController.autoSpin = false;
            SetSettingsMenuState(false);
            //_gameController.ShopPopup.Show();
#if !DEV_LOCAL
            ClassicGames.Hub.Common.GameHolderTool.SetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.PurchasesPopup, null);
#endif
        }

        private void OnSoundButtonClickHandler()
        {
            // toggle sounds
            SoundRightMenuHandler(!_soundController.IsMute);
        }

        private void OnShopButtonClickHandler()
        {
            SetSettingsMenuState(false);
            _gameController.BetPopup.Show();
        }

        private void OnInfoButtonClickHandler()
        {
            SetSettingsMenuState(false);
            _gameController.GameInfoPopup.Show();
        }

        public void SetWinExtraBonusInfo(double value)
        {
            _wonLineItem.SetExtraBonusInfo(value, false);

            //SetWinInfo(value);
        }

        public void ChangeSpinButtonsState(bool isSpin)
        {
            _startSpinButton.gameObject.SetActive(isSpin);
            _stopSpinButton.gameObject.SetActive(!isSpin);
        }

        private void QuitRightMenuHandler()
        {
            Application.OpenURL("uniwebview://closeApplication");
        }

        private void SoundRightMenuHandler(bool state)
        {
            _soundController.IsMute = state;
#if !DEV_LOCAL
            // SET STATUS OF SOUND TO HUB
            ClassicGames.Hub.Common.GameHolderTool.SetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.Sound, new object[] { !state });
#endif

            _soundController.UpdateSoundStatus();
            UpdateSoundButtons();
        }

        private void UpdateSoundButtons()
        {
            _soundButton.gameObject.SetActive(!_soundController.IsMute);
            _soundButtonMuted.gameObject.SetActive(_soundController.IsMute);
        }

        private void LobbyRightMenuHandler()
        {
            OnCloseGameClickHandler();
        }

        private void TouchRightMenuHandler(bool state)
        {

        }

        private void InfoRightMenuHandler()
        {
            _gameController.GameInfoPopup.Show();
        }

        private void SlotsMachineControllerOnSpinDone(double earned)
        {
            //Debug.Log("SPIN DONE!!!!!! was bonus: " + _wasBonusSpin + ", free left: " + _gameController.SlotsMachineController.FreeSpinsLeft + ", earned = " + earned);

            if (!_gameController.SlotsMachineController.autoSpin)
            {
                UpdateChangeBetAndLinesButtonsState(!_gameController.SlotsMachineController.isWinnerPopupOpen);
            }

            if (earned != 0 && _gameController.SlotsMachineController.is5OfAKindOpen == false)
            {
                StartCountersAnimation((float)_prevWinValue, (float)_gameController.SlotsMachineController.AccumulatedMoney);
            }
            else if (!_gameController.SlotsMachineController.isWinnerPopupOpen)
            {
                ChangeSpinButtonsState(true);
            }

            if (_gameController.SlotsMachineController.is5OfAKindOpen == false)
            {
                _prevWinValue = (float)_gameController.SlotsMachineController.AccumulatedMoney;
                _prevTotalWin = (float)_playerMoney.TotalWinMoney;
            }

            _startSpinButton.interactable = _gameController.SlotsMachineController.StartingBonusGame == false;
        }

        public void StartCountersManually()
        {
            Debug.Log("_prevWinValue: " + _prevWinValue + ", AccumulatedMoney: " + _gameController.SlotsMachineController.AccumulatedMoney);
            Debug.Log("_prevTotalWin: " + _prevTotalWin + ", TotalWinMoney: " + _playerMoney.TotalWinMoney);

            StartCountersAnimation((float)_prevWinValue, (float)_gameController.SlotsMachineController.AccumulatedMoney);

            _prevWinValue = (float)_gameController.SlotsMachineController.AccumulatedMoney;
            _prevTotalWin = (float)_playerMoney.TotalWinMoney;
        }

        private void OnSpinRunEventHandler()
        {
            SetAutospinButton();

            //reset win only out of bonus game
            ResetWinAmount();

            _wasBonusSpin = _gameController.SlotsServer.IsBonusGame;

            UpdateChangeBetAndLinesButtonsState(false);
            _wonLineItem?.ChangeState(false);

            if (!_gameController.SlotsMachineController.SingleColumnSpin)
            {
                ChangeSpinButtonsState(false);
            }
        }

        private void UpdateChangeBetAndLinesButtonsState(bool state)
        {
            _increaseBetButton.interactable = state;
            _decreaseBetButton.interactable = state;
            //buttonAddLine.interactable = state;
            //buttonDecreaseLine.interactable = state;
        }

        private void SlotsMachineControllerOnLineChange(int obj)
        {
            _textLinesCount.text = obj.ToString();
            _gameController.SlotsMachineController.ShowLines(obj);
        }

        private void SlotsMachineControllerOnBetChange(int value)
        {
            _textBet.text = value.ToString();
        }

        private void LocalPlayerOnMoneyOwnChanged(double money, double totalWin)
        {
            UpdateBalanceText(money);

            if (NeedTotalWinUpdate)
            {
                StartCountersAnimation((float)_prevWinValue, (float)_gameController.SlotsMachineController.AccumulatedMoney);
                NeedTotalWinUpdate = false;
                //UpdateTotalWinText(totalWin);
            }
        }

        private void UpdateBalanceText(double value)
        {
            _textMoney.text = ((int)value).ToString();
            _noBalanceText.gameObject.SetActive(!_gameController.SlotsMachineController.HasPlayerEnoughMoneyToBet());

            if (_noBalanceText.gameObject.activeSelf)
            {
#if !DEV_LOCAL
				ClassicGames.Hub.Common.GameHolderTool.SetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.CloseGameInfoPopup, null);
#endif
            }
        }

        private void UpdateTotalWinText(double amount)
        {
            //if (_gameController.SlotsMachineController.IsSpinning || _gameController.SlotsServer.IsBonusGame || _gameController.SlotsMachineController.FreeSpinsLeft > 0)
            //    return;

            //Debug.Log("UpdateTotalWinText: " + amount);

            _totalWin.text = String.Format("{0:0.00}", amount);
        }

        public void ResetWinAmount()
        {
            // only if not bonus game
           /// if (!_wasBonusSpin && !_gameController.SlotsServer.IsBonusGame && !_gameController.SlotsMachineController.freeSpin)
                _wonAmountText.text = "0";
        }

        public void DataManagerOnDataLoadedUpdate()
        {
            UpdateBalanceText(_playerMoney.Money);

            var bet = _gameController.SlotsMachineController.TotalBet;
            SlotsMachineControllerOnBetChange(bet);

            _prevTotalWin = (float)_playerMoney.TotalWinMoney;
            UpdateTotalWinText(_prevTotalWin);
        }

        public void UpdateExclusiveBonus()
        {
        }

        public void SetGrandBonus()
        {
            //_minorBonus = UnityEngine.Random.Range(100, 1000);
            //_majorBonus = UnityEngine.Random.Range(500, 10000);

            //_minorBonusAmountText.text = _minorBonus.ToString();
            //_majorBonusAmountText.text = _majorBonus.ToString();
        }

        public void UpdateRandomBonus(double increase = 0)
        {
            RandomBonus += increase;
            RandomBonus = Math.Round(RandomBonus, 2);
            //_randomBonusAmountText.text = RandomBonus.ToString();
        }

        public void UpdateWonPanelInfo(double value, int number)
        {
            _wonLineItem?.UpdateCurrentLineInfo(value, number);
        }

        private void OnCloseGameClickHandler()
        {
            Application.OpenURL("uniwebview://closeGame");
        }

        private void OnSpinClickHandler()
        {
            if (!_gameController.SlotsMachineController.endedSpining && !_gameController.SlotsMachineController.SingleColumnSpin)
            {
                return;
            }

            _wasBonusSpin = false;
            SlotsMachineControllerOnSpinDone(0);
            //_soundController.StopSound(Enumerators.SoundType.WinFrame);
            bool success = _gameController.SlotsMachineController.Run();
            _noBalanceText.gameObject.SetActive(!success);
            AddDelayForClick();

            if (!success)
            {
                SetAllButtonsInteractablity(true);
            }
        }

        private void OnStopSpinOnClickHandler()
        {
            if (_winCounter != null && _winCounter.IsAnimating)
                return;

            if (!_gameController.SlotsMachineController.connectionStatus)
            {
                _gameController.GameIntefacePage.SetAllButtonsInteractablity(false);
            }

            _gameController.SlotsMachineController.StopSpin();
            ChangeSpinButtonsState(true);
            _startSpinButton.interactable = false;

            AddDelayForClick();
        }

        private void OnRulesButtonOnClickHandler()
        {
            _gameController.GameInfoPopup.Show();
        }

        private void SetSettingsMenuState(bool active)
        {
            _settingsMenu.SetActive(active);
            _settingsButton.gameObject.SetActive(!active);
            _closeSettingsButton.gameObject.SetActive(active);

            // set not interactable if spinning or it will spin automatically
            _shopButton.interactable = _infoButton.interactable = !_gameController.SlotsMachineController.IsSpinning && !_gameController.SlotsMachineController.autoSpin;
        }

        private void OnSettingsButtonOnClickHandler()
        {
            SetSettingsMenuState(true);
        }

        private void OnCloseSettingsButtonHandler()
        {
            SetSettingsMenuState(false);
        }

        /// <summary>
        /// Call this method when jackpot popup opens
        /// </summary>
        public void StopAutospins()
        {
            _gameController.SlotsMachineController.autoSpin = false;
            if (_gameController.SlotsMachineController.IsSpinning == false)
                SetGeneralButtonsInteractablity(true);

            SetAutospinButton();
        }

        private void SetAutospinButton()
        {
            _autoSpinButton.GetComponent<Image>().sprite = _gameController.SlotsMachineController.autoSpin ? _autospinInProgressSprite : _initialAutospinSprite;
        }

        private void OnAutoSpinClickHandler()
        {
            if (_gameController.SlotsServer.IsBonusGame) // this check must be removed in 2nd mechanics!
            {
                Debug.Log("It is bonus game, cannot interrupt!");
                return;
            }

            bool canChange = _gameController.SlotsMachineController.FreeSpinsLeft <= 0;
            if (canChange)
            {
                _gameController.SlotsMachineController.autoSpin = !_gameController.SlotsMachineController.autoSpin;
                _gameController.SlotsMachineController.wasAutoSpinBeforeFreeSpins = _gameController.SlotsMachineController.autoSpin;
            }
            else
            {
                _gameController.SlotsMachineController.wasAutoSpinBeforeFreeSpins = _gameController.SlotsMachineController.wasAutoSpinBeforeFreeSpins;
            }

            if (!_gameController.SlotsMachineController.HasPlayerEnoughMoneyToBet())
            {
                _gameController.SlotsMachineController.autoSpin = _gameController.SlotsMachineController.wasAutoSpinBeforeFreeSpins = false;
                _noBalanceText.gameObject.SetActive(true);
            }

            if (_gameController.SlotsMachineController.autoSpin)
            {
                _soundController.PlaySound(Enumerators.SoundType.StartAutoSpin);

                if (canChange && _gameController.SlotsMachineController.IsSpinning == false)
                {
                    if (_gameController.SlotsMachineController.needToShowWinnerPopup)
                    {
                        _gameController.SlotsMachineController.StopSpin();
                    }
                    else
                    {
                        bool success = _gameController.SlotsMachineController.Run();
                        _noBalanceText.gameObject.SetActive(!success);

                        if (!success)
                        {
                            SetAllButtonsInteractablity(true);
                        }
                    }
                }
            }
            else
            {
                _soundController.PlaySound(Enumerators.SoundType.StopAutoSpin);

                if (canChange && _gameController.SlotsMachineController.IsSpinning == false)
                    SetGeneralButtonsInteractablity(true);
            }

            SetAutospinButton();
        }

        public void SetWinInfo(double value)
        {
            StartCountersAnimation((float)_prevWinValue, (float)value);

            _prevWinValue = (float)value;
            _prevTotalWin = (float)_playerMoney.TotalWinMoney;
        }

        public void Hide()
        {
            _selfPage.SetActive(false);
        }

        public void Show()
        {
            if (MainApp.Instance.NetworkController.CurrentSourceType != Enumerators.SourceType.Hub)
            {
                _returnToLobbyButton.gameObject.SetActive(true);
            }
            _wonAmountText.text = string.Empty;

            _selfPage.SetActive(true);
            ChangeSpinButtonsState(true);

            UpdateSoundButtons();
        }

        private void SetWinText(double amount)
        {
            _wonAmountText.text = Mathf.FloorToInt(Mathf.Clamp((float)amount, 0, (float)amount)).ToString();
        }

        private void StartCountersAnimation(float from, float to)
        {
            bool addToTotalWin = NeedTotalWinUpdate || ((_gameController.SlotsMachineController.FreeSpinsLeft >= 0) == false) && !_gameController.SlotsMachineController.needToShowWinnerPopup;


            if (!_gameController.SlotsServer.IsBonusGame && _gameController.SlotsMachineController.FreeSpinsLeft <= 0 && !_wasBonusSpin)
            {
                from = 0;
            }

            float prevTotalWin = _prevTotalWin;

            if (!float.TryParse(_totalWin.text, out prevTotalWin))
                prevTotalWin = _prevTotalWin;

            float duration = TimeForWinCounter;
            ChangeSpinButtonsState(false);

            //Debug.Log($"Starting counter from: {from} to: {to} addToTotalWin = {addToTotalWin}  prevTotalWin = {prevTotalWin}  totalWin = {_playerMoney.TotalWinMoney}");

            _winCounter.Animate(from, to, duration, () =>
            {
                SetWinText(_winCounter.CurrentValue);
            }, () =>
            {
                float targetValue = _winCounter.TargetValue;
                SetWinText(targetValue);

                if (addToTotalWin)
                {
                    _winCounter.Animate(targetValue, 0, duration, () =>
                    {
                        SetWinText(_winCounter.CurrentValue);
                    }, () =>
                    {
                        SetWinText(targetValue);
                        ChangeSpinButtonsState(true);
                    });

                    _totalWinCounter.Animate(prevTotalWin, (float)_playerMoney.TotalWinMoney, duration, () =>
                    {
                        UpdateTotalWinText(_totalWinCounter.CurrentValue);
                    }, () =>
                    {
                        UpdateTotalWinText(_totalWinCounter.TargetValue);
                    });
                }
                else
                {
                    ChangeSpinButtonsState(!_gameController.SlotsMachineController.needToShowWinnerPopup);
                }
            });
        }

        public void Update()
        {
            _winCounter?.Update();
            _totalWinCounter?.Update();

            UpdateDelayForClick();
        }

        private void AddDelayForClick()
        {
            //_isClickDelay = true;
            //_currentDelayForClick = DelayBeforeClick;
            //ChangeButtonState(false);
        }

        private void UpdateDelayForClick()
        {
            if (_isClickDelay)
            {
                _currentDelayForClick -= Time.deltaTime;
                if (_currentDelayForClick <= 0)
                {
                    _isClickDelay = false;
                    ChangeButtonState(true);
                }
            }
        }

        private void ChangeButtonState(bool state)
        {
            _buttonsGrops.interactable = state;
        }

        public void Dispose()
        {

        }

        private class WonLineItem
        {
            private GameObject _selfObject;

            private TextMeshProUGUI _lineInfoText;
            private TextMeshProUGUI _winInfoText;

            public WonLineItem(GameObject self)
            {
                _selfObject = self;
                _lineInfoText = _selfObject.transform.Find("Text_WinLines").GetComponent<TextMeshProUGUI>();
                _winInfoText = _selfObject.transform.Find("Text_WinValue").GetComponent<TextMeshProUGUI>();
            }

            public void SetGeneralInfo(int lineCount, double value)
            {
                _lineInfoText.text = string.Format("You won on {0} lines", lineCount);
                _winInfoText.text = string.Format("Total you won {0}", value);
            }

            public void SetExtraBonusInfo(double value, bool isGeneral = true)
            {
                if (isGeneral)
                {
                    _lineInfoText.text = "You won on 0 line";
                }
                _winInfoText.text = $"You won {value} on extra";
            }

            public void SetDoubleGameExtraInfo(double value)
            {
                _winInfoText.text = $"You won {value} on extra";
            }

            public void UpdateCurrentLineInfo(double value, int number)
            {
                _winInfoText.text = string.Format("You won {0} on line {1}", value, number);
            }

            public void ChangeState(bool state)
            {
                _selfObject.SetActive(state);
            }
        }

        public class DebugSplitInoPanel
        {
            public bool Active => _selfObject != null ? _selfObject.activeInHierarchy : false;

            private GameObject _selfObject;

            private TextMeshProUGUI _betsText,
                                    _rewardsText,
                                    _rtpText,
                                    _spinsText;

            public DebugSplitInoPanel(GameObject self)
            {
                _selfObject = self;
                _betsText = _selfObject.transform.Find("Text_TotalBetValue").GetComponent<TextMeshProUGUI>();
                _rewardsText = _selfObject.transform.Find("Text_RewardValue").GetComponent<TextMeshProUGUI>();
                _rtpText = _selfObject.transform.Find("Text_RtpValue").GetComponent<TextMeshProUGUI>();
                _spinsText = _selfObject.transform.Find("Text_SpinsValue").GetComponent<TextMeshProUGUI>();
            }

            public void ChangeState(bool state)
            {
                _selfObject.SetActive(state);
                if (state)
                {
                    _betsText.text = string.Empty;
                    _rewardsText.text = string.Empty;
                    _rtpText.text = string.Empty;
                    _spinsText.text = string.Empty;
                }
            }

            public void SetData(int count, double bets, double rewards, double rtp)
            {
                _betsText.text = bets.ToString("F2");
                _rewardsText.text = rewards.ToString("F2");
                _rtpText.text = string.Format("{0}%", rtp.ToString("F2"));
                _spinsText.text = count.ToString();
            }
        }
    }
}
