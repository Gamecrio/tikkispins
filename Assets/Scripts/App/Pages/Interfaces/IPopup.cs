﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClassicGames.WildBilly
{
	public interface IPopup
	{
		void Init(Transform canvas);
		void Update();
		void Dispose();
		void Show();
		void Hide();
	}
}
