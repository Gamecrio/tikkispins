﻿using System;
using UnityEngine;
using ClassicGames.WildBilly.Common;

namespace ClassicGames.WildBilly
{
	public class NetworkController
	{
		public Action OnPurchaseSuccessfulEvent;
		public Action OnPurchaseFailedEvent;

		public BaseApiController CurrentApiController { get { return _currentApiController; } }

		public event Action OnGameDataLoadedEvent;
		public Enumerators.SourceType CurrentSourceType { get; private set; }
		public Enumerators.ApiType CurrentApiType { get; private set; }
		public bool IsInternetConnectionAvailable
		{
			get { return Application.internetReachability != NetworkReachability.NotReachable; }
		}

		private BaseApiController _currentApiController;

		public void Init()
		{
			CurrentApiType = Enumerators.ApiType.ClassicGames;
			CurrentSourceType = Enumerators.SourceType.Hub;

			switch (CurrentApiType)
			{
				case Enumerators.ApiType.ClassicGames:
					_currentApiController = new ClassicGamesApiController();
					break;
			}

			_currentApiController?.Init();
			_currentApiController.OnGameDataLoadedEvent += OnGameDataLoadedEventHandler;
		}

		public void Bet(int amount, Action<bool> callback)
		{
			_currentApiController?.Bet(amount, callback);
		}

		public void HandleDoubleGame(double bet, bool correct)
		{
			_currentApiController?.HandleDoubleGame(bet, correct);
		}

		public void Update()
		{
		}

		public void Dispose()
		{
			_currentApiController?.Dispose();
		}

		private void OnGameDataLoadedEventHandler()
		{
			OnGameDataLoadedEvent?.Invoke();
		}
	}
}
