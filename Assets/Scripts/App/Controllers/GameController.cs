﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ClassicGames.WildBilly.Common;
using TMPro;

namespace ClassicGames.WildBilly
{
	public class GameController
	{
		public bool IsDoubleGame { get; set; } = false;
		public SlotsMachineController SlotsMachineController { get; set; }
		public SlotsServer SlotsServer { get; set; }
		public GameInterfacePage GameIntefacePage { get; set; }
		//public InfoPopup InfoPopup { get; set; }
		public GameInfoPopup GameInfoPopup { get; set; }
		//public SettingsPopup SettingsPopup { get; set; }
		//public DoubleMoneyPage DoubleMoneyPage { get; set; }
		public PlayerMoney PlayerMoney { get; set; }

		public BetPopup BetPopup { get; set; }
		public DisconnectionPopup DisconnectionPopup { get; set; }
		public WinnerPopup WinnerPopup { get; set; }

		public BonusWinPopup BonusWinPopup { get; set; }
		//public JackpotPopup JackpotPopup { get; set; }
		public PurchaseTimePopup PurchaseTimePopup { get; set; }
		public ShopPopup ShopPopup { get; set; }
		//public UltraWinPopup UltraWinPopup { get; set; }

		private FullScreenPopup _fullScreenPopup;

		public BonusGame BonusGame { get; private set; }

		public SoundController SoundController { get; private set; }

		public Dictionary<Enumerators.MiniGameType, bool> miniGameStates { get; private set; }

		public event Action OnGameStartedEvent;
		public event Action OnFullScreenModeUpdatedEvent;

		private DataController _dataController;
		private PlayerController _playerController;
		private NetworkController _networkController;
		private LoadObjectsController _loadObjectsController;

		private GameObject _go;
		private Transform _backCanvas,
							_frontCanvas;

		private bool _isLoaded = false;
		private bool _isInited = false;
		private bool _isUserDataLoaded;
		private bool _isBundleLoaded;
		private DateTime _initGameStartTime;

		private List<IUIGamePopup> _popups = new List<IUIGamePopup>();

		public void Init()
		{
			_dataController = MainApp.Instance.DataController;
			_playerController = MainApp.Instance.PlayerController;
			_networkController = MainApp.Instance.NetworkController;
			_loadObjectsController = MainApp.Instance.LoadObjectsController;
			_networkController.OnGameDataLoadedEvent += OnGameDataLoadedEventHandler;
			_loadObjectsController.BundlesDataLoadedEvent += OnBundleLoadedEventHandler;
		}

		public void Dispose()
		{
			_isInited = false;
			MonoBehaviour.Destroy(_go);
		}

		public void InitGame()
		{
			//LoadMiniGameData();

			var loadAssetRequest = _loadObjectsController.GetObjectByPath<GameObject>("prefabs/gameplay/slots");
			_go = MonoBehaviour.Instantiate(loadAssetRequest);

			_backCanvas = _go.transform.Find("Canvas_Back");
			_backCanvas.GetComponent<CanvasScaler>().matchWidthOrHeight = 0;
			_frontCanvas = _go.transform.Find("Canvas_Front");
			_frontCanvas.GetComponent<CanvasScaler>().matchWidthOrHeight = 1;

			Initialize(_go.transform);

#if UNITY_EDITOR
			ReapplyShaders(_go);
#endif
		}

		private void ReapplyShaders(GameObject go)
		{
			foreach (TextMeshProUGUI rend in go.GetComponentsInChildren<TextMeshProUGUI>(true))
			{
				Material[] materials = null;

				try
				{
					materials = rend.fontMaterials;
				}
				catch (Exception ex)
				{
					continue;
				}

				for (int i = 0; i < materials.Length; i++)
				{
					if (materials[i] == null || materials[i].shader == null)
						continue;

					string shaderName = materials[i].shader.name;
					Shader shader = Shader.Find(shaderName);

					if (shader != null)
						materials[i].shader = shader;
					else
						Debug.LogWarning("Shader " + shaderName + " not found!");
				}

				rend.fontMaterials = materials;
			}
			foreach (Renderer rend in go.GetComponentsInChildren<Renderer>(true))
			{
				Material[] materials = rend.sharedMaterials;

				for (int i = 0; i < materials.Length; i++)
				{
					if (materials[i] == null || materials[i].shader == null)
						continue;

					string shaderName = materials[i].shader.name;
					Shader shader = Shader.Find(shaderName);

					if (shader != null)
						materials[i].shader = shader;
					else
						Debug.LogWarning("Shader " + shaderName + " not found!");
				}

				rend.sharedMaterials = materials;
			}
		}

		//private void LoadMiniGameData()
		//{
		//	miniGameStates = new Dictionary<Enumerators.MiniGameType, bool>();
		//	for (int i = 0; i < Enum.GetNames(typeof(Enumerators.MiniGameType)).Length; i++)
		//	{
		//		miniGameStates.Add((Enumerators.MiniGameType)i, false);
		//	}

		//          BonusGameRewardData = new List<BonusGameRewardData>();
		//          BonusGameRewardData.Add(new BonusGameRewardData() { type = BonusGameRewardType.SPINS_LEFT, count = 1 });
		//          BonusGameRewardData.Add(new BonusGameRewardData() { type = BonusGameRewardType.SPINS_LEFT, count = 3 });
		//          BonusGameRewardData.Add(new BonusGameRewardData() { type = BonusGameRewardType.SPINS_LEFT, count = 5 });
		//          BonusGameRewardData.Add(new BonusGameRewardData() { type = BonusGameRewardType.MULTIPLIER, count = 5 });
		//          BonusGameRewardData.Add(new BonusGameRewardData() { type = BonusGameRewardType.SPINS_LEFT, count = 8 });

		//          List<MiniGameDataModel> minigames = JsonConvert.DeserializeObject<List<MiniGameDataModel>>(_loadObjectsController.GetObjectByPath<TextAsset>("data/minigamesdata").text,
		//              new MiniGameDataConverter());

		//          foreach (var item in minigames)
		//          {
		//              miniGameStates[item.type] = item.active;
		//          }
		//}

		public bool MiniGameActive(Enumerators.MiniGameType type)
		{
			if (miniGameStates != null && miniGameStates.ContainsKey(type))
			{
				return miniGameStates[type];
			}
			return false;
		}

		private void OnBundleLoadedEventHandler()
		{
			_initGameStartTime = DateTime.Now;
			InitGame();
			_isBundleLoaded = true;
			if (_isUserDataLoaded)
			{
				StartGame();
			}
			else
			{
				_go.SetActive(false);
			}
		}

		private void OnGameDataLoadedEventHandler()
		{
			_isUserDataLoaded = true;
			if (_isBundleLoaded)
			{
				StartGame();
			}
		}
						
		public void StartGame()
		{
			_go.SetActive(true);
			GameIntefacePage.UpdateExclusiveBonus();
			GameIntefacePage.DataManagerOnDataLoadedUpdate();
			OnGameStartedEvent?.Invoke();
			//TimeDebugger.Instance.AddTimeInfo("GameStart", (DateTime.Now - _initGameStartTime).TotalSeconds.ToString());
		}

		public void Update()
		{
			if (!_isInited)
				return;

			GameIntefacePage.Update();
			SlotsMachineController.Update();
			SoundController.Update();

			for (int i = 0; i < _popups.Count; i++)
			{
				_popups[i].Update();
			}

			if (!Screen.fullScreen && !_fullScreenPopup.ActiveInHierarchy && _networkController.CurrentSourceType == Enumerators.SourceType.Browser)
			{
				Screen.fullScreen = true;
				_fullScreenPopup.Show();
				UpdateFullScreenMode();
			}
		}

		public void UpdateFullScreenMode()
		{
			OnFullScreenModeUpdatedEvent?.Invoke();
		}

		private void Initialize(Transform root)
		{
			SoundController = new SoundController(this);
			PlayerMoney = new PlayerMoney(this);
			SlotsServer = new SlotsServer(this);
			SlotsMachineController = new SlotsMachineController(this, GameObject.Find("SlotMachine_Screen_NEW"));
			SlotsServer.Init();
			GameIntefacePage = new GameInterfacePage();
			GameIntefacePage.Init(this, _frontCanvas);
			_fullScreenPopup = new FullScreenPopup();
			GameInfoPopup = new GameInfoPopup();
			BonusGame = new BonusGame(_frontCanvas, this);
			BetPopup = new BetPopup();
			DisconnectionPopup = new DisconnectionPopup();
			PurchaseTimePopup = new PurchaseTimePopup();
			ShopPopup = new ShopPopup();
			WinnerPopup = new WinnerPopup();
			BonusWinPopup = new BonusWinPopup();

			_popups.Add(BetPopup);
			_popups.Add(_fullScreenPopup);
			_popups.Add(GameInfoPopup);
			_popups.Add(ShopPopup);
			_popups.Add(WinnerPopup);
			_popups.Add(BonusWinPopup);

			foreach (var item in _popups)
			{
				item.Init(_frontCanvas);
			}

			SlotsMachineController.AddListenerToGameInterfacePageButtons();

			GameIntefacePage.Show();

			_isInited = true;
		}
	}
}
