﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ClassicGames.WildBilly.Common;
using DG.Tweening;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;
using UnityEngine.U2D;
using static ClassicGames.WildBilly.LayerDataModel;
using static ClassicGames.WildBilly.SoundController;

namespace ClassicGames.WildBilly
{
    public class SlotsMachineController
    {
        public double AccumulatedMoney { get; private set; } = 0;

        private const int ColumnsCount = 5;
        public int Multiplier { get; private set; } = 1;
        public int FreeSpinsLeft { get; private set; } = -1;
        public bool IsSpinning = false;
        public double FreeSpinsMoney { get; set; } = 0;

        // columns will be spinning for at least this amount of time, if we got response from backend too quick (too avoid bugs)
        private const float MIN_SPINS_DURATION = 1.5f;
        private const float MIN_SPINS_DURATION_WHEN_STOPPED = 0.75f; // if we click stop, this duration for spin is a must
        private float _timeElapsedFromLastSpin;
        private bool _finishedSpins = false;
        private int _bonusSoundCounter = 0;
        private int _currentMaxFreeSpins;
        private bool _jackpotStatus;
        private double _globalFee;

        private const int BonusGameTileIndex = 12;
        private const int WildFirstTileIndex = 11;
        private const int WildSecondTileIndex = 10;

        private const float LINE_BLINK_INTERVAL = 1f;
        private const float LINE_SWITCH_INTERVAL = 2f;

        public event Action<int> OnFreeSpinsUpdated;
        public event Action OnEndFreeSpins;
        public event Action OnSpinRunEvent;
        public event Action OnDebugSpinEndedEvent;
        public event Action<int> OnBetChange;
        public event Action<int> OnLineChange;
        public event Action<double> OnSpinDone;
        public event Action GotMatrixEvent;
        public Action<bool> OnGameInfoPopupShown;

        private PlayerMoney _playerMoney;
        private SlotsServer SlotsServer;
        private GameController _gameController;
        private PlayerController _playerController;
        private DataController _dataController;
        private SoundController _soundController;
        private LoadObjectsController _loadObjectsController;

        private Sequence _linesSequence;
        private Sequence _certainLinesSequence;
        private Sequence _autoSpinsSequence;
        private Sequence _textAnimationSequence;
        private int countSpin;
        private int currentCol;
        private int randPlate;
        public int freeSpinWinValue;
        public int winLinesCount;

        private bool _gotEndMatrix;
        private List<Action> _actionsWhenSpinEnds;
        private int _totalBet;

        private readonly int lineStep = 1;
        private readonly int maxLine = 20;

        private bool _autoSpin;
        private bool _singleColumnSpin;
        private bool bonusGame = false;
        private bool _isBonusGameActive;
        private bool _finishingFreeSpins = true;

        public bool StartingBonusGame { get; private set; } = false;

        private Transform _container;
        private List<KeyValuePair<int, Sprite>> sprites;
        private readonly Sprite _shellSprite;

        private List<List<SlotsItem>> _columns = new List<List<SlotsItem>>();
        private List<List<BlockOfSlots>> _blockOfSlots = new List<List<BlockOfSlots>>();
        private List<ColumnItem> _columnItems;
        private int[] _centralItems;

        private List<ShellBonusGameItem> _shellBonusGameItems;

        private GameObject _slotMachine;

        private GameObject _freeSpinsFrames;
        private GameObject _freeSpinsColumn;
       // private GameObject _freeSpinsBackground;
        private GameObject _freeSpinsLineMask;
        private GameObject _lineMask;

        private List<Sequence> _currentSequences;
        private List<WinPanelItem> _winItems;

        private LayersDataInfo _layersData;

        private Transform _parentOfWinnerItems;

        private SpinResultData _lastSpinResult = null;

        private List<int> _shellsInColumns;

        private int _currentIndexFrame = 0;

        private List<Sequence> _stopColumnsSequences = new List<Sequence>();
        private Sequence _currentMovementSequence,
                         _startMovementSequence,
                         _movementTimeoutSequence,
                         _delayedRunSequence;

        private List<List<int>> _slotMachineMatrix;

        private readonly string _animationPath = "images/animations/gameplay/winnerframe/layer";

        private ColumnItem _firstMoveColumn;

        public bool startSpining = false,
                                         continueSpining = false,
                                         fullSpin = false,
                                         endedSpining = true;

        public bool freeSpin;
        public bool connectionStatus;
        public bool needToShowWinnerPopup = false;
        public bool isWinnerPopupOpen, is5OfAKindOpen;

        public int currentLines = 20;

        public const int AmountOfTiles = 13;
        public const float autospinDelay = 1.1f;

        public int currentBetIndex = 0;
        public int currentBetLevel = 0;

        public int[] bets;
        public int[] betLevels = new int[] { 1, 2, 3, 4, 5, 10, 15 };

        public LineRenderer[] line;

        public DotItem[] dots;

        public Dictionary<int, Sprite[]> AnimationFrames { get; private set; }

        public bool wasAutoSpinBeforeFreeSpins = false;

        public bool autoSpin
        {
            get
            {
                return _autoSpin;
            }
            set
            {
                _autoSpin = value;
            }
        }

        public float CurrentLineCoef
        {
            get { return currentLines; }
        }

        public int TotalBet
        {
            get { return _totalBet; }
        }

        public bool SingleColumnSpin
        {
            get { return _singleColumnSpin; }
        }

        public bool JackpotStatus
        {
            get { return _jackpotStatus; }
        }

        public List<List<SlotsItem>> Columns => _columns;
        public List<List<BlockOfSlots>> BlockOfSlots => _blockOfSlots;

        public object ResultLine { get; internal set; }

#if !DEV_LOCAL
        private Dictionary<ClassicGames.Hub.Common.GameHolderTool.WinPopupType, int> _winMultipliers;
        private Dictionary<ClassicGames.Hub.Common.GameHolderTool.WinPopupType, int> _winPopupsInfo;
#endif

        public void SetLines()
        {
            line = new LineRenderer[maxLine];
            dots = new DotItem[maxLine];

            var dotPanel = _slotMachine.transform.Find("Lines/Dots");
            float z = 0;
            for (int i = 0; i < dotPanel.childCount; i++)
            {
                dotPanel.GetChild(i).transform.position += Vector3.forward * z;
                z -= 0.001f;
            }


            for (int l = 0; l < line.Length; l++)
            {
                line[l] = _slotMachine.transform.Find("Lines/Lines_" + l).GetComponent<LineRenderer>();
                dots[l] = new DotItem(_slotMachine.transform.Find("Lines/Dots/dot_" + (l + 1)).gameObject, (l + 1).ToString());
                Vector3[] points = new Vector3[7];

                line[l].sortingOrder = 15;

            }
        }

        public SlotsMachineController(GameController SlotsGameController, GameObject slotMachine)
        {
            _dataController = MainApp.Instance.DataController;
            _playerController = MainApp.Instance.PlayerController;
            _loadObjectsController = MainApp.Instance.LoadObjectsController;
            _gameController = SlotsGameController;
            _soundController = _gameController.SoundController;
            _gameController.OnGameStartedEvent += OnGameStartedEventHandler;
            _gameController.OnFullScreenModeUpdatedEvent += UpdateSlotsSizeAfterChangeFullScreen;

            _currentSequences = new List<Sequence>();
            _columnItems = new List<ColumnItem>();

            _slotMachine = slotMachine;
            SlotsServer = SlotsGameController.SlotsServer;

            _playerMoney = SlotsGameController.PlayerMoney;
            SetSizeOfSlotMachine();

            SetImages();
            Init();
        }

        public void DoActionWhenSpinEnd(Action action)
        {
            _actionsWhenSpinEnds.Add(action);
        }

        public void SetBets(int[] data)
        {
            bets = data;
            OnUpdateBetHandler();
        }


        public void UpdateTotalBet()
        {
            if (_jackpotStatus)
            {
                _totalBet = (int)(bets[currentBetIndex] + (_globalFee * 100));
            }
            else
            {
                _totalBet = bets[currentBetIndex];
            }
            currentBetLevel = betLevels[currentBetIndex];
            OnBetChange?.Invoke(_totalBet);
        }

        public void Init()
        {
            _parentOfWinnerItems = new GameObject("[WinnerFrames]").transform;
            _parentOfWinnerItems.SetParent(_slotMachine.transform);
            _parentOfWinnerItems.localPosition = Vector3.zero;
            _parentOfWinnerItems.localScale = Vector3.one;

            _actionsWhenSpinEnds = new List<Action>();
            _shellBonusGameItems = new List<ShellBonusGameItem>();
            _centralItems = new int[ColumnsCount];
            _winItems = new List<WinPanelItem>();

            _freeSpinsFrames = _slotMachine.transform.Find("Frames_FreeSpin").gameObject;
            _freeSpinsColumn = _slotMachine.transform.Find("ColumnsFreespin").gameObject;
           // _freeSpinsBackground = _slotMachine.transform.Find("Image_FreespinBackground").gameObject;
            _freeSpinsLineMask = _slotMachine.transform.Find("LinesMaskFreeGame").gameObject;
            _lineMask = _slotMachine.transform.Find("LinesMask").gameObject;
            _freeSpinsFrames.SetActive(true);
            _freeSpinsColumn.SetActive(false);
          // _freeSpinsBackground.SetActive(false);
            _freeSpinsLineMask.SetActive(false);
            _lineMask.SetActive(true);

            var data = _loadObjectsController.GetObjectByPath<TextAsset>("data/layersdata");
            _layersData = JsonConvert.DeserializeObject<LayersDataInfo>(data.text);

            LoadAnimationFrames();
            FillWinMultipliers();
        }

        public void SetSlotMachineMatrix(List<List<int>> matrix)
        {
            //Debug.Log("SetSlotMachineMatrix");
            _slotMachineMatrix = matrix;
            var itemSlotPrefab = _loadObjectsController.GetObjectByPath<GameObject>("prefabs/gameplay/item_slot");
            ColumnItem columnItem = null;
            SlotsItem slotsItem = null;
            BlockOfSlots blockItem = null;
            Transform parentOfSlotItems;
            for (int col = 0; col < matrix.Count; col++)
            {
                _container = _slotMachine.transform.Find("SlotsColumn_" + col);
                parentOfSlotItems = _container.Find("Slots");

                columnItem = new ColumnItem(_container.gameObject, col, _gameController, this);
                columnItem.OnColumnClickEvent += OnColumnClickEventHandler;
                _columnItems.Add(columnItem);

                var blocks = new List<BlockOfSlots>();
                var slots = new List<SlotsItem>();

                for (int j = 0; j < Constants.BlockCount; j++)
                {
                    blockItem = new BlockOfSlots(parentOfSlotItems, j, matrix[col].Count, col);
                    blocks.Add(blockItem);
                    blockItem.OnMoveEndedEvent += OnBlockItemMoveEndedEventHandler;

                    for (int i = 0; i < matrix[col].Count; i++)
                    {
                       // Debug.Log(matrix[col][i]);

                        slotsItem = new SlotsItem(
                                                itemSlotPrefab,
                                                blockItem.SelfTransform,
                                                sprites[matrix[col][i]].Value,
                                                sprites[matrix[col][i]].Key,
                                                i);

                        slots.Add(slotsItem);
                    }
                }

                _blockOfSlots.Add(blocks);
                _columns.Add(slots);
            }

            SetRandomFirstPosition();
            SetLines();
            InternalTools.DoActionDelayed(DisableAllLines, 4.5f);

#if !DEV_LOCAL
			// SAY TO HUB THAT GAME LOADED
			ClassicGames.Hub.Common.GameHolderTool.ThrowGameActionEvent(ClassicGames.Hub.Common.GameHolderTool.EventType.GameLoaded);
#endif
        }

        private void SetRandomFirstPosition()
        {
            int value;
            for (int col = 0; col < _slotMachineMatrix.Count; col++)
            {
                value = UnityEngine.Random.Range(1, _slotMachineMatrix[col].Count);
                for (int i = 0; i < _blockOfSlots[col].Count; i++)
                {
                    _blockOfSlots[col][i].SetPosition(value);
                }
            }
        }

        private void LoadAnimationFrames()
        {
            AnimationFrames = new Dictionary<int, Sprite[]>();

            for (int i = 1; i <= AmountOfTiles; i++)
            {
                int index = i;

                Action<Sprite[]> callback = (receivedFrames) =>
                {
                    if (receivedFrames != null)
                    {
                        receivedFrames = receivedFrames.OrderBy(go => go.name.Length).ThenBy(x => x.name).ToArray();
                        AnimationFrames.Add(index, receivedFrames);
                    }
                };

                _loadObjectsController.GetObjectsByPathAsync<Sprite>($"{_animationPath + index}/", callback);
            }
        }

        private void OnColumnClickEventHandler(ColumnItem column)
        {
            if (autoSpin || freeSpin)
            {
                return;
            }

            if (HasPlayerEnoughMoneyToBet())
            {
                column.currentState = ColumnState.Active;

                if (endedSpining)
                {
                    if (_firstMoveColumn == null)
                    {
                        _firstMoveColumn = column;
                    }

                    _singleColumnSpin = true;
                    Run(false);
                }
                else
                {
                    StartOneColumnSpinImmediately();
                }

                if (_columnItems.FindAll(col => col.currentState != ColumnState.None).Count == _columnItems.Count)
                {
                    MainApp.Instance.GameController.GameIntefacePage.ChangeSpinButtonsState(false);
                }
            }
            else
            {
                NotEnoughMoneyToBet();
            }
        }

        private void OnAllColumnMoveEndedHandler()
        {
            _singleColumnSpin = false;
            foreach (var column in _columnItems)
            {
                column.OnAllColumnSpinDoneEventHandler();
            }
        }

        private void OnBlockItemMoveEndedEventHandler(int index)
        {
            _columnItems[index].UpdateCountOfBlocks();
        }

        private void ResetStopSounds()
        {
            //Debug.Log("Reset stop spin sounds");

            foreach (var item in _columnItems)
            {
                item.playedStopSound = false;
            }
        }

        public void PlayStopSpinSoundByColumnId(int columndId)
        {
            if (columndId >= 0 && columndId < _columnItems.Count)
            {
                PlayStopSpinSound(columndId);
            }
        }

        private void SetAllColumntActiveState()
        {
            foreach (var column in _columnItems)
            {
                column.currentState = ColumnState.Active;
            }
        }

        private void OnGameStartedEventHandler()
        {
        }

        private void UpdateSlotsSizeAfterChangeFullScreen()
        {
            MainApp.Instance.StartCoroutine(UpdateSizeOfSlotMachineAfterDelay(5));
        }

        private IEnumerator UpdateSizeOfSlotMachineAfterDelay(int frameCount)
        {
            while (frameCount > 0)
            {
                frameCount--;
                yield return null;
            }
            SetSizeOfSlotMachine();
        }

        public void SetSizeOfSlotMachine()
        {
            Vector3 scale = _slotMachine.transform.localScale;

            float ratio = Screen.width / (float)Screen.height;

            Dictionary<float, float> tests = new Dictionary<float, float>()
             {
                {1f, 0.64f },
                {1.3f, 0.845f },
                {1.33f, 0.86f },
                {1.5f, 0.97f },
                {1.62f, 1.03f },
                {1.77f, 1.148f },
                {2.16f, 1.395f },
                {2.43f, 1.29f }
            };
            float actualX = 0;

            foreach (var item in tests)
            {
                if (Mathf.Abs(ratio - item.Key) <= 0.01f)
                {
                    actualX = item.Value;
                    break;
                }
                else if (ratio > item.Key)
                {
                    float downValue = item.Key;
                    var list = tests.Keys.ToList();
                    int indexNext = list.IndexOf(item.Key) + 1;
                    float upValue;
                    if (indexNext < list.Count)
                    {
                        upValue = list[indexNext];
                    }
                    else
                    {
                        upValue = downValue;
                    }

                    float downChance = tests[downValue];
                    float upChance = tests[upValue];
                    float delta = (upChance - downChance) / (upValue - downValue);

                    actualX = downChance + (ratio - downValue) * delta;
                }
            }

            scale.x = Mathf.Clamp(actualX, 0.5f, 4f);
        }

        public LayerDataInfo GetLayerDataById(int id)
        {
            return _layersData.data.Find(info => info.id == id);
        }

        public void Dispose()
        {
            StopAnimation();
            ResetLineSequence();

            foreach (var column in _columns)
            {
                foreach (var slotItem in column)
                {
                    slotItem.Dispose();
                }
            }
        }

        public void Update()
        {
            if (!endedSpining)
            {
                MoveItems();
            }

            if (!endedSpining)
            {
                int count = _columnItems.FindAll(column => column.currentState == ColumnState.Stopped).Count;

                if (count == _columnItems.Count)
                {
                    CheckResult();
                }
            }

            if (IsSpinning)
            {
                _timeElapsedFromLastSpin += Time.deltaTime;

                if (!_finishedSpins && _timeElapsedFromLastSpin >= MIN_SPINS_DURATION && _gotEndMatrix)
                {
                    Debug.Log("Finishing spins!");
                    _finishedSpins = true;
                    EndSpin(_needStopSpin);

                    //_needStopSpin = false; // must be reset after EndSpin
                    //GotMatrixEvent?.Invoke();
                }
            }

#if UNITY_EDITOR
            //test
            if (Input.GetKeyDown(KeyCode.Keypad3))
            {
                Debug.Log("Test show winner popup");
                _gameController.WinnerPopup.Show();
            }
#endif
        }

        private void MoveItems()
        {
            for (int i = 0; i < _columnItems.Count; i++)
            {
                for (int j = 0; j < _blockOfSlots[i].Count; j++)
                {
                    if (_columnItems[i].currentState == ColumnState.EndMove)
                    {
                        _blockOfSlots[i][j].Move();
                    }
                }
            }
        }

        public void HandleBetResponse(SpinResultData data)
        {
            _lastSpinResult = data;
            winLinesCount = data.winLines.Length;
            if (data.resultMatrix == null || data.resultMatrix.Length <= 0)
            {
                Debug.LogError("HandleBetResponce: data.resultMatrix is null or its Length = 0");
                return;
            }
            Debug.Log(data.ToString());
            _movementTimeoutSequence?.Kill();
            _movementTimeoutSequence = null;

            _centralItems = data.resultMatrix;
            _shellsInColumns = data.shellInfo.Select(item => item.col).ToList();

            if (data.isBonusGame == 1)
            {
               // CheckFreeSpinsCount();
                _isBonusGameActive = true;
                SlotsServer.IsBonusGame = true;
                autoSpin = false;
                bonusGame = false;

             //   _currentMaxFreeSpins = FreeSpinsLeft;
            }

            //EndSpin();

            _gotEndMatrix = true;
            GotMatrixEvent?.Invoke();
        }

        public void SignalColumnEnded(ColumnItem columnItem)
        {
            if (_shellsInColumns == null)
            {
                return;
            }

            var columnsMoveEnded = _columnItems.FindAll(column => column.currentState == ColumnState.Stopped);

            int shellsCount = 0;

            foreach (var item in columnsMoveEnded)
            {
                if (_shellsInColumns.Contains(item.Index))
                {
                    shellsCount++;
                }
            }

            var columnsMove = _columnItems.FindAll(column => column.currentState != ColumnState.Stopped);
            if (columnsMove.Count > 0)
            {
                float delay = UnityEngine.Random.Range(0.1f, 0.15f);

                if (shellsCount >= 2)
                {
                    foreach (var item in columnsMove)
                    {
                        //item.UpdateFireBorderState(true);
                    }
                    delay = UnityEngine.Random.Range(1f, 1.2f);
                }

                _startMovementSequence?.Kill();
                _startMovementSequence = InternalTools.DoActionDelayed(() =>
                {
                    EndColumnsSpin(columnsMove[0]);
                }, delay);
            }
        }

        private void ClearStopColumnSequences()
        {
            foreach (var item in _stopColumnsSequences)
            {
                InternalTools.KillSequence(item);
            }
        }

        /// <summary>
        /// If there is a bonus element in this column, play its sound or default stop spin sound
        /// </summary>
        private void PlayStopSpinSound(int columnIndex)
        {
            if (_columnItems[columnIndex].playedStopSound)
                return;

            //Debug.Log("PlayStopSpinSound: " + columnIndex);

            _columnItems[columnIndex].playedStopSound = true;

            //first be sure that this columns is moving
            if (columnIndex >= 0)
            {
                if (_bonusSoundCounter < 3)
                {
                    var nearMatrix = GetNearResultMatrix();
                    var bonuses = nearMatrix[columnIndex].FindAll(x => x == BonusGameTileIndex);
                    if (bonuses.Count == 1)
                    {
                        _bonusSoundCounter++;
                        bool success = Enum.TryParse("Bonus" + _bonusSoundCounter, out Enumerators.SoundType bonusSound);
                        if (!success)
                            bonusSound = Enumerators.SoundType.Bonus3;

                        //Debug.Log("Bonus: " + bonusSound);

                        MainApp.Instance.GameController.SoundController.PlaySound(bonusSound, parameters: new SoundParameters() { Volume = 0.5f });
                    }
                    else
                    {
                        PlayDefaultStopSpinSound();
                    }
                }
                else
                {
                    PlayDefaultStopSpinSound();
                }
            }
        }

        private void PlayDefaultStopSpinSound()
        {
            MainApp.Instance.GameController.SoundController.PlaySound(Enumerators.SoundType.StopSpin);
        }

        private void SetFreeSpins(int amount)
        {
            FreeSpinsLeft = amount;
            //Debug.Log("Free spins now: " + FreeSpinsLeft);
        }

        public bool Run(bool isAllColumns = true)
        {
            Debug.Log("Run!!!");

            if (FreeSpinsLeft <= 0)
                AccumulatedMoney = 0;

            needToShowWinnerPopup = false;
            StartingBonusGame = false;
            _lastSpinResult = null;
            winLineId = 0;
            layerIndex = 0;
            _bonusSoundCounter = 0;
            counter = 1;
            _finishedSpins = false;
            _needStopSpin = false;
            ResetLineSequence();
            InternalTools.KillSequence(_certainLinesSequence);
            ClearStopColumnSequences();
            ResetStopSounds();

            _gameController.GameIntefacePage.SetGeneralButtonsInteractablity(false);

            bool hasMoney = HasPlayerEnoughMoneyToBet();

            if (!hasMoney)
            {
                hasMoney = SetLowerBet();
            }

            if (hasMoney)
            {
                if (freeSpin)
                {
                    if (FreeSpinsLeft > 0)
                        SetFreeSpins(FreeSpinsLeft - 1);

                    OnFreeSpinsUpdated?.Invoke(FreeSpinsLeft);
                }

                _timeElapsedFromLastSpin = 0;
                IsSpinning = true;
#if !DEV_LOCAL
                ClassicGames.Hub.Common.GameHolderTool.SetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.SpinStatus, new object[] { IsSpinning });
#endif
                if (isAllColumns)
                {
                    SetAllColumntActiveState();
                }

                if (_singleColumnSpin && !endedSpining)
                {
                    _gameController.GameIntefacePage.ChangeSpinButtonsState(false);
                    StartOneColumnSpinImmediately();
                }
                else if (endedSpining)
                {
                    StartSpinImmediately();

                    connectionStatus = true;
#if !DEV_LOCAL
					if (FreeSpinsLeft >= 0)
					{
						int value = _currentMaxFreeSpins - FreeSpinsLeft;
						string message = string.Format(Constants.MessageFreeGames, value, _currentMaxFreeSpins);

						ClassicGames.Hub.Common.GameHolderTool.SetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.GameInfoPopup,
							new object[] { ClassicGames.Hub.Common.GameHolderTool.GameInfoType.FreeSpin, message });
					}
#endif

#if !DEV_LOCAL
					connectionStatus = (bool)ClassicGames.Hub.Common.GameHolderTool.GetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.ConnectStatus)[0];
#endif

                    if (!connectionStatus)
                        return true;

                    InternalTools.KillSequence(_movementTimeoutSequence);
                    _movementTimeoutSequence = InternalTools.DoActionDelayed(OnDisconnectedHandler, Constants.TimeoutBetResponce);

                    MainApp.Instance.NetworkController.Bet(_totalBet, (status) =>
                    {
                        if (!status)
                        {
                            ConectionError();
                            SetLosingBetResponse();
                        }
                    });
                }
                else
                {
                    return false;
                }
            }
            else
            {
                NotEnoughMoneyToBet();
                return false;
            }
            return true;
        }

        private void NotEnoughMoneyToBet()
        {
            Debug.Log("NotEnoughMoneyToBet");
            _gameController.GameIntefacePage.StopAutospins();
            _gameController.GameIntefacePage.SetAllButtonsInteractablity(true);
        }

        private void ConectionError()
        {
            Debug.Log("ConectionError");
            //InformationPopupData infoPopupData = new InformationPopupData("ERROR", "Connection error", "OK");
            //_gameController.InfoPopup.Show(infoPopupData);
        }

        private void OnDisconnectedHandler()
        {
            Debug.Log("OnDisconnectedHandler");
            SetLosingBetResponse();
            //InformationPopupData infoPopupData = new InformationPopupData("TIPS", "Disconnect", "YES");
            //_gameController.InfoPopup.Show(infoPopupData);
        }

        private void SetLosingBetResponse()
        {
            var losingMatrix = GetLosingMatrix();

            string matrx = "";
            foreach (var item in losingMatrix.resultMatrix)
            {
                matrx += item + " ";
            }
            Debug.Log("SetLosingBetResponse: " + matrx);

            HandleBetResponse(losingMatrix);
            _gameController.GameIntefacePage.SetAutospinInteractability();
        }

        private SpinResultData GetLosingMatrix()
        {
            SpinResultData spinResultData = SlotsServer.GetFailedSpinData();
            for (int i = 0; i < spinResultData.resultMatrix.Length; i++)
            {
                spinResultData.resultMatrix[i] = GetLosingIndex(i, i >= 2);
            }

            return spinResultData;
        }

        private int GetLosingIndex(int columnIndex, bool checkOnlyForBonuses)
        {
            int index = 0;
            bool notAsPrev, notAsNext, prevNextDifferent, noBonuses;
            List<int> bonuses = new List<int>() { BonusGameTileIndex, WildFirstTileIndex }; //Add this for 2 and 3 mechanics as well: WildSecondTileIndex

            for (int j = 1; j < _columns[columnIndex].Count - 1; j++)
            {
                notAsPrev = _columns[columnIndex][j].number != _columns[columnIndex][j - 1].number;
                notAsNext = _columns[columnIndex][j].number != _columns[columnIndex][j + 1].number;
                prevNextDifferent = _columns[columnIndex][j - 1].number != _columns[columnIndex][j + 1].number;
                noBonuses = !bonuses.Contains(_columns[columnIndex][j].number) && !bonuses.Contains(_columns[columnIndex][j - 1].number) && !bonuses.Contains(_columns[columnIndex][j + 1].number);

                if ((checkOnlyForBonuses && noBonuses) || (notAsPrev && notAsNext && prevNextDifferent && noBonuses))
                {
                    index = _columns[columnIndex][j].posNum;
                    break;
                }
            }

            return index;
        }


        public void SetBonusInfo(int spinsLeft, int multiplier)
        {
            FreeSpinsLeft = spinsLeft;
            Multiplier = multiplier;
            _currentMaxFreeSpins = FreeSpinsLeft;
        }

        public void RunBonusGame()
        {
            _gameController.GameIntefacePage.SetAllButtonsInteractablity(true);
            _gameController.GameIntefacePage.SetGeneralButtonsInteractablity(false);
            _gameController.GameIntefacePage.SetAutospinInteractability();

            Debug.LogWarning("RunBonusGame");
            Debug.LogWarning(bonusGame);
            if (!bonusGame)
            {
                _gameController.GameIntefacePage.ChangeSpinButtonsState(true);
                _gameController.GameIntefacePage.SetAutospinInteractability();
                _gameController.SoundController.PlaySound(Enumerators.SoundType.FreeGameBackground, "", new SoundParameters()
                {
                    FadeIn = false,
                    Loop = true,
                    Time = 0f,
                    Volume = 1f,
                    AffectedVolume = 0f
                });
            }

            bonusGame = true;
            freeSpinWinValue = 0;
            _freeSpinsFrames.SetActive(FreeSpinsLeft > 0);
            _freeSpinsColumn.SetActive(FreeSpinsLeft > 0);
           // _freeSpinsBackground.SetActive(FreeSpinsLeft > 0);
            _freeSpinsLineMask.SetActive(FreeSpinsLeft > 0);
            _lineMask.SetActive(FreeSpinsLeft == 0);
            OnFreeSpinsUpdated?.Invoke(FreeSpinsLeft);

            autoSpin = true;
            freeSpin = true;
            Run();
        }

        private void EndSpin(bool stopAll = false)
        {
            var filteredColumns = _columnItems.FindAll(column => (column.currentState != ColumnState.Stopped)).OrderBy(x => x.Index).ToList();

            if (stopAll)
            {
                ClearStopColumnSequences();
                for (int i = 0; i < filteredColumns.Count; i++)
                {
                    int index = Mathf.Clamp(_columnItems.IndexOf(filteredColumns[i]), 0, int.MaxValue);
                    int filteredIndex = i;
                    var sequence = InternalTools.DoActionDelayed(() =>
                    {
                        EndColumnsSpin(filteredColumns[filteredIndex]);

                    }, index * 0.1f);
                    _stopColumnsSequences.Add(sequence);
                }
            }
            else
            {
                if (_firstMoveColumn != null && (_firstMoveColumn.currentState != ColumnState.Stopped))
                {
                    EndColumnsSpin(_firstMoveColumn);
                    _firstMoveColumn = null;
                }
                else
                {
                    EndColumnsSpin(filteredColumns[0]);
                }
            }
        }

        public void StartSpinImmediately()
        {
            _gotEndMatrix = false;

            DisableAllLines();
            ResetLineSequence();
            ResetShells();
            _startSpin = true;
            endedSpining = false;

            _gameController.SoundController.StopSound(Enumerators.SoundType.Win);

            OnSpinRunEvent?.Invoke();

            int id = 0;
            int index = 0;
            var filteredColumns = _columnItems.FindAll(column => column.currentState == ColumnState.Active);
            for (int i = 0; i < filteredColumns.Count; i++)
            {
                id = i;
                index = filteredColumns[id].Index;
                filteredColumns[id].currentState = ColumnState.EndMove;
                SpinColumnContinuously(new object[] { index });
            }

            if (filteredColumns.Count == _columnItems.Count)
            {
                if (bonusGame)
                {
                    _gameController.SoundController.PlaySound(Enumerators.SoundType.SpinFree, parameters: new SoundController.SoundParameters()
                    {
                        Time = 0.3f
                    });
                }
                else
                {
                    _gameController.SoundController.PlaySound(Enumerators.SoundType.Spin, parameters: new SoundController.SoundParameters()
                    {
                        Time = 0.3f,
                        Loop = true
                    });
                }
            }
        }

        private void StartOneColumnSpinImmediately()
        {
            var nonecolumns = _columnItems.FindAll(column => column.currentState == ColumnState.None);
            if (nonecolumns.Count == 0)
            {
                _gameController.SoundController.PlaySound(Enumerators.SoundType.Spin, parameters: new SoundController.SoundParameters()
                {
                    Time = 0.3f
                });
            }

            var filteredColumns = _columnItems.FindAll(column => column.currentState == ColumnState.Active);
            int index = 0;
            if (filteredColumns.Count > 0)
            {
                for (int i = 0; i < filteredColumns.Count; i++)
                {
                    index = filteredColumns[i].Index;
                    filteredColumns[i].currentState = ColumnState.EndMove;
                    SpinColumnContinuously(new object[] { index });
                }
                if (_gotEndMatrix)
                {
                    InternalTools.KillSequence(_currentMovementSequence);
                    _currentMovementSequence = InternalTools.DoActionDelayed(() =>
                    {
                        EndColumnsSpin(filteredColumns[0]);
                    }, UnityEngine.Random.Range(0.2f, 0.3f));
                }
            }
        }

        private List<List<int>> GetNearResultMatrix()
        {
            List<List<int>> nearResultsMatrix = new List<List<int>>();

            foreach (var item in _lastSpinResult.resultMatrix)
            {
                List<int> nearColumn = new List<int>();
                if (item > 0 && item < 50)
                {
                    nearColumn.Add(item);
                    nearColumn.Add(item + 1);
                    nearColumn.Add(item - 1);
                }
                else if (item == 0)
                {
                    nearColumn.Add(item);
                    nearColumn.Add(item + 1);
                    nearColumn.Add(50);
                }
                else if (item == 50)
                {
                    nearColumn.Add(item);
                    nearColumn.Add(0);
                    nearColumn.Add(item - 1);
                }
                nearResultsMatrix.Add(nearColumn);
            }

            return nearResultsMatrix;
        }

        private int CheckFreeSpinsCount()
        {
            if (FreeSpinsLeft < 0)
                SetFreeSpins(0);

            switch (_lastSpinResult.shellInfo.Length)
            {
                case 2:
                    {
                        SetFreeSpins(FreeSpinsLeft + 5);
                        break;
                    }
                case 3:
                    {
                        SetFreeSpins(FreeSpinsLeft + 10);
                        break;
                    }
                case 4:
                    {
                        SetFreeSpins(FreeSpinsLeft + 20);
                        break;
                    }
                case 5:
                    {
                        SetFreeSpins(FreeSpinsLeft + 60);
                        break;
                    }
                default: { break; }
            }

            Multiplier = FreeSpinsLeft > 0 ? Constants.MultiplierBonusGame : 1;

            return FreeSpinsLeft;
        }

        private bool _startSpin;

        private void EndColumnsSpin(ColumnItem columnItem)
        {
            if (columnItem.currentState == ColumnState.Stopped)
            {
                return;
            }

            InternalTools.KillSequence(_startMovementSequence);
            InternalTools.KillSequence(_currentMovementSequence);
            int index = columnItem.Index;
            StopColumnMovement(index);
            float deltaY = CalculateDeltaY(index);
            columnItem.currentState = ColumnState.EndMove;
            StopColumnMove(index, deltaY);
        }

        public void ForceEndColumnSpin(ColumnItem columnItem)
        {
            Action actionEnd = null;
            actionEnd = () =>
            {
                GotMatrixEvent -= actionEnd;

                if (columnItem.currentState == ColumnState.Stopped)
                {
                    return;
                }

                StopColumnMovement(columnItem.Index);
                StopColumnMove(columnItem.Index, CalculateDeltaY(columnItem.Index));
            };

            if (!_gotEndMatrix)
            {
                GotMatrixEvent += actionEnd;
                return;
            }

            actionEnd();
        }

        private bool _needStopSpin;

        public void StopSpin()
        {
            // showing free spins winner popup after the last line animation
            if (needToShowWinnerPopup)
            {
                ResetLineSequence();
                ShowFreeSpinsWinnerPopup();
                return;
            }

            _timeElapsedFromLastSpin += MIN_SPINS_DURATION_WHEN_STOPPED;
            _needStopSpin = true;
        }

        private float CalculateDeltaY(int column)
        {
            int idCentralItem = _centralItems[column];

            var centralItems = _columns[column].FindAll(slot => slot.posNum == idCentralItem);

            float nearestYPosition = 10000;//centralItems[centralItems.Count - 1].selfTransform.position.y;
            float deltaOffsetY;

            foreach (var item in centralItems)
            {
                deltaOffsetY = item.selfTransform.localPosition.y + item.selfTransform.parent.localPosition.y;

                if (deltaOffsetY < 0)
                {
                    deltaOffsetY += _blockOfSlots[column][0].Size;
                }

                if (deltaOffsetY > 0 && deltaOffsetY <= nearestYPosition)
                {
                    nearestYPosition = deltaOffsetY;
                }
            }

            return nearestYPosition;
        }

        public void StopColumnMove(int column, float delta)
        {
            for (int i = 0; i < _blockOfSlots[column].Count; i++)
            {
                if (_needStopSpin)
                    _blockOfSlots[column][i].StopFastWithDelay(delta);
                else
                    _blockOfSlots[column][i].StopMove(delta);
            }
        }

        public void StopColumnMovement(int column)
        {
            for (int i = 0; i < _blockOfSlots[column].Count; i++)
            {
                _blockOfSlots[column][i].UpdateMovementState(false);
            }
        }

        public void SpinColumnContinuously(object[] data)
        {
            int col = (int)data[0];

            for (int i = 0; i < _blockOfSlots[col].Count; i++)
            {
                _blockOfSlots[col][i].MoveUp(col);
            }
        }

        private bool SetLowerBet()
        {
            for (int i = bets.Length; i >= 0 && currentBetIndex > 0; i--)
            {
                currentBetIndex--;
                UpdateTotalBet();
                if (HasPlayerEnoughMoneyToBet())
                    return true;
            }

            return false;
        }

        public bool HasPlayerEnoughMoneyToBet()
        {
            return _playerMoney.Money >= _totalBet || freeSpin;
        }

        public void SetImages()
        {
            sprites = new List<KeyValuePair<int, Sprite>>();
            KeyValuePair<int, Sprite> sprite;

            SpriteAtlas spritesPack = _loadObjectsController.GetObjectByPath<SpriteAtlas>("images/icons/icons");

            for (int i = 0; i < AmountOfTiles; i++)
            {
                var loadAssetRequest = spritesPack.GetSprite("Layer" + (i + 1));//Resources.Load<Sprite>("Images/Icons/Icons/Layer" + (i + 1));
                sprite = new KeyValuePair<int, Sprite>(i, loadAssetRequest);
                sprites.Add(sprite);
            }
        }

        public void StopAnimation()
        {
            for (int col = 0; col < ColumnsCount; col++)
            {
                if (_currentSequences.Count > 0)
                {
                    _currentSequences[0].Kill();
                    _currentSequences.RemoveAt(0);
                }
            }
        }

        public void CheckResult()
        {
            Debug.Log("CheckResult");
            IsSpinning = false;
#if !DEV_LOCAL
            ClassicGames.Hub.Common.GameHolderTool.SetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.SpinStatus, new object[] { IsSpinning });
#endif
            foreach (var item in _actionsWhenSpinEnds)
            {
                item?.Invoke();
            }

            _actionsWhenSpinEnds.Clear();

            _startSpin = false;
            _needStopSpin = false;
            endedSpining = true;
            _gameController.SoundController.StopSound(Enumerators.SoundType.Spin);

            if (_lastSpinResult == null)
            {
                //Debug.LogError("_lastSpinResult is null");
                return;
            }

            double earned = SlotsServer.GetEarnForSpin(_lastSpinResult);
            if (earned > 0)
            {
                _gameController.SoundController.PlaySound(Enumerators.SoundType.Win);
                if (freeSpin|| SlotsServer.IsBonusGame || _finishingFreeSpins || FreeSpinsLeft >= 0)
                {
                    _finishingFreeSpins = false;
                    FreeSpinsMoney += earned;
                }
            }

            AccumulatedMoney += earned;

            OnSpinDone(earned);

            OnAllColumnMoveEndedHandler();

            if (freeSpin)
            {
                if (FreeSpinsLeft <= 0)
                {
#if !DEV_LOCAL
				ClassicGames.Hub.Common.GameHolderTool.SetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.CloseGameInfoPopup, null);
#endif
                    needToShowWinnerPopup = true;
                    Multiplier = 1;
                    SetFreeSpins(-1);
                    freeSpin = false;
                    autoSpin = false;
                    bonusGame = false;
                    _freeSpinsFrames.SetActive(false);
                    _freeSpinsColumn.SetActive(false);
                    // _freeSpinsBackground.SetActive(false);
                    _freeSpinsLineMask.SetActive(false);
                    _lineMask.SetActive(true);
                    _gameController.SoundController.StopSound(Enumerators.SoundType.FreeGameBackground, true);

                    StopAnimation();
                    _gameController.GameIntefacePage.SetAfterFreeSpinsState();

                    SlotsServer.IsBonusGame = false;
                    OnFreeSpinsUpdated?.Invoke(FreeSpinsLeft);
                    _finishingFreeSpins = true;
                    Multiplier = 1;
                    _isBonusGameActive = false;

                    if (_lastSpinResult.winLines.Length <= 0)
                    {
                        needToShowWinnerPopup = false;
                        _gameController.WinnerPopup.Show();
                    }

                    //_gameController.SoundController.PlaySound(Enumerators.SoundType.WinPopup);
                    OnEndFreeSpins?.Invoke();
                }
            }

            if (!HasPlayerEnoughMoneyToBet())
                SetLowerBet();

            //everything moved to FinishedSpinHandler
        }

        public void ShowLines(int lines)
        {
            var linesContainer = _slotMachine.transform.Find("Lines");

            for (int i = 0; i < linesContainer.childCount; i++)
            {
                if (linesContainer.GetChild(i).name != "Dots")
                {
                    linesContainer.GetChild(i).gameObject.SetActive(false);
                }
            }

            for (int i = 0; i < lines; i++)
            {
                linesContainer.Find("Lines_" + i).gameObject.SetActive(true);
            }

            if (_linesSequence != null)
            {
                _linesSequence.Kill();
                _linesSequence = null;
            }

            _linesSequence = DOTween.Sequence();
            _linesSequence.AppendInterval(2);
            _linesSequence.AppendCallback(() => DisableAllLines());
            _linesSequence.Play();
        }

        public void ResetLineSequence()
        {
            //Debug.Log("ResetLineSequence (KillAll)");

            StopItemsAnimations();

            InternalTools.KillSequence(_certainLinesSequence);
            InternalTools.KillSequence(_linesSequence);
            InternalTools.KillSequence(_autoSpinsSequence);
            InternalTools.KillSequence(_movementTimeoutSequence);
            InternalTools.KillSequence(_currentMovementSequence);
            InternalTools.KillSequence(_startMovementSequence);
            InternalTools.KillSequence(_delayedRunSequence);

            if (_currentSequences != null && _currentSequences.Count > 0)
            {
                foreach (var item in _currentSequences)
                {
                    InternalTools.KillSequence(item);
                }
            }

            foreach (var view in frameItems)
            {
                view.ChangeState(false);
                view.Dispose();
            }
            foreach (var winItem in _winItems)
            {
                winItem.Dispose();
            }
            _winItems.Clear();
            frameItems.Clear();
            _currentIndexFrame = 0;
        }

        public void ResetShells()
        {
            //Debug.Log("ResetShells");
            foreach (var item in _shellBonusGameItems)
            {
                item.Dispose();
            }
            _shellBonusGameItems.Clear();
        }

        #region Lines Animation Controls

        readonly bool _winSoundWasPlayed = false;
        private void AppendChangingLineState(WinPanelItem winPanel, bool enableFrames, bool enableLine, bool disableAnim, bool overrideTextAnimationState, bool textAnimationState, bool startAutospinManually)
        {
            //Debug.Log("AUTOSPIN STATE = " + autoSpin);
            _certainLinesSequence.AppendCallback(() =>
            {
                winPanel.ChangeState(enableLine);
                ChangeFrameState(winPanel, enableFrames, true, disableAnim, 0, overrideTextAnimationState, textAnimationState);

                // if this is the last animation step, check for autospin
                if (!startAutospinManually && !enableFrames && !enableLine && !overrideTextAnimationState && !textAnimationState)
                {
                    // showing free spins winner popup after the last line animation
                    if (needToShowWinnerPopup)
                    {
                        ShowFreeSpinsWinnerPopup();
                    }
                    // if fast autospin
                    else if (FreeSpinsLeft <= 0 && autoSpin)
                    {
                        _certainLinesSequence?.Kill();
                        AddAutospinToSequence();
                    }
                }
            });
        }

        private void ShowFreeSpinsWinnerPopup()
        {
            ResetLineSequence();
            needToShowWinnerPopup = false;
            _gameController.WinnerPopup.Show();
        }

        private void AddInterval(float amount)
        {
            _certainLinesSequence.AppendInterval(amount);
        }

        private void AddSingleLineBlink(WinPanelItem winPanel, bool startAutospinManually)
        {
            AppendChangingLineState(winPanel, true, true, true, true, true, startAutospinManually);
            AddInterval(LINE_BLINK_INTERVAL);
            AppendChangingLineState(winPanel, false, false, false, false, true, startAutospinManually);
            AddInterval(LINE_BLINK_INTERVAL);
            AppendChangingLineState(winPanel, true, true, true, false, true, startAutospinManually);
            AddInterval(LINE_BLINK_INTERVAL);
            AppendChangingLineState(winPanel, false, false, false, false, false, startAutospinManually);
            AddInterval(LINE_BLINK_INTERVAL);
        }

        #endregion

        private void AddAutospinToSequence()
        {
            //Debug.Log("AddAutospinToSequence");

            // Autospin Starts here
            if (autoSpin)
            {
                StopItemsAnimations();
                // if not bonus game, autospin
                if (StartingBonusGame == false)
                {
                    Run();
                }
            }
        }

        private void StopItemsAnimations()
        {
            foreach (var item in _winItems)
            {
                item.ChangeState(false);
            }
            ChangeFrameState(false);
        }

        private void FinishActionsAfterSpin(bool checkAndstartAutospin = false)
        {
            if (SlotsServer.IsBonusGame)
            {
                CheckOnBonusGame();
            }

            _gameController.GameIntefacePage.SetAllButtonsInteractablity(true);

            if (checkAndstartAutospin)
            {
                AddAutospinToSequence();
                if (autoSpin)
                {
                    _gameController.GameIntefacePage.SetGeneralButtonsInteractablity(false);
                }
            }

            if (autoSpin)
            {
                _gameController.GameIntefacePage.SetGeneralButtonsInteractablity(false);
            }
        }

        public void OnFiveOfAKindPopupClosed()
        {
            Debug.Log("5 of a kind closed");

            if (is5OfAKindOpen)
            {
                is5OfAKindOpen = false;
                _gameController.GameIntefacePage.StartCountersManually();

#if !DEV_LOCAL
                PlayLineSequence(_winPopupsInfo.Count > 0);
#else
                PlayLineSequence(false); // for local test
#endif
            }

#if !DEV_LOCAL
            if (_winPopupsInfo.Count > 0)
            {
                OpenFirstWinPopup();
            }
            else
            {
                FinishActionsAfterSpin(!needToShowWinnerPopup);
                if (needToShowWinnerPopup)
                {
                    ShowFreeSpinsWinnerPopup();
                }
            }
#else
            FinishActionsAfterSpin(!needToShowWinnerPopup);
            if (needToShowWinnerPopup)
            {
                ShowFreeSpinsWinnerPopup();
            }
#endif
        }

        public void OnReconectedHandler()
        {
            if (!connectionStatus && IsSpinning)
            {
                _movementTimeoutSequence = InternalTools.DoActionDelayed(OnDisconnectedHandler, Constants.TimeoutBetResponce);

                MainApp.Instance.NetworkController.Bet(_totalBet, (status) =>
                {
                    if (!status)
                    {
                        ConectionError();
                        SetLosingBetResponse();
                    }
                });
            }
        }

        public void OnUpdateBetHandler()
        {
#if !DEV_LOCAL
            object[] data = ClassicGames.Hub.Common.GameHolderTool.GetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.JackpotStatusData);
            _jackpotStatus = (bool)data[0];
            _globalFee = (double)data[1];
#endif
            UpdateTotalBet();
        }

        private bool CheckFor5OfAKind()
        {
            int successCount = 0;
            bool success = false;
            int kindKinesReward = 0;
            isWinnerPopupOpen = false;

            for (int i = 0; i < _lastSpinResult.winLines.Length; i++)
            {
                if (_lastSpinResult.winLines[i].winnersIcons != null && _lastSpinResult.winLines[i].winnersIcons.Length == 5)
                {
                    successCount++;
                    kindKinesReward += (int)_lastSpinResult.winLines[i].winValue;
                }
            }

#if !DEV_LOCAL
            _winPopupsInfo.Clear();

            if (successCount > 0 && _lastSpinResult != null)
            {
                _winPopupsInfo.Add(Hub.Common.GameHolderTool.WinPopupType.Kind, kindKinesReward);
            }

            foreach (var multiplierInfo in _winMultipliers)
            {
                if (_lastSpinResult.totalReward >= _totalBet * multiplierInfo.Value)
                {
                    _winPopupsInfo.Add(multiplierInfo.Key, (int)_lastSpinResult.totalReward);
                    break;
                }
            }

            success = _winPopupsInfo.Count > 0;
#endif

#if UNITY_EDITOR
            if (Input.GetKey(KeyCode.KeypadDivide))
            {
                Debug.Log("--5 of a kind test");
                success = true;
                successCount = 1;
            }
#endif

            is5OfAKindOpen = successCount > 0;
            if (success)
            {
                OpenFirstWinPopup();
            }

            return success;
        }

        private void FillWinMultipliers()
        {
#if !DEV_LOCAL
            _winPopupsInfo = new Dictionary<Hub.Common.GameHolderTool.WinPopupType, int>();
            _winMultipliers = new Dictionary<Hub.Common.GameHolderTool.WinPopupType, int>()
            {
                { Hub.Common.GameHolderTool.WinPopupType.UltraWin500, 500 },
                { Hub.Common.GameHolderTool.WinPopupType.UltraWin100, 100 },
                { Hub.Common.GameHolderTool.WinPopupType.MegaWin50, 50 },
                { Hub.Common.GameHolderTool.WinPopupType.SuperWin20, 20 },
                { Hub.Common.GameHolderTool.WinPopupType.BigWin10, 10 },
                { Hub.Common.GameHolderTool.WinPopupType.BigWin5, 5 }
            };
#endif
        }

        private void OpenFirstWinPopup()
        {
#if !DEV_LOCAL
            ClassicGames.Hub.Common.GameHolderTool.SetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.GameWinPopup,
                new object[] { _winPopupsInfo.Keys.First(), _winPopupsInfo.Values.First() });

            if (!isWinnerPopupOpen)
            {
                isWinnerPopupOpen = true;
                _gameController.GameIntefacePage.ChangeSpinButtonsState(false);
                _gameController.GameIntefacePage.SetAllButtonsInteractablity(false);
                _gameController.GameIntefacePage.SetAutospinInteractability(false);
            }

            _winPopupsInfo.Remove(_winPopupsInfo.Keys.First());
#endif
        }

        public void FinishedSpinHandler()
        {
            //Debug.Log("FinishedSpinHandler");

            // if true, wait when popup is closed, then check bonus or free game and start autospin
            bool fiveOfAKind = CheckFor5OfAKind();
            bool startAutospinManually = fiveOfAKind;

            if (!fiveOfAKind)
            {
                FinishActionsAfterSpin();
            }
            else
            {
#if DEV_LOCAL
                _gameController.GameIntefacePage.SetAllButtonsInteractablity(false);
                Debug.Log("test five of a kind");
                InternalTools.DoActionDelayed(OnFiveOfAKindPopupClosed, 4f);
#endif
            }

            if (is5OfAKindOpen == false)
                PlayLineSequence(startAutospinManually);
        }

        /// <summary>
        /// Must be called before all the winLines!
        /// </summary>
        public void AddBonusesAsWinFrames()
        {
            //Debug.Log("Adding bonuses: last = " + _lastSpinResult);

            if (_lastSpinResult != null && _lastSpinResult.shellInfo != null && _lastSpinResult.shellInfo.Length >= 3) // we gain money when there are 3 or more
            {
                double winLinesReward = _lastSpinResult.winLines != null && _lastSpinResult.winLines.Length > 0 ? _lastSpinResult.winLines.Sum(x => x.winValue) : 0;
                int winValue = (int)(_lastSpinResult.totalReward - winLinesReward);

                // this way we determine the most left bonus in visible matrix
                SlotsItem nearestItem = GetNearestSlotItemById(_lastSpinResult.shellInfo[0].col, _lastSpinResult.shellInfo[0].pos);

                _currentIndexFrame++;
                frameItems.Insert(0, new WinnerFrameItem(_parentOfWinnerItems, _currentIndexFrame,
                    -1,
                    BonusGameTileIndex,
                    nearestItem.posNum,
                    null,
                    nearestItem.selfObject.transform.position,
                    nearestItem.InitialSprite,
                    line[0].endColor,
                    nearestItem.number + 1,
                    _loadObjectsController.GetObjectByPath<GameObject>("prefabs/gameplay/framewinner"),
                    AnimationFrames.ContainsKey(nearestItem.number + 1) ? AnimationFrames[nearestItem.number + 1] : null,
                    SlotsServer.IsBonusGame && !SlotsServer.IsBonusGameOpen,
                    winValue
                    ));
                frameItems[frameItems.Count - 1].isBonusElement = true;
                frameItems[frameItems.Count - 1].selfObject.name = "BONUS_WIN_FRAME";
                //frameItems[frameItems.Count - 1].UpdateAnimation(true);

                WinPanelItem winPanel = new WinPanelItem(null, new List<int>() { _currentIndexFrame }, Color.white, null, -1, winValue);
                _winItems.Insert(0, winPanel);
            }
        }

        public void PlayLineSequence(bool startAutospinManually)
        {
            //Debug.Log("PlayLineSequence: " + _winItems.Count + ", time: " + DateTime.Now);

            float lineBlinkInterval = LINE_BLINK_INTERVAL;
            float lineSwitchInterval = LINE_SWITCH_INTERVAL;

            bool winLineCondition = _winItems.Count >= 1;

            InternalTools.KillSequence(_autoSpinsSequence);
            _autoSpinsSequence = DOTween.Sequence();

            //_autoSpinsSequence.SetDelay(delay);
            //_autoSpinsSequence.AppendInterval(intervalValue);
            _autoSpinsSequence.AppendCallback(() =>
            {
                if (winLineCondition)
                {
                    InternalTools.KillSequence(_certainLinesSequence);
                    _certainLinesSequence = DOTween.Sequence();

                    foreach (var winPanel in _winItems)
                    {
                        if (_winItems.Count == 1)
                        {
                            AddSingleLineBlink(winPanel, startAutospinManually);
                            _certainLinesSequence.AppendInterval(lineSwitchInterval);
                        }
                        else
                        {
                            AddSingleLineBlink(winPanel, startAutospinManually);
                            _certainLinesSequence.AppendInterval(lineBlinkInterval);
                        }
                    }

                    if (!startAutospinManually)
                    {
                        _certainLinesSequence.AppendCallback(() =>
                        {
                            AddAutospinToSequence();
                        });
                    }

                    _certainLinesSequence.SetLoops(-1);
                    _certainLinesSequence.Play();
                }
                else
                {
                    if (!startAutospinManually)
                    {
                        AddAutospinToSequence();
                    }
                }
            });
        }

        List<WinnerFrameItem> frameItems = new List<WinnerFrameItem>();

        private WinnerFrameItem GetFrame(int column, int posNum)
        {
            if (frameItems == null)
            {
                return null;
            }

            return frameItems.Find(frame => frame.column == column && frame.posNum == posNum);
        }

        private WinnerFrameItem GetFrame(int index)
        {
            if (frameItems == null)
            {
                return null;
            }

            return frameItems.Find(frame => frame.index == index);
        }

        int counter = 1;
        int winLineId = 0;
        int layerIndex = 0;
        private void ChangeFrameState(WinPanelItem winPanel, bool disabled = true, bool useDisabledState = false, bool disableAnim = true, int index = 0, bool overrideTextAnimationState = false, bool textAnimationState = true)
        {
            if (winLineId <= _lastSpinResult.winLines.Length - 1 && counter % 4 == 0)
            {
                foreach (var spinResultItem in _lastSpinResult.winLines[winLineId].winnersIcons)
                {

                    if (spinResultItem.num != 11 && spinResultItem.num != 12)
                    {
                        layerIndex = spinResultItem.num;
                        //Debug.LogError("spinResultItem.num " + spinResultItem.num);
                    }
                }
                if (!bonusGame)
                {
                    if (!IsSpinning && (!autoSpin || FreeSpinsLeft > 0))
                    {
                        MainApp.Instance.GameController.SoundController.PlaySound(Enumerators.SoundType.WinFrame, "/SlotItem_Layer" + (layerIndex + 1), null, false);
                    }
                }
                if (winLineId < _lastSpinResult.winLines.Length)
                {
                    //Debug.LogWarning("Winline ID " + winLineId);
                    //Debug.LogWarning("Layer ID " + layerIndex);
                    //Debug.LogWarning("Counter " + counter);
                    winLineId++;
                }
            }
            counter++;

            if (index > 0)
            {

            }

            if (winPanel == null || winPanel.Frames == null)
            {
                return;
            }

            var startFrame = frameItems.Find(frame => frame.IsActive && winPanel.Frames.Contains(frame.index));
            float animationTime = 0;
            if (startFrame != null)
            {
                animationTime = startFrame.AnimationTime;
            }

            // sorting from left to right
            var currentFrames = frameItems.FindAll(x => winPanel.Frames.Contains(x.index)).OrderBy(x => x.selfObject.transform.position.x).ToList();
            WinnerFrameItem firstFrame = currentFrames.Count > 0 ? currentFrames[0] : null;

            foreach (var frame in frameItems)
            {
                if (winPanel.Frames.Exists(x => x == frame.index))
                {
                    frame.ChangeBorderState(!useDisabledState ? true : disabled, winPanel.LineColor, animationTime, disableAnim);
                }
                else if (disabled)
                {
                    frame.ChangeBorderState(false, disableAnim: disableAnim);
                }
            }

            if (firstFrame != null)
            {
                firstFrame.SetWinTextAmount(winPanel.winValue);
                if (overrideTextAnimationState)
                    firstFrame.UpdateAnimation(textAnimationState);
            }
        }

        private void ChangeFrameState(bool state)
        {
            //Debug.Log("ChangeFrameState: ");
            //Debug.Log("frame items count " + frameItems.Count);
            foreach (var frame in frameItems)
            {
                frame.ChangeState(state);
            }
        }

        public void ShowCertainLines(int lineIndex, SpinResultWinInfoItem numsWin)
        {
            //Debug.Log("ShowCertainLines: " + lineIndex + ", time: " + DateTime.Now);

            GameObject lineObject = _slotMachine.transform.Find("Lines/Lines_" + lineIndex).gameObject;
            Color lineColor = line[lineIndex].endColor;
            List<int> indexes = new List<int>();
            for (int i = 0; i < numsWin.winnersIcons.Length; i++)
            {
                SlotsItem nearestItem = GetNearestSlotItemById(numsWin.winnersIcons[i].col, numsWin.winnersIcons[i].pos);
                var tempFrame = GetFrame(numsWin.winnersIcons[i].col, nearestItem.posNum);
                if (tempFrame != null)
                {
                    indexes.Add(tempFrame.index);
                }
                else
                {
                    _currentIndexFrame++;
                    indexes.Add(_currentIndexFrame);
                    frameItems.Add(new WinnerFrameItem(_parentOfWinnerItems, _currentIndexFrame,
                        numsWin.winnersIcons[i].col,
                        numsWin.winnersIcons[i].num,
                        nearestItem.posNum,
                        null,
                        nearestItem.selfObject.transform.position,
                        nearestItem.spriteRenderer.sprite,
                        lineColor,
                        nearestItem.number + 1,
                        _loadObjectsController.GetObjectByPath<GameObject>("prefabs/gameplay/framewinner"),
                                                AnimationFrames.ContainsKey(nearestItem.number + 1) ? AnimationFrames[nearestItem.number + 1] : null,
                        //SlotsServer.IsBonusGame && !SlotsServer.IsBonusGameOpen
                        bonusGame, (int)numsWin.winValue
                        ));
                }
            }

            WinPanelItem winPanel = new WinPanelItem(lineObject, indexes, lineColor, dots[lineIndex], lineIndex + 1, numsWin.winValue);
            winPanel.ChangeState(false);
            _winItems.Add(winPanel);
        }

        public void ShowShellAnimations()
        {
            if (_lastSpinResult.shellInfo.Length > 1)
            {
                SlotsItem slotsItem = null;
                foreach (var shellInfo in _lastSpinResult.shellInfo)
                {
                    slotsItem = GetNearestSlotItemById(shellInfo.col, shellInfo.pos);

                    _shellBonusGameItems.Add(new ShellBonusGameItem(slotsItem.selfObject.transform,
                            shellInfo.col,
                            shellInfo.num,
                            _loadObjectsController.GetObjectByPath<GameObject>("prefabs/ui/shellview"),
                            AnimationFrames[shellInfo.num + 1],
                            slotsItem));
                }
            }
        }

        private SlotsItem GetNearestSlotItemById(int column, int id)
        {
            var items = _columns[column].FindAll(slot => slot.posNum == id);
            SlotsItem nearestItem = items[0];
            foreach (var item in items)
            {
                if (Mathf.Abs(item.selfTransform.position.y) < Mathf.Abs(nearestItem.selfTransform.position.y))
                {
                    nearestItem = item;
                }
            }
            return nearestItem;
        }

        private void CheckOnBonusGame()
        {
            if (_isBonusGameActive)
            {
                if (!bonusGame)
                { 
                    wasAutoSpinBeforeFreeSpins = autoSpin;
                    autoSpin = true;
#if !DEV_LOCAL
                ClassicGames.Hub.Common.GameHolderTool.SetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.SpinStatus, new object[] { true });
#endif
                    StartingBonusGame = true;
                    _soundController.PlaySound(Enumerators.SoundType.HotFreeSpins_Voice);
                    ShowShellAnimations();

                    _gameController.GameIntefacePage.SetAllButtonsInteractablity(false);
                    _gameController.GameIntefacePage.SetAutospinInteractability();

                    InternalTools.KillSequence(_delayedRunSequence);
                  //  _delayedRunSequence = InternalTools.DoActionDelayed(() => { RunBonusGame(); }, 4f);
                    _delayedRunSequence = InternalTools.DoActionDelayed(OpenBonusGame, 4f);
                    //OpenBonusGame();

                }
                //else if (FreeSpinsLeft < 0)
                //{
                //                InternalTools.KillSequence(_delayedRunSequence);
                //                float delay = _lastSpinResult != null && _lastSpinResult.totalReward > 0 ? 2f : 1f;
                //                _delayedRunSequence = InternalTools.DoActionDelayed(() => { Run(); }, delay);
                //            }
            }

        }
        private void OpenBonusGame()
        {
            ResetShells();
            _gameController.BonusGame.Show();
        }

        public void DisableAllLines()
        {
            foreach (var lineItem in line)
            {
                lineItem.gameObject.SetActive(false);
            }

            foreach (var item in _winItems)
            {
                item.ChangeState(false);
            }
            counter = 1;
        }

        public void AddListenerToGameInterfacePageButtons()
        {
            BetsControll();
            LinesControll();
        }

        private void LinesControll()
        {
            //	_gameController.GameIntefacePage.buttonAddLine.onClick.AddListener(OnAddLineButtonOnClickHandler);
            //	_gameController.GameIntefacePage.buttonDecreaseLine.onClick.AddListener(OnDecreaseLineButtonOnClickHandler);
        }

        private void OnAddLineButtonOnClickHandler()
        {
            currentLines++;
            if (currentLines > maxLine)
            {
                currentLines = 1;
            }

            ShowLines(currentLines);
            OnLineChange?.Invoke(currentLines);
            UpdateTotalBet();

            SlotsServer.currentLineCount = currentLines;
        }

        private void OnDecreaseLineButtonOnClickHandler()
        {
            currentLines--;
            if (currentLines < 1)
            {
                currentLines = maxLine;
            }

            ShowLines(currentLines);
            OnLineChange?.Invoke(currentLines);
            UpdateTotalBet();

            SlotsServer.currentLineCount = currentLines;
        }

        public void OnAddBetButtonOnClickHandler()
        {
            currentBetIndex++;
            if (currentBetIndex >= bets.Length)
            {
                currentBetIndex = 0;
            }
            currentBetLevel = betLevels[currentBetIndex];
            UpdateTotalBet();
        }

        public void OnDecreaseBetButtonOnClickHandler()
        {
            Debug.LogWarning("OnDecreaseBetButtonOnClickHandler");
            currentBetIndex--;
            if (currentBetIndex < 0)
            {
                currentBetIndex = bets.Length - 1;
            }
            currentBetLevel = betLevels[currentBetIndex];
            UpdateTotalBet();
        }

        private void BetsControll()
        {
            //_gameController.GameIntefacePage._increaseBetButton.onClick.AddListener(OnAddBetButtonOnClickHandler);
            //_gameController.GameIntefacePage._decreaseBetButton.onClick.AddListener(OnDecreaseBetButtonOnClickHandler);
        }
    }
}
