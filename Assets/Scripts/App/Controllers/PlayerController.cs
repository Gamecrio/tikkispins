﻿using System;

namespace ClassicGames.WildBilly
{
	public class PlayerController
	{
		public event Action OnUserDataLoadedEvent;

		public Player LocalPlayer { get; set; }


		public double Money
		{
			get
			{
				return LocalPlayer.balance;
			}
			set
			{
				LocalPlayer.balance = value;

				if (LocalPlayer.balance < 0)
					LocalPlayer.balance = 0;
			}
		}

		public double TotalWinMoney
		{
			get
			{
				return LocalPlayer.winBalance;
			}
			set
			{
				LocalPlayer.winBalance = value;

				if (LocalPlayer.winBalance < 0)
					LocalPlayer.winBalance = 0;
			}
		}

		public void Init()
		{
		}

		public void Update()
		{
		}

		public void Dispose()
		{
		}

		public void SetUserData(Player player)
		{
			LocalPlayer = player;
			OnUserDataLoadedEvent?.Invoke();
		}
	}
}