﻿using ClassicGames.WildBilly.Common;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

namespace ClassicGames.WildBilly
{
	public class SoundController
	{
		public const float BackgroundSoundValume = 1f;

		private AudioSource _backgroundSound;

		private AudioClip _clickClip;

		private bool _isMute;

		private float _soundVolume = 1f;

		private List<SoundSource> _soundSources;

		private Transform _soundContainer;

		private LoadObjectsController _loadObjectsController;

		public bool IsMute
		{
			get { return _isMute; }
			set
			{
				_isMute = value;
				UpdateMuteState();
			}
		}

		public SoundController(GameController controller)
		{
			_soundSources = new List<SoundSource>();
			_soundContainer = new GameObject("[Sound Container]").transform;

			_loadObjectsController = MainApp.Instance.LoadObjectsController;

			_soundVolume = 1f;

#if !DEV_LOCAL
			// SET STATUS OF SOUND FROM HUB
			_soundVolume = (float)ClassicGames.Hub.Common.GameHolderTool.GetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.Sound)[0];
#endif

			IsMute = _soundVolume == 0;

			//         PlaySound(Enumerators.SoundType.Background, parameters:
			//new SoundParameters()
			//{
			//	Loop = true,
			//	Volume = BackgroundSoundValume
			//});
		}

		public void Dispose()
		{
			for (int i = 0; i < _soundSources.Count; i++)
			{
				_soundSources[i].Dispose();
			}
			_soundSources.Clear();
			if (_soundContainer != null)
			{
				MonoBehaviour.Destroy(_soundContainer.gameObject);
			}
		}

		public void Update()
		{
			for (int i = 0; i < _soundSources.Count; i++)
			{
				if (_soundSources[i].IsSoundEnded())
				{
					_soundSources[i].Dispose();
					_soundSources.RemoveAt(i--);
				}
			}
		}

		public void UpdateSoundStatus()
		{
			_soundVolume = 1f;

#if !DEV_LOCAL
			// SET STATUS OF SOUND FROM HUB
			_soundVolume = (float)ClassicGames.Hub.Common.GameHolderTool.GetDataByType(ClassicGames.Hub.Common.GameHolderTool.DataType.Sound)[0];
#endif
			for (int i = 0; i < _soundSources.Count; i++)
			{
				_soundSources[i].SoundParameters.AffectedVolume = _soundSources[i].SoundParameters.Volume * _soundVolume;
				_soundSources[i].AudioSource.volume = _soundSources[i].SoundParameters.AffectedVolume;
			}

			IsMute = _soundVolume == 0;
		}

		public void PlayClickSound()
		{
			PlaySound(Enumerators.SoundType.Click);
		}

		public void PlaySound(Enumerators.SoundType soundType, string subName = "", SoundParameters parameters = null, bool removeSameSound = true, bool fadeOutSame = true)
		{
			string path = "sounds/";
			AudioClip sound;
			string name = (soundType + subName).ToLowerInvariant();

			sound = _loadObjectsController.GetObjectByPath<AudioClip>($"{path}{name}");

			if (sound == null)
				return;

			SoundSource foundSameSource = _soundSources.Find(soundSource => soundSource.SoundType == soundType);

			if (foundSameSource != null)
			{
				if (removeSameSound)
				{
					if (!fadeOutSame)
						foundSameSource.Dispose();
					else
					{
						foundSameSource.AudioSource.DOFade(0, (0.2f * _soundVolume)).OnComplete(() => { foundSameSource.Dispose(); });
					}
					_soundSources.Remove(foundSameSource);
				}
				else return;
			}

			if (parameters == null)
			{
				parameters = new SoundParameters()
				{
					FadeIn = false,
					Loop = false,
					Time = 0f,
					Volume = 1f,
					AffectedVolume = 0f
				};
			}

			parameters.AffectedVolume = parameters.Volume * _soundVolume;

			_soundSources.Add(new SoundSource(_soundContainer, sound, soundType, parameters, _isMute));
		}

		public void StopSound(Enumerators.SoundType soundType, bool fadeOut = false)
		{
			fadeOut = false; // doesn't work currently

			for (int i = 0; i < _soundSources.Count; i++)
			{
				if (_soundSources[i].SoundType == soundType)
				{
					if (!fadeOut)
						_soundSources[i].Dispose();
					else
					{
						var source = _soundSources[i];
						source.AudioSource.DOFade(0, 0.2f).OnComplete(() => { source.Dispose(); });
					}

					_soundSources.RemoveAt(i--);
				}
			}
		}

		private void UpdateMuteState()
		{
			foreach (var sound in _soundSources)
			{
				sound.AudioSource.mute = _isMute;
			}
		}

		class SoundSource
		{
			public GameObject SoundSourceObject { get; }
			public AudioClip Sound { get; }
			public AudioSource AudioSource { get; }
			public Enumerators.SoundType SoundType { get; }
			public SoundParameters SoundParameters { get; }


			public SoundSource(Transform parent, AudioClip sound, Enumerators.SoundType soundType, SoundParameters parameters, bool mute)
			{
				Sound = sound;
				SoundParameters = parameters;
				SoundType = soundType;
				SoundSourceObject = new GameObject($"[Sound] - {SoundType} - {Time.time}");
				SoundSourceObject.transform.SetParent(parent);
				AudioSource = SoundSourceObject.AddComponent<AudioSource>();
				AudioSource.clip = Sound;
				AudioSource.mute = mute;

				if (SoundParameters != null)
				{
					AudioSource.volume = SoundParameters.AffectedVolume;
					AudioSource.loop = SoundParameters.Loop;
					AudioSource.time = SoundParameters.Time;

					//if (parameters.FadeIn)
					//{
					//    AudioSource.volume = 0;
					//    AudioSource.DOFade(parameters.Volume, 0.2f);
					//}
				}

				AudioSource.Play();
			}

			public bool IsSoundEnded()
			{
				return !AudioSource.loop && !AudioSource.isPlaying;
			}

			public void Dispose()
			{
				AudioSource.Stop();
				MonoBehaviour.Destroy(SoundSourceObject);
			}
		}

		public class SoundParameters
		{
			public bool Loop { get; set; } = false;
			public float Volume { get; set; } = 1f;
			public float AffectedVolume { get; set; } = 1f;
			public float Time { get; set; }
			public bool FadeIn { get; set; } = false;
		}
	}
}