﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ClassicGames.WildBilly
{
    public class MainApp : MonoBehaviour
    {
        private static MainApp _Instance;
        public static MainApp Instance
        {
            get { return _Instance; }
            private set { _Instance = value; }
        }

		public GameController GameController { get; private set; }
		public PlayerController PlayerController { get; private set; }
		public NetworkController NetworkController { get; private set; }
		public DataController DataController { get; private set; }
		public LoadObjectsController LoadObjectsController { get; set; }

		private void Awake()
        {
            if(Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;

			GameController = new GameController();
			PlayerController = new PlayerController();
			NetworkController = new NetworkController();
			DataController = new DataController();
			LoadObjectsController = new LoadObjectsController();
		}

		public static void ActivateFullscreen()
		{
#if UNITY_WEBGL
			Screen.fullScreen = true;
#endif
		}

		private void Start()
        {
            if (Instance == this)
            {
				DataController.Init();
				LoadObjectsController.Init();
				GameController.Init();
				NetworkController.Init();
				PlayerController.Init();
			}
        }

		private void Update()
        {
			if (Instance == this)
			{
				GameController.Update();
				PlayerController.Update();
				NetworkController.Update();
				DataController.Update();
				LoadObjectsController.Update();
			}
        }

        private void LateUpdate()
        {
            if (Instance == this)
            {

            }
        }

        private void FixedUpdate()
        {
            if (Instance == this)
            {

            }
        }

        private void OnDestroy()
        {
			DataController?.Dispose();
			LoadObjectsController?.Dispose();
			GameController?.Dispose();
			PlayerController?.Dispose();
			NetworkController?.Dispose();
		}
	}
}