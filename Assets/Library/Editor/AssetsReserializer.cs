﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEditor;
using UnityEngine;

public class AssetsReserializer
{
    [MenuItem("Utilities/Reserialize Assets")]
    private static void ReserializeAssets()
    {
        string projectName = "wild-billy";

        var realPath = Application.dataPath;
        realPath = realPath.Remove(realPath.Length - 6);
        var selectedPath = realPath + "Assets/Scripts";
        var fileEntries = Directory.GetFiles(selectedPath, "*", SearchOption.AllDirectories);

        List<string> oldGUIDs = new List<string>();
        Dictionary<string, string> newGUIDs = new Dictionary<string, string>();

        Debug.Log("<color=blue>REPLACE STARTED</color>");

        bool hasChanges = false;

        foreach (string f in fileEntries)
        {
            string[] fileLines = File.ReadAllLines(f);
            hasChanges = false;

            for (int i = 0; i < fileLines.Length; i++)
            {
                if (fileLines[i].Contains("guid: "))
                {
                    int index = fileLines[i].IndexOf("guid: ") + 6;
                    string oldGUID = fileLines[i].Substring(index, 32);

                    oldGUIDs.Add(oldGUID);
                    newGUIDs.Add(oldGUID, GetNewHash(projectName + f).ToString("N"));

                    fileLines[i] = fileLines[i].Replace(oldGUID, newGUIDs[oldGUID]);

                    hasChanges = true;
                }
            }

            if (hasChanges)
            {
                File.WriteAllLines(f, fileLines);
            }
        }


        ReplaceGUIDs(realPath + "Assets/Assets", oldGUIDs, newGUIDs);
        ReplaceGUIDs(realPath + "Assets/Scenes", oldGUIDs, newGUIDs);
        ReplaceGUIDs(realPath + "Assets/Resources", oldGUIDs, newGUIDs);

        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);

        Debug.Log($"<color=blue>REPLACE ENDED with updated {oldGUIDs.Count} files</color>");
    }

    private static void ReplaceGUIDs(string findDirectory, List<string> oldGUIDs, Dictionary<string, string> newGUIDs)
    {
        var fileEntries = Directory.GetFiles(findDirectory, "*", SearchOption.AllDirectories);

        bool hasChanges = false;

        foreach (string f in fileEntries)
        {
            string[] fileLines = File.ReadAllLines(f);
            hasChanges = false;

            for (int i = 0; i < fileLines.Length; i++)
            {

                if (fileLines[i].Contains("guid: "))
                {
                    int index = fileLines[i].IndexOf("guid: ") + 6;
                    string oldGUID = fileLines[i].Substring(index, 32);

                    if (oldGUIDs.Contains(oldGUID))
                    {
                        fileLines[i] = fileLines[i].Replace(oldGUID, newGUIDs[oldGUID]);
                        hasChanges = true;
                    }
                }
            }

            if (hasChanges)
            {
                File.WriteAllLines(f, fileLines);
            }
        }
    }

    private static Guid GetNewHash(string name)
    {
        using (MD5 md5 = MD5.Create())
        {
            byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(name));
            return new Guid(hash);
        }
    }

}
